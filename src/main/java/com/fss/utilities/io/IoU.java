package com.fss.utilities.io;

import java.io.IOException;
import java.io.InputStream;

public class IoU {
	public static String toString(InputStream is) {
		if (is == null) {
			return null;
		}
		try {
			int ava = is.available();
			if (ava < 0) {
				return null;
			}
			byte[] data = new byte[ava];
			is.read(data);
			return new String (data);
		} catch (IOException e) {
			return null;
		}
	}
}
