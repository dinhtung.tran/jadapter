package com.fss.utilities.converter;

public class Mod96 {
	public static int decodeMod96(byte[] byteData) {
		int loop = 0;
		int result = 0;
		do {
			result = (result << 5) + (result << 6);
			result += (int) (char) byteData[loop] - 32;
			loop++;
		} while (loop < byteData.length);
		return result;
	}
	
	public static byte[] encodeMod96(int number, int loop) {
		byte[] byteResult = new byte[loop];
		char temp;
		while (loop > 0) {
			temp = (char) ((number % 96) + 32);
			byteResult[loop - 1] = (byte) temp;
			number = number / 96;
			loop--;
		}
		return byteResult;
	} 
}
