package com.fss.utilities.array;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public final class ByteArrayU {

	private ByteArrayU() {

	}

	private static byte[] mergeByte(byte[]... bs) {
		int totalLength = 0;
		for (byte[] b : bs) {
			totalLength += b.length;
		}
		byte[] m = new byte[totalLength];
		int offset = 0;
		for (byte[] b : bs) {
			System.arraycopy(b, 0, m, offset, b.length);
			offset += b.length;
		}
		return m;
	}

	public static String toString(byte[] data) {
		return data == null ? "null" : new String(data);
	}
	
	public static byte[] getBytes(String string) {
		return string == null ? new byte[] { 0 } : string.getBytes();
	}

	public static byte[] cut(byte[] data, int fromIndex, int toIndex) {
		int len = toIndex - fromIndex;
		if (len <= 0 || fromIndex >= data.length)
			return new byte[0];
		if (toIndex > data.length) {
			toIndex = data.length;
			len = toIndex - fromIndex;
		}
		byte[] result = new byte[len];
		System.arraycopy(data, fromIndex, result, 0, len);
		return result;
	}

	public static int compare(byte[] b1, byte[] b2) {
		return new String(b1).compareTo(new String(b2));
	}

	public static boolean isEquals(byte[] b1, byte[] b2) {
		if (b1.length != b2.length)
			return false;
		for (int i = 0; i < b1.length; i++) {
			if (b1[i] != b2[i])
				return false;
		}
		return true;
	}

	public static Collection<byte[]> split(byte[] data, byte sepUnit) {
		ByteBuffer buffer = ByteBuffer.wrap(data);
		Collection<byte[]> coll = new ArrayList<>();
		byte[] temp = new byte[0];
		while (buffer.hasRemaining()) {
			int remain = buffer.remaining();
			temp = new byte[remain];
			subLoop: for (int i = 0; i < remain; i++) {
				byte c = buffer.get();
				if (sepUnit != c) {
					temp[i] = c;
				} else {
					byte[] rs = new byte[i];
					System.arraycopy(temp, 0, rs, 0, i);
					coll.add(rs);
					temp = null;
					break subLoop;
				}
			}
		}
		if (temp != null)
			coll.add(temp);
		return coll;
	}

	public static void main(String[] args) {		
		BigDecimal n = new BigDecimal("26.60");
		BigDecimal m = new BigDecimal("10000");
		BigDecimal rs = n.multiply(m);
		NumberFormat fomarter = new DecimalFormat("#.#"); 
		
		System.out.println("Raw rs: " + n.multiply(m));
		System.out.println("Formated rs: " + fomarter.format(rs));
		
		
		int sep = 32;
		String str = "minh duc ta ";
		byte[] data = str.getBytes();
		System.out.println("__________");
		for (byte[] b : split(data, (byte) ' ')) {
			// if (b.length > 0)
			System.out.println(new String(b));
		}
		System.out.println("----------");
	}

}
