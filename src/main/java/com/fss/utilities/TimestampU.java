package com.fss.utilities;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;

public final class TimestampU {
		
	public static boolean isAfter(DayOfWeek dow, int ordinalNum, LocalDate localDate) {
		return localDate.getDayOfMonth() > getDayOfMonth(dow, ordinalNum, localDate);
	}
	
	public static boolean isBefore(DayOfWeek dow, int ordinalNum, LocalDate localDate) {
		return localDate.getDayOfMonth() < getDayOfMonth(dow, ordinalNum, localDate);
	}	
	
	public static int getDayOfMonth(DayOfWeek dow, int ordinalNum, Year year, Month month) {
		DayOfWeek dayOfWeekOfFirstDayOfMonth = year.atMonth(month.getValue()).atDay(1).getDayOfWeek();
		int delta = dayOfWeekOfFirstDayOfMonth.getValue() - dow.getValue() - 1;
		int dom = 7 * ordinalNum - delta; 
		return dom;
	}
	
	public static int getDayOfMonth(DayOfWeek dow, int ordinalNum, LocalDate now) {
		return getDayOfMonth(dow, ordinalNum, Year.of(now.getYear()), now.getMonth());
	}

	public static int correctMonth(int month) {
		return (month - 1) % 12 + 1;
	}
}
