package com.fss.utilities;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.util.stream.Stream;

public final class DerivativeU {
	
	public enum UnderlyingSymbol {
		VN30 ,
		VGB5;
	}
	
	public static final String VN30_PREFIX = "VN30F";
	public static final String VGB5_PREFIX = "GB05F";
	
	public static void main(String[] args) {
		for (int i = 1; i <= 12; i++) {
			bm(TimestampU.correctMonth(i));
		}
	}
	
	public static void bm(int month) {
		LocalDate date = Year.now().atMonth(month).atDay(1);
		System.out.println("Start: date: " + date);
		getAvailableSymbols(UnderlyingSymbol.VN30, date).forEach(System.out::println);;
		System.out.println("End:_____________");
	}
	
	public static Stream<String> getAvailableSymbols(UnderlyingSymbol underlying, LocalDate localDate) {
		switch (underlying) {
		case VN30:
			return getAvailableSymbolsVN30(localDate);
		case VGB5:
			return getAvailableSymbolsVGB5(localDate);
		default:
			return Stream.empty();
		}
	}

	private static Stream<String> getAvailableSymbolsVGB5(LocalDate localDate) {
		return null;
	}

	private static Stream<String> getAvailableSymbolsVGB5(Month plus) {
		return null;
	}

	private static Stream<String> getAvailableSymbolsVN30(LocalDate localDate) {
		int dom = TimestampU.getDayOfMonth(DayOfWeek.THURSDAY, 3, localDate);
		if (localDate.getDayOfMonth() > dom) {
			return getAvailableSymbolsVN30(localDate.getYear(), localDate.getMonth().plus(1).getValue());
		} else {
			return getAvailableSymbolsVN30(localDate.getYear(), localDate.getMonth().getValue());
		}
	}

	private static Stream<String> getAvailableSymbolsVN30(int year, int month) {
		int theNextMonth = TimestampU.correctMonth(month + 1);
		int theLastMonthOfNextQuarter = TimestampU.correctMonth(Month.of(TimestampU.correctMonth(theNextMonth + 3)).firstMonthOfQuarter().getValue() - 1 + 12);
		theLastMonthOfNextQuarter = TimestampU.correctMonth(theLastMonthOfNextQuarter == theNextMonth ? theLastMonthOfNextQuarter + 3 : theLastMonthOfNextQuarter);
		int theLastMonthOfTheNextOfNextQuarter = Month.of(TimestampU.correctMonth(theLastMonthOfNextQuarter + 3)).getValue();
		
		String currentMonthSymbol = VN30_PREFIX + genVN30_Suffix(year, TimestampU.correctMonth(month));
		
		if (theNextMonth < month)
			year += 1;
		String nextMonthSymbol = VN30_PREFIX + genVN30_Suffix(year, theNextMonth);
		
		if (theLastMonthOfNextQuarter < theNextMonth)
			year += 1;
		String nextQuarterSymbol = VN30_PREFIX + genVN30_Suffix(year, theLastMonthOfNextQuarter);
		
		if (theLastMonthOfTheNextOfNextQuarter < theLastMonthOfNextQuarter)
			year += 1;
		String nextOfTheNextQuarterSymbol = VN30_PREFIX + genVN30_Suffix(year, theLastMonthOfTheNextOfNextQuarter);
		
		return Stream.of(currentMonthSymbol, nextMonthSymbol, nextQuarterSymbol, nextOfTheNextQuarterSymbol);
	}

	private static String genVN30_Suffix(int year, int month) {
		String yearInString = String.valueOf(year);
		String monthInString = String.valueOf(month);		
		return yearInString.substring(yearInString.length() - 2, yearInString.length()) + (monthInString.length() < 2 ? "0" + monthInString : monthInString);
	}
	
}
