package com.fss.jedis.constant;

public enum JedisDataType {
	/**
	 * @return Status code reply, specifically: 
	 * "none" if the key does not exist
	 * "string" if the key contains a String value 
	 * "list" if the key contains a List value 
	 * "set" if the key contains a Set value 
	 * "zset" if the key contains a Sorted Set value 
	 * "hash" if the key contains a Hash value
	 */
	
	NONE,
	STRING,
	LIST,
	SET,
	ZSET,
	HASH
}
