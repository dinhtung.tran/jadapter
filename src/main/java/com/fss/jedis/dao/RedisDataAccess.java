package com.fss.jedis.dao;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fss.jedis.constant.JedisDataType;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class RedisDataAccess {
	@Autowired
	private RedisConnectionManager manager;
		
	public String set(String key, String value) {
		return doTask((conn) -> conn.set(key, value));
	}
	
	public String set(String key, Object value) {
		return set(key, String.valueOf(value));
	}
	
	public boolean exists(String key) {
		return doTask((conn) -> conn.exists(key));
	}
	
	public Set<String> keys(String pattern) {
		return doTask((conn) -> conn.keys(pattern));
	}
	
	public List<String> getList(String key, long start, long stop) {
		List<String> list = doTask((conn) -> conn.lrange(key, start, stop));
		if (list == null || list.isEmpty())
			return Collections.emptyList();
		return Collections.unmodifiableList(list);
	}
	
	public List<String> getList(String key) {
		return getList(key, 0, getListLength(key));
	}
	
	public Set<String> getSet(String key) {
		return doTask((conn) -> 
			conn.smembers(key)
		);
	}
	
	public long getListLength(String key) {
		Long len = doTask((conn) -> conn.llen(key));
		if (len == null)
			return 0;
		return len;
	}

	public String getString(String key) {
		return doTask((conn) -> conn.get(key));
	}

	public JedisDataType type(String key) {
		String type = doTask((conn) -> conn.type(key));
		if (type == null)
			return null;
		return JedisDataType.valueOf(type.toUpperCase());
	}
	
	public List<Object> pipeline(Stream<RedisPipelineTask> taskStream) {
		return manager.pipeline(taskStream);
	}
	
	public <T> T doTask(RedisTask<T> task) {
		return manager.doTask(task);
	}
	
	public Set<String> getKeys(String pattern) {
		return doTask((conn) -> {
			return conn.keys(pattern);
		});
	}
}
