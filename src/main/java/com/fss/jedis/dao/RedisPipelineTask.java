package com.fss.jedis.dao;

import redis.clients.jedis.MultiKeyPipelineBase;
import redis.clients.jedis.Response;

public interface RedisPipelineTask<T> {
	public Response<T> doTask(MultiKeyPipelineBase connection);
}
