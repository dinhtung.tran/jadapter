package com.fss.jedis.dao;

import redis.clients.jedis.Jedis;

public interface RedisTask<T> {
	public T doTask(Jedis connection);
}
