package com.fss.jedis.dao;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Pipeline;
import redis.clients.jedis.exceptions.JedisException;

@Component
@Slf4j
public class RedisConnectionManager implements InitializingBean {

	@Value("${redis.server.host}")
	private String host;
	@Value("${redis.server.port}")
	private int port;
	@Value("${redis.connection.max}")
	private int maxConenction;
	@Value("${redis.database}")
	private int database;
	@Value("${redis.timeout}")
	private int timeout;
	@Value("${redis.password}")
	private String password;

	private JedisPool pool;
	private JedisPoolConfig config;

	@Override
	public void afterPropertiesSet() throws Exception {
		config = new JedisPoolConfig();
		config.setMaxTotal(maxConenction);
		password = password != null && password.isEmpty() ? null : password;
		pool = new JedisPool(config, host, port, timeout, password, database);
	}

	public Jedis getConnection() {
		return pool.getResource();
	}

	public void closeConnection(Jedis conn) {
		conn.close();
	}

	public <T> T doTask(RedisTask<T> task) {
		Jedis connection = null;
		try {
			connection = getConnection();
			return task.doTask(connection);
		} catch (JedisException je) {
			log.error("doTask exception!", je);
			return null;
		} finally {
			if (connection != null)
				connection.close();
		}
	}

	public List<Object> pipeline(Stream<RedisPipelineTask> taskStream) {
		Jedis connection = null;
		Pipeline pipeline = null;
		try {
			connection = getConnection();
			pipeline = connection.pipelined();
//			pipeline.multi();
			final Pipeline pipeLineTemp = pipeline;
			taskStream.forEach((task) -> task.doTask(pipeLineTemp));
//			Response<List<Object>> res = pipeline.exec();
			pipeline.close();
			return pipeline.syncAndReturnAll();
		} catch (JedisException je) {
			log.error("doTask exception!", je);
			return null;
		} finally {
			if (connection != null)
				connection.close();
			if (pipeline != null)
				pipeline.close();
		}
	}
}
