package com.fss.jedis.model;

import lombok.Data;
import lombok.NonNull;

@Data(staticConstructor = "of")
public class Instrument {
	@NonNull
	private String symbol;
	@NonNull
	private String exchange;
	@NonNull
	private int ceilingPrice;
	@NonNull
	private int floorPrice;
	@NonNull
	private int basicPrice;
	
}
