package com.fss.jedis.model;

import java.util.List;
import java.util.Set;

import com.fss.jedis.dao.RedisPipelineTask;
import com.fss.jedis.dao.RedisTask;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.MultiKeyPipelineBase;
import redis.clients.jedis.Response;

public abstract class RedisSetObjectTask implements RedisObject<Set<String>>, RedisTask<Long>, RedisPipelineTask<Long> {
	public final Long doTask(Jedis connection) {
		String[] arr = getValue().toArray(new String[getValue().size()]);
		return connection.sadd(getKey(), arr);
	}
	
	public final Response<Long> doTask(MultiKeyPipelineBase connection) {
		String[] arr = getValue().toArray(new String[getValue().size()]);
		return connection.sadd(getKey(), arr);
	}
}