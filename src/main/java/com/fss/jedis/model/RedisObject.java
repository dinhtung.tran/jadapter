package com.fss.jedis.model;

public interface RedisObject<T> {
	String getKey();
	T getValue();
}
