package com.fss.jedis.model;

import com.fss.jedis.dao.RedisPipelineTask;
import com.fss.jedis.dao.RedisTask;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.MultiKeyPipelineBase;
import redis.clients.jedis.Response;

public abstract class RedisStringObjectTask implements RedisObject<String>, RedisTask<String>, RedisPipelineTask<String> {
	public final String doTask(Jedis connection) {
		return connection.set(getKey(), getValue());
	}
	
	public final Response<String> doTask(MultiKeyPipelineBase connection) {
		return connection.set(getKey(), getValue());
	}
}
