package com.fss.jedis.model.adapter;

import com.fss.jedis.model.RedisStringObjectTask;

import lombok.Data;

@Data(staticConstructor = "of")
public class RedisRefInfor  extends RedisStringObjectTask {
	
	public static final String PREFIX = "ref:";

	private final String key;
	private String value;

	@Override
	public String getKey() {
		return PREFIX + key;
	}

	@Override
	public String getValue() {
		if (value == null)
			value = "";
		return value;
	}
	
}
