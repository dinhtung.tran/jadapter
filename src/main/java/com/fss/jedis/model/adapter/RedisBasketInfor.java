package com.fss.jedis.model.adapter;

import java.util.Collections;
import java.util.Set;

import com.fss.jedis.model.RedisSetObjectTask;

import lombok.Data;

@Data(staticConstructor = "of")
public class RedisBasketInfor extends RedisSetObjectTask {
	
	public static final String PREFIX = "basket:";

	private final String key;
	private Set<String> value;

	@Override
	public String getKey() {
		return PREFIX + key;
	}

	@Override
	public Set<String> getValue() {
		if (value == null)
			value = Collections.emptySet();
		return value;
	}

}
