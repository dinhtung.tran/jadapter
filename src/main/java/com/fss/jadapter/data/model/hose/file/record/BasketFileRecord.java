package com.fss.jadapter.data.model.hose.file.record;

import com.fss.jadapter.data.model.hose.file.HosePrsFileRecord;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
public class BasketFileRecord implements HosePrsFileRecord {
	private int stockNo;
	private String symbol;
}
