package com.fss.jadapter.data.model.worker;

public abstract class AbstractAlgoMessage implements AlgoMessage {

	public final String toString() {
		return getTransformedMessage();
	}

}
