package com.fss.jadapter.data.model.hnx.infogate.message;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fss.jadapter.data.model.hnx.infogate.AbstractHnxInfogateMessage;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Symbol", "BoardCode", "NoTopPrice", "BeginString", "BodyLength", "MsgType", "SenderCompID",
		"SendingTime" })
public class TopPriceInfoIG extends AbstractHnxInfogateMessage implements Serializable {

	@JsonProperty("Symbol")
	public String symbol;
	@JsonProperty("BoardCode")
	public String boardCode;
	@JsonProperty("NoTopPrice")
	public List<NoTopPrice> noTopPrice = new ArrayList<NoTopPrice>();
	@JsonProperty("BeginString")
	public String beginString;
	@JsonProperty("BodyLength")
	public String bodyLength;
	@JsonProperty("MsgType")
	public String msgType;
	@JsonProperty("SenderCompID")
	public String senderCompID;
	@JsonProperty("SendingTime")
	public String sendingTime;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = 8514064076123339650L;

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonPropertyOrder({ "NumTopPrice", "BestBidPrice", "BestBidQtty", "BestOfferPrice", "BestOfferQtty" })
	public static class NoTopPrice implements Serializable {

		@JsonProperty("NumTopPrice")
		public String numTopPrice;
		@JsonProperty("BestBidPrice")
		public String bestBidPrice;
		@JsonProperty("BestBidQtty")
		public String bestBidQtty;
		@JsonProperty("BestOfferPrice")
		public String bestOfferPrice;
		@JsonProperty("BestOfferQtty")
		public String bestOfferQtty;
		@JsonIgnore
		private Map<String, Object> additionalProperties = new HashMap<String, Object>();
		private final static long serialVersionUID = -950209640139597018L;

		@JsonAnyGetter
		public Map<String, Object> getAdditionalProperties() {
			return this.additionalProperties;
		}

		@JsonAnySetter
		public void setAdditionalProperty(String name, Object value) {
			this.additionalProperties.put(name, value);
		}

	}

}