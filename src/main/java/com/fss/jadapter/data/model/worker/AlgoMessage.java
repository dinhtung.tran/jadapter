package com.fss.jadapter.data.model.worker;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public interface AlgoMessage {
	public static final char MESSAGE_SEPARATOR = '@';
	public static final char ELEMENTS_SEPARATOR = '|';
	public static final char FIELDS_SEPARATOR = ';';
	public static final char FIELD_KEY_VALUE_SEPARATOR = ':';
	public static final NumberFormat NUMBER_FORMAT = new DecimalFormat("#.##");
	
	public String getTransformedMessage();
	public String getTransformedSnapshotMessage();
	
}
