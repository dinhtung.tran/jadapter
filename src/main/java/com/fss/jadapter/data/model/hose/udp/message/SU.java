package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;
import lombok.ToString;

@Data
public class SU extends AbstractHoseUdpBroadcastMessage {
	private int securityNumberOld;
	private int securityNumberNew;
	private String filler_1;
	private int sectorNumber;
	private String filler_2;
	private String securitySymbol;
	private String securityType;
	private int ceilingPrice;
	private int floorPrice;
	private int lastSalePrice;
	private String marketId;
	private String filler_3;
	private String securityName;
	private String filler_4;
	private String suspension;
	private String delist;
	private String haltResumeFlag;
	private String split;
	private String benefit;
	private String meeting;
	private String notice;
	private String clientIdRequired;
	private int parValue;
	private String sdcFlag;
	private int priorClosePrice;
	private String priorCloseDate;
	private int openPrice;
	private int highestPrice;
	private int lowestPrice;
	private int totalSharesTraded;
	private int totalValuesTraded;
	private int boardLot;
	private String filler_5;
	private String underlyingSymbol;
	private String issuerName;
	private String coveredWarrantType;
	private String maturityDate;
	private String lastTradingDate;
	private int exercisePrice;
	private String exerciseRatio;
	private int listedShare;

}
