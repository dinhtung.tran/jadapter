package com.fss.jadapter.data.model.system;

import java.time.LocalDateTime;
import java.time.LocalTime;

import lombok.Data;

@Data(staticConstructor = "of")
public class InitDaySignal implements SystemSignal {
	private final LocalTime idTime;
	private final LocalDateTime createdDateTime = LocalDateTime.now();
}
