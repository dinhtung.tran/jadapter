package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;
import lombok.ToString;

@Data
public class SI extends AbstractHoseUdpBroadcastMessage {
	private int indexSectoral;
	private String filler;
	private int indexTime;


}
