package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;

@Data
public class NH extends AbstractHoseUdpBroadcastMessage {
	private int newsnumber;
	private String securitySymbol;
	private int newsHeadlineLength;
	private int totalNewsStoryPages;
	private String newsHeadlineText;
	
}
