package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;

@Data
public class AA extends AbstractHoseUdpBroadcastMessage {
	private int securityNumber;
	private int volume;
	private String price;
	private int firm;
	private int trader;
	private String side;
	private String board;
	private int time;
	private String flag;
	private String contact;	
}
