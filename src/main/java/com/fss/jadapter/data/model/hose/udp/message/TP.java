package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;

@Data
public class TP extends AbstractHoseUdpBroadcastMessage {
	private int securityNumber;
	private String side;
	private int price1;
	private int lotVolume1;
	private int price2;
	private int lotVolume2;
	private int price3;
	private int lotVolume3;
}