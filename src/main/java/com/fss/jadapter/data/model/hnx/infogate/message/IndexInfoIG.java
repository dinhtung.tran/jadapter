package com.fss.jadapter.data.model.hnx.infogate.message;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fss.jadapter.data.model.hnx.infogate.AbstractHnxInfogateMessage;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "IDIndex", "IndexCode", "Value", "CalTime", "Change", "RatioChange", "TotalQtty", "TotalValue",
		"IndexName", "TradingDate", "CurrentStatus", "totalStock", "PriorIndexVal", "HighestIndex", "LowestIndex",
		"TypeIndex", "BeginString", "BodyLength", "MsgType", "SenderCompID", "SendingTime" })
public class IndexInfoIG extends AbstractHnxInfogateMessage implements Serializable {

	@JsonProperty("IDIndex")
	public String iDIndex;
	@JsonProperty("IndexCode")
	public String indexCode;
	@JsonProperty("Value")
	public String value;
	@JsonProperty("CalTime")
	public String calTime;
	@JsonProperty("Change")
	public String change;
	@JsonProperty("RatioChange")
	public String ratioChange;
	@JsonProperty("TotalQtty")
	public String totalQtty;
	@JsonProperty("TotalValue")
	public String totalValue;
	@JsonProperty("IndexName")
	public String indexName;
	@JsonProperty("TradingDate")
	public String tradingDate;
	@JsonProperty("CurrentStatus")
	public String currentStatus;
	@JsonProperty("totalStock")
	public String totalStock;
	@JsonProperty("PriorIndexVal")
	public String priorIndexVal;
	@JsonProperty("HighestIndex")
	public String highestIndex;
	@JsonProperty("LowestIndex")
	public String lowestIndex;
	@JsonProperty("TypeIndex")
	public String typeIndex;
	@JsonProperty("BeginString")
	public String beginString;
	@JsonProperty("BodyLength")
	public String bodyLength;
	@JsonProperty("MsgType")
	public String msgType;
	@JsonProperty("SenderCompID")
	public String senderCompID;
	@JsonProperty("SendingTime")
	public String sendingTime;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = -6322111188950515291L;

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
