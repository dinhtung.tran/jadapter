package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;

@Data
public class IU extends AbstractHoseUdpBroadcastMessage {
	private int index;
	private int totalTrades;
	private int totalSharesTraded;
	private int totalValuesTraded;
	private int upVolume;
	private int downVolume;
	private int noChangeVolume;
	private int advances;
	private int declines;
	private int noChange;
	private String filler1;
	private String marketId;
	private String filler2;
	private int time;
}
