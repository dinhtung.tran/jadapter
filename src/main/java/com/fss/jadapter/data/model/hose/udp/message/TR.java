package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;

@Data
public class TR extends AbstractHoseUdpBroadcastMessage {
	private int securityNumber;
	private int totalRoom;
	private int currentRoom;
	private int buyVolume;
	private int sellVolume;
}