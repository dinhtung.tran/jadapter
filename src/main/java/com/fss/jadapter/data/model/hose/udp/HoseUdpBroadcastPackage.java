package com.fss.jadapter.data.model.hose.udp;

import java.util.List;

import lombok.Data;

@Data
public class HoseUdpBroadcastPackage {
	private int seqNum;
	private String marketId;
	private int messageCount;
	private List<HoseUdpBroadcastMessage> packedContent;
}
