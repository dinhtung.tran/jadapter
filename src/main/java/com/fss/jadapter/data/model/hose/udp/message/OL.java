package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;

@Data
public class OL extends AbstractHoseUdpBroadcastMessage {
	private int securityNumber;
	private int oddLotVolume;
	private int price;
	private String side;
	private int referenceNumber;

}
