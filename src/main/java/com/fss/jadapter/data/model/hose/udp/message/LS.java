package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;

@Data
public class LS extends AbstractHoseUdpBroadcastMessage {
	private int confirmNumber;
	private int securityNumber;
	private int lotVolume;
	private int price;
	private String side;
	
	
}
