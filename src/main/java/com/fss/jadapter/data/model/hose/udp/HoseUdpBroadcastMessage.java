package com.fss.jadapter.data.model.hose.udp;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fss.jadapter.data.model.hose.udp.message.*;

@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "messageType")
@JsonSubTypes({
	@JsonSubTypes.Type(value = LS.class, name="LS"),
	@JsonSubTypes.Type(value = PO.class, name="PO"),
	@JsonSubTypes.Type(value = AA.class, name="AA"),
	@JsonSubTypes.Type(value = TP.class, name="TP"),
	@JsonSubTypes.Type(value = IU.class, name="IU"),
	@JsonSubTypes.Type(value = BR.class, name="BR"),
	@JsonSubTypes.Type(value = BS.class, name="BS"),
	@JsonSubTypes.Type(value = CO.class, name="CO"),
	@JsonSubTypes.Type(value = DC.class, name="DC"),
	@JsonSubTypes.Type(value = GA.class, name="GA"),
	@JsonSubTypes.Type(value = LO.class, name="LO"),
	@JsonSubTypes.Type(value = NH.class, name="NH"),
	@JsonSubTypes.Type(value = NS.class, name="NS"),
	@JsonSubTypes.Type(value = OL.class, name="OL"),
	@JsonSubTypes.Type(value = OS.class, name="OS"),
	@JsonSubTypes.Type(value = PD.class, name="PD"),
	@JsonSubTypes.Type(value = SC.class, name="SC"),
	@JsonSubTypes.Type(value = SI.class, name="SI"),
	@JsonSubTypes.Type(value = SR.class, name="SR"),
	@JsonSubTypes.Type(value = SS.class, name="SS"),
	@JsonSubTypes.Type(value = SU.class, name="SU"),
	@JsonSubTypes.Type(value = TC.class, name="TC"),
	@JsonSubTypes.Type(value = TR.class, name="TR"),
	@JsonSubTypes.Type(value = TS.class, name="TS"),
		})
public interface HoseUdpBroadcastMessage {
	public String getMessageType();
}
