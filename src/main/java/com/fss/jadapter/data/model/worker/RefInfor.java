package com.fss.jadapter.data.model.worker;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Builder
@Getter
@Slf4j
public class RefInfor implements AlgoMessage {
	
	@NonNull
	private String symbol;
	@NonNull
	private String ceiling;
	@NonNull
	private String floor;
	@NonNull
	private String basic;
	private String fullName;
	private String benefit;
	private String meeting;
	private String split;
	private String isMargin;
	
	@Override
	public String getTransformedMessage() {
		return getTransformedSnapshotMessage();
	}
	
	@Override
	public String getTransformedSnapshotMessage() {
		StringBuilder sb = new StringBuilder();
		sb
		.append("")
		.append(symbol)
		.append(FIELDS_SEPARATOR)
		.append(validData(ceiling))
		.append(FIELDS_SEPARATOR)
		.append(validData(floor))
		.append(FIELDS_SEPARATOR)
		.append(validData(basic))
		.append(FIELDS_SEPARATOR)
		.append(validData(fullName))
		.append(FIELDS_SEPARATOR)
		.append(validData(benefit))
		.append(FIELDS_SEPARATOR)
		.append(validData(meeting))
		.append(FIELDS_SEPARATOR)
		.append(validData(split))
		.append(FIELDS_SEPARATOR)
		.append(validData(isMargin))
		;
		
		return sb.toString();
	}

	private String validData(String data) {
		if (data == null)
		return "";
		return data;
	}

	public final String toString() {
		return getTransformedMessage();
	}
}
