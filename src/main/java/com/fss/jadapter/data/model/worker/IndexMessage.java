package com.fss.jadapter.data.model.worker;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.stream.Stream;

import com.fss.jadapter.constant.AlgoMessageFields;
import com.fss.jadapter.data.model.ChangedValues;

import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Data(staticConstructor = "of")
@Slf4j
public class IndexMessage implements AlgoMessage {
	@NonNull
	private String indexName;
	@NonNull
	private String marketStatus;
	@NonNull
	private ChangedValues changedValues;
	
	@Override
	public String getTransformedMessage() {
		StringBuilder sb = new StringBuilder();
		sb
			.append("")
			.append(indexName)
			.append(FIELDS_SEPARATOR)
			.append(marketStatus)
			.append(FIELDS_SEPARATOR)
		;
		changedValues.forEach((k, v) -> {
			String convertedValue = convertValue(k, v);
			sb
			.append(k).append(FIELD_KEY_VALUE_SEPARATOR).append(convertedValue)
			.append(FIELDS_SEPARATOR);
		});
		return sb.toString();
	
	}
	
	public final String toString() {
		return getTransformedMessage();
	}

	@Override
	public String getTransformedSnapshotMessage() {
		StringBuilder sb = new StringBuilder();
		sb
			.append("")
			.append(indexName)
			.append(FIELDS_SEPARATOR)
			.append(marketStatus)
			.append(FIELDS_SEPARATOR)
		;
		
		Stream.of(AlgoMessageFields.Index.MARKETINFOR_ALL_FIELDS).forEach((s) -> {
			if (changedValues.contains(s)) {
				String convertedValue = convertValue(s, changedValues.get(s));
				sb
				.append(convertedValue);
			} else {
				sb
				.append("");
			}
			sb
			.append(FIELDS_SEPARATOR);
		});
		return sb.toString();
	}
	
	private String convertValue(String k, String v) {
		BigDecimal division = AlgoMessageFields.Index.getDivision(k);
		if (division == null 
//				|| division.compareTo(BigDecimal.ONE) == 0
				) {
			return v;
		} else {
			try {
				BigDecimal dv = new BigDecimal(v);
				return NUMBER_FORMAT.format(dv.divide(division, 2, RoundingMode.HALF_UP));
			} catch (Exception e) {
				log.error("k: {} v: {}", k, v);
				log.error("Error!", e);
				return v;
			}
		}
	}
}
