package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;
import lombok.ToString;

@Data
public class SS extends AbstractHoseUdpBroadcastMessage {
	private int securityNumber;
	private String filler_1;
	private int sectorNumber;
	private String filler_2;
	private String haltResumeFlag;
	private String systemControlCode;
	private String filler_3;
	private String suspension;
	private String delist;
	private String filler_4;
	private int ceiling;
	private int floorPrice;
	private String securityType;
	private int priorClosePrice;
	private int filler_5;
	private String split;
	private String benefit;
	private String meeting;
	private String notice;
	private int boardLot;
	private String filler_6;
	private String underlyingSymbol;
	private String issuerName;
	private String coveredWarrantType;
	private String maturityDate;
	private String lastTradingDate;
	private int exercisePrice;
	private String exerciseRatio;
	private int listedShare;
}
