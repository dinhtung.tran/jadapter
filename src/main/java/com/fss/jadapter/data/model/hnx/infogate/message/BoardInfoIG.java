package com.fss.jadapter.data.model.hnx.infogate.message;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fss.jadapter.data.model.hnx.infogate.AbstractHnxInfogateMessage;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "DateNo", "totalNormalTradedQttyOd", "totalNormalTradedValueOd", "totalNormalTradedQttyRd",
		"totalNormalTradedValueRd", "TotalStock", "numSymbolAdvances", "numSymbolNochange", "numSymbolDeclines",
		"TotalTrade", "TradingSessionID", "TradSesStatus", "MarketCode", "Tradingdate", "Time", "Name", "Shortname",
		"BoardCode", "BoardStatus", "BeginString", "BodyLength", "MsgType", "SenderCompID", "SendingTime" })
public class BoardInfoIG extends AbstractHnxInfogateMessage implements Serializable {

	@JsonProperty("DateNo")
	public String dateNo;
	@JsonProperty("totalNormalTradedQttyOd")
	public String totalNormalTradedQttyOd;
	@JsonProperty("totalNormalTradedValueOd")
	public String totalNormalTradedValueOd;
	@JsonProperty("totalNormalTradedQttyRd")
	public String totalNormalTradedQttyRd;
	@JsonProperty("totalNormalTradedValueRd")
	public String totalNormalTradedValueRd;
	@JsonProperty("TotalStock")
	public String totalStock;
	@JsonProperty("numSymbolAdvances")
	public String numSymbolAdvances;
	@JsonProperty("numSymbolNochange")
	public String numSymbolNochange;
	@JsonProperty("numSymbolDeclines")
	public String numSymbolDeclines;
	@JsonProperty("TotalTrade")
	public String totalTrade;
	@JsonProperty("TradingSessionID")
	public String tradingSessionID;
	@JsonProperty("TradSesStatus")
	public String tradSesStatus;
	@JsonProperty("MarketCode")
	public String marketCode;
	@JsonProperty("Tradingdate")
	public String tradingdate;
	@JsonProperty("Time")
	public String time;
	@JsonProperty("Name")
	public String name;
	@JsonProperty("Shortname")
	public String shortname;
	@JsonProperty("BoardCode")
	public String boardCode;
	@JsonProperty("BoardStatus")
	public String boardStatus;
	@JsonProperty("BeginString")
	public String beginString;
	@JsonProperty("BodyLength")
	public String bodyLength;
	@JsonProperty("MsgType")
	public String msgType;
	@JsonProperty("SenderCompID")
	public String senderCompID;
	@JsonProperty("SendingTime")
	public String sendingTime;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = -1702897821884068788L;

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}