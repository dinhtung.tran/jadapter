package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;
import lombok.ToString;

@Data
public class TC extends AbstractHoseUdpBroadcastMessage {
	private int firm;
	private int traderID;
	private String traderStatus;
}
