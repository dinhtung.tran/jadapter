package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;

@Data
public class LO extends AbstractHoseUdpBroadcastMessage {
	private int confirmNumber;
	private int securityNumber;
	private int oddLotVolume;
	private int price;
	private int referenceNumber;

}
