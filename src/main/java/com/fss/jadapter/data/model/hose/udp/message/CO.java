package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;

@Data
public class CO extends AbstractHoseUdpBroadcastMessage {
	private int referenceNumber;
}
