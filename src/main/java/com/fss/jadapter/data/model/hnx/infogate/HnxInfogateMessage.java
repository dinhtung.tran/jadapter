package com.fss.jadapter.data.model.hnx.infogate;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.fss.jadapter.data.model.hnx.infogate.message.BoardInfoIG;
import com.fss.jadapter.data.model.hnx.infogate.message.DerivativeInfoIG;
import com.fss.jadapter.data.model.hnx.infogate.message.IndexInfoIG;
import com.fss.jadapter.data.model.hnx.infogate.message.OddLotITopPriceInfoG;
import com.fss.jadapter.data.model.hnx.infogate.message.BasketInfoIG;
import com.fss.jadapter.data.model.hnx.infogate.message.StockInfoIG;
import com.fss.jadapter.data.model.hnx.infogate.message.TopPriceInfoIG;

@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "MsgType")
@JsonSubTypes({
	@JsonSubTypes.Type(value = StockInfoIG.class, name="SI"),
	@JsonSubTypes.Type(value = BasketInfoIG.class, name="S"),
	@JsonSubTypes.Type(value = BoardInfoIG.class, name="BI"),
	@JsonSubTypes.Type(value = TopPriceInfoIG.class, name="TP"),
	@JsonSubTypes.Type(value = OddLotITopPriceInfoG.class, name="PO"),
	@JsonSubTypes.Type(value = DerivativeInfoIG.class, name="DI"),
	@JsonSubTypes.Type(value = IndexInfoIG.class, name="I"),
})
public interface HnxInfogateMessage {

}
