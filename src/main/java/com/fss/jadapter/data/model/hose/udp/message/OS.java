package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;

@Data
public class OS extends AbstractHoseUdpBroadcastMessage {
	private int securityNumber;
	private int price;


}
