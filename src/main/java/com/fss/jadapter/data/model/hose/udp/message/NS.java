package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;

@Data
public class NS extends AbstractHoseUdpBroadcastMessage {
	private int newsNumber;
	private int newsPageNumber;
	private int newsTextLength;
	private String newsText;
	
}
