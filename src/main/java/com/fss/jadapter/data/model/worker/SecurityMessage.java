package com.fss.jadapter.data.model.worker;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.stream.Stream;

import com.fss.jadapter.constant.AlgoMessageFields;
import com.fss.jadapter.data.model.ChangedValues;

import lombok.Data;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Data(staticConstructor = "of")
@Slf4j
public class SecurityMessage implements AlgoMessage {
	
	@NonNull
	private String symbol;
	@NonNull
	private ChangedValues changedValues;
	
	@Override
	public String getTransformedMessage() {
		StringBuilder sb = new StringBuilder();
		sb
			.append("")
			.append(symbol)
			.append(FIELDS_SEPARATOR)
		;
		changedValues.forEach((k, v) -> {
			String convertedValue = convertValue(k, v);
			sb
			.append(k).append(FIELD_KEY_VALUE_SEPARATOR).append(convertedValue)
			.append(FIELDS_SEPARATOR);
		});
		return sb.toString();
	}
	
	@Override
	public String getTransformedSnapshotMessage() {
		StringBuilder sb = new StringBuilder();
		sb
		.append("")
		.append(symbol)
		.append(FIELDS_SEPARATOR)
		;
		
		Stream.of(AlgoMessageFields.STOCKINFOR_ALL_FIELDS).forEach((s) -> {
			if (changedValues.contains(s)) {
				String convertedValue = convertValue(s, changedValues.get(s));
				sb
				.append(convertedValue);
			} else {
				sb
				.append("");
			}
			sb
			.append(FIELDS_SEPARATOR);
		});
		return sb.toString();
	}

	public final String toString() {
		return getTransformedMessage();
	}
	
	private String convertValue(String k, String v) {
		BigDecimal division = AlgoMessageFields.getDivision(k);
		if (division == null || division.compareTo(BigDecimal.ONE) == 0) {
			return v;
		} else {
			try {
				BigDecimal dv = new BigDecimal(v);
				return NUMBER_FORMAT.format(dv.divide(division, 2, RoundingMode.HALF_UP));
			} catch (Exception e) {
				log.debug("k: {} v: {} Msg: {}", k, v, e.getMessage());
//				log.error("Error! Msg: {}", e);
				return v;
			}
		}
	}
}
