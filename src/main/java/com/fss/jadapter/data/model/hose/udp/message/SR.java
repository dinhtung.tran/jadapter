package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;
import lombok.ToString;

@Data
public class SR extends AbstractHoseUdpBroadcastMessage {
	private int securityNumber;
	private int mainOrForeignDeal;
	private int mainOrForeignAccVolume;
	private int mainOrForeignAccValue;
	private int dealsInBigLotBoard;
	private int bigLotAccVolume;
	private int bigLotAccValue;
	private int dealsInOddLotBoard;
	private int oddLotAccVolume;
	private int oddLotAccValue;
}
