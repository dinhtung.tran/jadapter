package com.fss.jadapter.data.model.hose.udp.message;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fss.jadapter.data.model.hose.udp.HoseUdpBroadcastMessage;

public abstract class AbstractHoseUdpBroadcastMessage implements HoseUdpBroadcastMessage{
	
	@Override
	@JsonIgnore
	public final String getMessageType() {
		return this.getClass().getSimpleName();
	}
}
