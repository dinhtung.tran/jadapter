package com.fss.jadapter.data.model.worker;

import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NonNull;
import lombok.Setter;

@Data(staticConstructor = "of")
@Setter(AccessLevel.NONE)
public class DataVersionMessage implements AlgoMessage {
	@NonNull
	private String serverTime;
	@NonNull
	private String marketStatus;
	@NonNull
	private int nextVersion;
	
	private List<IndexMessage> indexMessageList = new ArrayList<>();
	private List<SecurityMessage> securityMessageList = new ArrayList<>();
	
	@Override
	public String getTransformedMessage() {
		StringBuilder sb = new StringBuilder();
		StringBuffer indexMessageListSb = new StringBuffer();
		StringBuffer securityMessageListSb = new StringBuffer();
		
		indexMessageListSb.append("");
		indexMessageList.forEach((v) -> {
			indexMessageListSb
			.append(v).append(ELEMENTS_SEPARATOR)
			;
		});
		
		securityMessageListSb.append("");
		securityMessageList.forEach((v) -> {
			securityMessageListSb
			.append(v).append(ELEMENTS_SEPARATOR)
			;
		});
		
//		if (!indexMessageList.isEmpty()) {
//			sb
//			.append("")
//			.append(serverTime)
//			.append(MESSAGE_SEPARATOR)
//			.append(marketStatus)
//			.append(MESSAGE_SEPARATOR)
//			.append(indexMessageListSb)
//			.append(MESSAGE_SEPARATOR)
//			.append(nextVersion);
//			;
//		}
//		if (!securityMessageList.isEmpty()) {
//			sb
//			.append("")
//			.append(securityMessageListSb)
//			.append(MESSAGE_SEPARATOR)
//			.append(nextVersion);
//			;
//		}
		
		sb
			.append("")
			.append(serverTime)
			.append(MESSAGE_SEPARATOR)
			.append(marketStatus)
			.append(MESSAGE_SEPARATOR)
			.append(indexMessageListSb)
			.append(MESSAGE_SEPARATOR)
			.append(securityMessageListSb)
			.append(MESSAGE_SEPARATOR)
			.append(nextVersion);
		;
		
		return sb.toString();
	
	}
	
	public final String toString() {
		return getTransformedMessage();
	}

	@Override
	public String getTransformedSnapshotMessage() {
		StringBuilder sb = new StringBuilder();
		StringBuffer indexMessageListSb = new StringBuffer();
		StringBuffer securityMessageListSb = new StringBuffer();
		
		indexMessageListSb.append("");
		indexMessageList.forEach((v) -> {
			indexMessageListSb
			.append(v.getTransformedSnapshotMessage()).append(ELEMENTS_SEPARATOR)
			;
		});
		
		securityMessageListSb.append("");
		securityMessageList.forEach((v) -> {
			securityMessageListSb
			.append(v.getTransformedSnapshotMessage()).append(ELEMENTS_SEPARATOR)
			;
		});
		
		sb
		.append("");
		
		if (!indexMessageList.isEmpty()) {
			sb
			.append("")
			.append(serverTime)
			.append(MESSAGE_SEPARATOR)
			.append(marketStatus)
			.append(MESSAGE_SEPARATOR)
			.append(indexMessageListSb)
			.append(MESSAGE_SEPARATOR)
			.append(nextVersion);
			;
		}
		if (!securityMessageList.isEmpty()) {
			sb
			.append("")
			.append(securityMessageListSb)
			.append(MESSAGE_SEPARATOR)
			.append(nextVersion);
			;
		}
		
//		sb
//			.append("")
//			.append(serverTime)
//			.append(MESSAGE_SEPARATOR)
//			.append(marketStatus)
//			.append(MESSAGE_SEPARATOR)
//			.append(indexMessageListSb)
//			.append(MESSAGE_SEPARATOR)
//			.append(securityMessageListSb)
//			.append(MESSAGE_SEPARATOR)
//			.append(nextVersion);
		;
		
		return sb.toString();
	}
	
}
