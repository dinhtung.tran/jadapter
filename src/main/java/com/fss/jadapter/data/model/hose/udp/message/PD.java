package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;

@Data
public class PD extends AbstractHoseUdpBroadcastMessage {
	private int confirmNumber;
	private int securityNumber;
	private int volume;
	private int price;
	private String board;
}
