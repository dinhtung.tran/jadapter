package com.fss.jadapter.data.model.hnx.infogate.message;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fss.jadapter.data.model.hnx.infogate.AbstractHnxInfogateMessage;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "BeginString", "SenderCompID", "Weighted", "AddDate", "SendingTime", "Symbol", "IndexCode",
		"IDSymbol", "totalQtty", "MsgType", "BodyLength", "IDIndex" })
public class BasketInfoIG extends AbstractHnxInfogateMessage implements Serializable {

	@JsonProperty("BeginString")
	public String beginString;
	@JsonProperty("SenderCompID")
	public String senderCompID;
	@JsonProperty("Weighted")
	public String weighted;
	@JsonProperty("AddDate")
	public String addDate;
	@JsonProperty("SendingTime")
	public String sendingTime;
	@JsonProperty("Symbol")
	public String symbol;
	@JsonProperty("IndexCode")
	public String indexCode;
	@JsonProperty("IDSymbol")
	public String iDSymbol;
	@JsonProperty("totalQtty")
	public String totalQtty;
	@JsonProperty("MsgType")
	public String msgType;
	@JsonProperty("BodyLength")
	public String bodyLength;
	@JsonProperty("IDIndex")
	public String iDIndex;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = -5584442457508676192L;

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}