package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;
import lombok.ToString;

@Data
public class SC extends AbstractHoseUdpBroadcastMessage {
	private String systemControlCode;
	private int timestamp;

}
