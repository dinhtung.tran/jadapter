package com.fss.jadapter.data.model.hose.file.record;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fss.jadapter.data.model.hose.file.HosePrsFileRecord;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "noChange", "ceiling", "totalSharesAOM", "totalSharesPT", "totalValuesAOM", "index", "up", "time",
		"totalValuesPT", "floor", "down" })
@Data
public class IndexFileRecord implements HosePrsFileRecord {
	@JsonProperty("noChange")
	private Integer noChange;
	@JsonProperty("ceiling")
	private Integer ceiling;
	@JsonProperty("totalSharesAOM")
	private Double totalSharesAOM;
	@JsonProperty("totalSharesPT")
	private Double totalSharesPT;
	@JsonProperty("totalValuesAOM")
	private Double totalValuesAOM;
	@JsonProperty("index")
	private Integer index;
	@JsonProperty("up")
	private Integer up;
	@JsonProperty("time")
	private Integer time;
	@JsonProperty("totalValuesPT")
	private Double totalValuesPT;
	@JsonProperty("floor")
	private Integer floor;
	@JsonProperty("down")
	private Integer down;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("noChange")
	public Integer getNoChange() {
		return noChange;
	}

	@JsonProperty("noChange")
	public void setNoChange(Integer noChange) {
		this.noChange = noChange;
	}

	@JsonProperty("ceiling")
	public Integer getCeiling() {
		return ceiling;
	}

	@JsonProperty("ceiling")
	public void setCeiling(Integer ceiling) {
		this.ceiling = ceiling;
	}

	@JsonProperty("totalSharesAOM")
	public Double getTotalSharesAOM() {
		return totalSharesAOM;
	}

	@JsonProperty("totalSharesAOM")
	public void setTotalSharesAOM(Double totalSharesAOM) {
		this.totalSharesAOM = totalSharesAOM;
	}

	@JsonProperty("totalSharesPT")
	public Double getTotalSharesPT() {
		return totalSharesPT;
	}

	@JsonProperty("totalSharesPT")
	public void setTotalSharesPT(Double totalSharesPT) {
		this.totalSharesPT = totalSharesPT;
	}

	@JsonProperty("totalValuesAOM")
	public Double getTotalValuesAOM() {
		return totalValuesAOM;
	}

	@JsonProperty("totalValuesAOM")
	public void setTotalValuesAOM(Double totalValuesAOM) {
		this.totalValuesAOM = totalValuesAOM;
	}

	@JsonProperty("index")
	public Integer getIndex() {
		return index;
	}

	@JsonProperty("index")
	public void setIndex(Integer index) {
		this.index = index;
	}

	@JsonProperty("up")
	public Integer getUp() {
		return up;
	}

	@JsonProperty("up")
	public void setUp(Integer up) {
		this.up = up;
	}

	@JsonProperty("time")
	public Integer getTime() {
		return time;
	}

	@JsonProperty("time")
	public void setTime(Integer time) {
		this.time = time;
	}

	@JsonProperty("totalValuesPT")
	public Double getTotalValuesPT() {
		return totalValuesPT;
	}

	@JsonProperty("totalValuesPT")
	public void setTotalValuesPT(Double totalValuesPT) {
		this.totalValuesPT = totalValuesPT;
	}

	@JsonProperty("floor")
	public Integer getFloor() {
		return floor;
	}

	@JsonProperty("floor")
	public void setFloor(Integer floor) {
		this.floor = floor;
	}

	@JsonProperty("down")
	public Integer getDown() {
		return down;
	}

	@JsonProperty("down")
	public void setDown(Integer down) {
		this.down = down;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}
}
