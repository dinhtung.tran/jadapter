package com.fss.jadapter.data.model.hose.file;

import java.util.List;
import java.util.function.Consumer;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor(staticName = "of")
@ToString
public class HosePrsFilePackage<T extends HosePrsFileRecord> {
//	@Getter
//	@NonNull
//	private String key;
//	@Getter
//	@NonNull
//	private Class<T> recordType;
	
	@Getter
	private final List<T> records;
	
	
	public void addRecord(T t) {
		records.add(t);
	}
	
	public void forEach(Consumer<T> action) {
		records.forEach(action);
	}
	
	public T getLast() {
		if (records.isEmpty())
			return null;
		return records.get(records.size() - 1);
	}
}
