package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;

@Data
public class BS extends AbstractHoseUdpBroadcastMessage {
	private int firm;
	private String automatchHaltFlag;
	private String putthroughHaltFlag;	
}
