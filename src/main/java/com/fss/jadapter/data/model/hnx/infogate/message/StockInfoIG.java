package com.fss.jadapter.data.model.hnx.infogate.message;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fss.jadapter.data.model.hnx.infogate.AbstractHnxInfogateMessage;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "IDSymbol", "DateNo", "MatchPrice", "MatchQtty", "Symbol", "TotalListingQtty", "BestBidPrice",
		"BestOfferPrice", "TotalBidQtty", "TotalOfferQtty", "OpenPrice", "PriorOpenPrice", "PriorClosePrice",
		"SecurityType", "IssueDate", "ReferenceStatus", "BasicPrice", "HighestPrice", "MatchValue", "OfferCount",
		"BidCount", "SecurityTradingStatus", "ListingStatus", "CeilingPrice", "FloorPrice", "Parvalue",
		"TradingSessionID", "TradSesStatus", "TotalVolumeTraded", "Tradingdate", "NM_TotalTradedQtty",
		"NM_TotalTradedValue", "TotalBuyTradingQtty", "TotalSellTradingQtty", "BuyForeignQtty", "Time", "TradingUnit",
		"BoardCode", "BestBidQtty", "BestOfferQtty", "TotalBidQtty_OD", "TotalOfferQtty_OD", "LowestPrice",
		"RemainForeignQtty", "TotalValueTraded", "BuyCount", "TotalBuyTradingValue", "SellCount",
		"TotalSellTradingValue", "BuyForeignValue", "BeginString", "BodyLength", "MsgType", "SenderCompID",
		"SendingTime" })
@Data
public class StockInfoIG extends AbstractHnxInfogateMessage implements Serializable {

	@JsonProperty("IDSymbol")
	public String iDSymbol;
	@JsonProperty("DateNo")
	public String dateNo;
	@JsonProperty("MatchPrice")
	public String matchPrice;
	@JsonProperty("MatchQtty")
	public String matchQtty;
	@JsonProperty("Symbol")
	public String symbol;
	@JsonProperty("TotalListingQtty")
	public String totalListingQtty;
	@JsonProperty("BestBidPrice")
	public String bestBidPrice;
	@JsonProperty("BestOfferPrice")
	public String bestOfferPrice;
	@JsonProperty("TotalBidQtty")
	public String totalBidQtty;
	@JsonProperty("TotalOfferQtty")
	public String totalOfferQtty;
	@JsonProperty("OpenPrice")
	public String openPrice;
	@JsonProperty("PriorOpenPrice")
	public String priorOpenPrice;
	@JsonProperty("PriorClosePrice")
	public String priorClosePrice;
	@JsonProperty("SecurityType")
	public String securityType;
	@JsonProperty("IssueDate")
	public String issueDate;
	@JsonProperty("ReferenceStatus")
	public String referenceStatus;
	@JsonProperty("BasicPrice")
	public String basicPrice;
	@JsonProperty("HighestPrice")
	public String highestPrice;
	@JsonProperty("MatchValue")
	public String matchValue;
	@JsonProperty("OfferCount")
	public String offerCount;
	@JsonProperty("BidCount")
	public String bidCount;
	@JsonProperty("SecurityTradingStatus")
	public String securityTradingStatus;
	@JsonProperty("ListingStatus")
	public String listingStatus;
	@JsonProperty("CeilingPrice")
	public String ceilingPrice;
	@JsonProperty("FloorPrice")
	public String floorPrice;
	@JsonProperty("Parvalue")
	public String parvalue;
	@JsonProperty("TradingSessionID")
	public String tradingSessionID;
	@JsonProperty("TradSesStatus")
	public String tradSesStatus;
	@JsonProperty("TotalVolumeTraded")
	public String totalVolumeTraded;
	@JsonProperty("Tradingdate")
	public String tradingdate;
	@JsonProperty("NM_TotalTradedQtty")
	public String nMTotalTradedQtty;
	@JsonProperty("NM_TotalTradedValue")
	public String nMTotalTradedValue;
	@JsonProperty("TotalBuyTradingQtty")
	public String totalBuyTradingQtty;
	@JsonProperty("TotalSellTradingQtty")
	public String totalSellTradingQtty;
	@JsonProperty("BuyForeignQtty")
	public String buyForeignQtty;
	@JsonProperty("Time")
	public String time;
	@JsonProperty("TradingUnit")
	public String tradingUnit;
	@JsonProperty("BoardCode")
	public String boardCode;
	@JsonProperty("BestBidQtty")
	public String bestBidQtty;
	@JsonProperty("BestOfferQtty")
	public String bestOfferQtty;
	@JsonProperty("TotalBidQtty_OD")
	public String totalBidQttyOD;
	@JsonProperty("TotalOfferQtty_OD")
	public String totalOfferQttyOD;
	@JsonProperty("LowestPrice")
	public String lowestPrice;
	@JsonProperty("RemainForeignQtty")
	public String remainForeignQtty;
	@JsonProperty("TotalValueTraded")
	public String totalValueTraded;
	@JsonProperty("BuyCount")
	public String buyCount;
	@JsonProperty("TotalBuyTradingValue")
	public String totalBuyTradingValue;
	@JsonProperty("SellCount")
	public String sellCount;
	@JsonProperty("TotalSellTradingValue")
	public String totalSellTradingValue;
	@JsonProperty("BuyForeignValue")
	public String buyForeignValue;
	@JsonProperty("BeginString")
	public String beginString;
	@JsonProperty("BodyLength")
	public String bodyLength;
	@JsonProperty("MsgType")
	public String msgType;
	@JsonProperty("SenderCompID")
	public String senderCompID;
	@JsonProperty("SendingTime")
	public String sendingTime;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = -6430466777834056558L;

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}