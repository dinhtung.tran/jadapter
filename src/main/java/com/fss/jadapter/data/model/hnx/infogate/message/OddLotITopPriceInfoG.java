package com.fss.jadapter.data.model.hnx.infogate.message;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fss.jadapter.data.model.hnx.infogate.AbstractHnxInfogateMessage;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "Symbol", "BestBidPrice", "BestOfferPrice", "TotalBidQtty", "TotalOfferQtty", "NoMiscFees",
		"OpenPrice", "BoardCode", "BestBidQtty", "BestOfferQtty", "TotalBidQtty_OD", "TotalOfferQtty_OD", "BeginString",
		"BodyLength", "MsgType", "SenderCompID", "SendingTime" })
public class OddLotITopPriceInfoG extends AbstractHnxInfogateMessage implements Serializable {

	@JsonProperty("Symbol")
	public String symbol;
	@JsonProperty("BestBidPrice")
	public String bestBidPrice;
	@JsonProperty("BestOfferPrice")
	public String bestOfferPrice;
	@JsonProperty("TotalBidQtty")
	public String totalBidQtty;
	@JsonProperty("TotalOfferQtty")
	public String totalOfferQtty;
	@JsonProperty("NoMiscFees")
	public String noMiscFees;
	@JsonProperty("OpenPrice")
	public String openPrice;
	@JsonProperty("BoardCode")
	public String boardCode;
	@JsonProperty("BestBidQtty")
	public String bestBidQtty;
	@JsonProperty("BestOfferQtty")
	public String bestOfferQtty;
	@JsonProperty("TotalBidQtty_OD")
	public String totalBidQttyOD;
	@JsonProperty("TotalOfferQtty_OD")
	public String totalOfferQttyOD;
	@JsonProperty("BeginString")
	public String beginString;
	@JsonProperty("BodyLength")
	public String bodyLength;
	@JsonProperty("MsgType")
	public String msgType;
	@JsonProperty("SenderCompID")
	public String senderCompID;
	@JsonProperty("SendingTime")
	public String sendingTime;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = 1639605869188588696L;

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}