package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;
import lombok.ToString;

@Data
public class GA extends AbstractHoseUdpBroadcastMessage {
	private int adminMessageLength;
	private String adminMessageText;

}
