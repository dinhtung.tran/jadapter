package com.fss.jadapter.data.model.hnx.infogate.message;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fss.jadapter.data.model.hnx.infogate.AbstractHnxInfogateMessage;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "IDSymbol", "DateNo", "MatchPrice", "MatchQtty", "Symbol", "BestBidPrice", "BestOfferPrice",
		"TotalBidQtty", "TotalOfferQtty", "OpenPrice", "PriorOpenPrice", "PriorClosePrice", "SecurityType",
		"BasicPrice", "HighestPrice", "MatchValue", "OfferCount", "BidCount", "SecurityTradingStatus", "ListingStatus",
		"CeilingPrice", "FloorPrice", "TradingSessionID", "TradSesStatus", "TotalVolumeTraded", "Tradingdate",
		"NM_TotalTradedQtty", "NM_TotalTradedValue", "BuyForeignQtty", "SellForeignQtty", "Time", "TradingUnit",
		"BoardCode", "Underlying", "OpenInterest", "FirstTradingDate", "LastTradingDate", "OpenQtty",
		"NM_BuyForeignQtty", "NM_SellForeignQtty", "BestBidQtty", "BestOfferQtty", "LowestPrice", "TotalValueTraded",
		"BuyForeignValue", "SellForeignValue", "NM_BuyForeignValue", "NM_SellForeignValue", "BeginString", "BodyLength",
		"MsgType", "SenderCompID", "SendingTime" })
public class DerivativeInfoIG extends AbstractHnxInfogateMessage implements Serializable {

	@JsonProperty("IDSymbol")
	public String iDSymbol;
	@JsonProperty("DateNo")
	public String dateNo;
	@JsonProperty("MatchPrice")
	public String matchPrice;
	@JsonProperty("MatchQtty")
	public String matchQtty;
	@JsonProperty("Symbol")
	public String symbol;
	@JsonProperty("BestBidPrice")
	public String bestBidPrice;
	@JsonProperty("BestOfferPrice")
	public String bestOfferPrice;
	@JsonProperty("TotalBidQtty")
	public String totalBidQtty;
	@JsonProperty("TotalOfferQtty")
	public String totalOfferQtty;
	@JsonProperty("OpenPrice")
	public String openPrice;
	@JsonProperty("PriorOpenPrice")
	public String priorOpenPrice;
	@JsonProperty("PriorClosePrice")
	public String priorClosePrice;
	@JsonProperty("SecurityType")
	public String securityType;
	@JsonProperty("BasicPrice")
	public String basicPrice;
	@JsonProperty("HighestPrice")
	public String highestPrice;
	@JsonProperty("MatchValue")
	public String matchValue;
	@JsonProperty("OfferCount")
	public String offerCount;
	@JsonProperty("BidCount")
	public String bidCount;
	@JsonProperty("SecurityTradingStatus")
	public String securityTradingStatus;
	@JsonProperty("ListingStatus")
	public String listingStatus;
	@JsonProperty("CeilingPrice")
	public String ceilingPrice;
	@JsonProperty("FloorPrice")
	public String floorPrice;
	@JsonProperty("TradingSessionID")
	public String tradingSessionID;
	@JsonProperty("TradSesStatus")
	public String tradSesStatus;
	@JsonProperty("TotalVolumeTraded")
	public String totalVolumeTraded;
	@JsonProperty("Tradingdate")
	public String tradingdate;
	@JsonProperty("NM_TotalTradedQtty")
	public String nMTotalTradedQtty;
	@JsonProperty("NM_TotalTradedValue")
	public String nMTotalTradedValue;
	@JsonProperty("BuyForeignQtty")
	public String buyForeignQtty;
	@JsonProperty("SellForeignQtty")
	public String sellForeignQtty;
	@JsonProperty("Time")
	public String time;
	@JsonProperty("TradingUnit")
	public String tradingUnit;
	@JsonProperty("BoardCode")
	public String boardCode;
	@JsonProperty("Underlying")
	public String underlying;
	@JsonProperty("OpenInterest")
	public String openInterest;
	@JsonProperty("FirstTradingDate")
	public String firstTradingDate;
	@JsonProperty("LastTradingDate")
	public String lastTradingDate;
	@JsonProperty("OpenQtty")
	public String openQtty;
	@JsonProperty("NM_BuyForeignQtty")
	public String nMBuyForeignQtty;
	@JsonProperty("NM_SellForeignQtty")
	public String nMSellForeignQtty;
	@JsonProperty("BestBidQtty")
	public String bestBidQtty;
	@JsonProperty("BestOfferQtty")
	public String bestOfferQtty;
	@JsonProperty("LowestPrice")
	public String lowestPrice;
	@JsonProperty("TotalValueTraded")
	public String totalValueTraded;
	@JsonProperty("BuyForeignValue")
	public String buyForeignValue;
	@JsonProperty("SellForeignValue")
	public String sellForeignValue;
	@JsonProperty("NM_BuyForeignValue")
	public String nMBuyForeignValue;
	@JsonProperty("NM_SellForeignValue")
	public String nMSellForeignValue;
	@JsonProperty("BeginString")
	public String beginString;
	@JsonProperty("BodyLength")
	public String bodyLength;
	@JsonProperty("MsgType")
	public String msgType;
	@JsonProperty("SenderCompID")
	public String senderCompID;
	@JsonProperty("SendingTime")
	public String sendingTime;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();
	private final static long serialVersionUID = -1126137754269720290L;

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}