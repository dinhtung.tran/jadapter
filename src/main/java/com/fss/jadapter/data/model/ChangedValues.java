package com.fss.jadapter.data.model;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

import lombok.AccessLevel;
import lombok.Generated;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@RequiredArgsConstructor(staticName = "of")
@ToString(includeFieldNames = true)
public class ChangedValues {
	
	public static final String TYPE_SECURITY = "security";
	public static final String TYPE_INDEX = "index";
	public static final String TYPE_MARKET = "market";
	public static final String TYPE_TRANS = "translog";
	
	@NonNull
	@Getter
	@ToString.Include
	private final String key;
	
	@NonNull
	@Getter
	@ToString.Include
	private final String type;
	
	@ToString.Include
	private Map<String, String> repo = new HashMap<>();
	
	@Generated
	@Setter(AccessLevel.NONE)
	@ToString.Include
	private long version;
		
	public void put(String field, String value) {
		repo.put(field, value);
	}
	
	public void put(String field, int value) {
		repo.put(field, String.valueOf(value));
	}
	
	public void put(String field, long value) {
		repo.put(field, String.valueOf(value));
	}
	
	public void put(String field, Object value) {
		repo.put(field, String.valueOf(value));
	}
	
	public String get(String field) {
		return repo.get(field);
	}	
	
	public boolean isEmpty() {
		return repo.isEmpty();
	}
	
	public int size() {
		return repo.size();
	}
	
	public void forEach(BiConsumer<String, String> action) {
		repo.forEach(action);
	}
	
	public boolean contains(String key) {
		return repo.containsKey(key);
	}
	
	public void putAll(ChangedValues other) {
		repo.putAll(other.repo);
	}
}
