package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;

@Data
public class TS extends AbstractHoseUdpBroadcastMessage {
	private int timestamp;
}