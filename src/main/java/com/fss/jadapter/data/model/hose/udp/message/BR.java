package com.fss.jadapter.data.model.hose.udp.message;

import lombok.Data;

@Data
public class BR extends AbstractHoseUdpBroadcastMessage{
	private int firm;
	private String marketID;
	private String volumeSold;
	private String valueSold;
	private String volumeBought;
	private String valueBought;
}
