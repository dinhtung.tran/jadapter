package com.fss.jadapter.data.repo.file;

import java.util.function.BiConsumer;

import org.springframework.stereotype.Component;

@Component
public class HoseSecurityNumberFileRepository extends AbstractFileRepository {
	
	@Override
	protected String getPath() {
		return "/hose_sec_num";
	}

}
