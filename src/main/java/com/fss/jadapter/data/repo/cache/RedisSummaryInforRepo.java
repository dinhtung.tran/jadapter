package com.fss.jadapter.data.repo.cache;

import java.util.HashSet;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

import com.fss.jadapter.data.model.ChangedValues;

import lombok.AccessLevel;
import lombok.Getter;

@Component
public class RedisSummaryInforRepo {
	private final static BlockingQueue<String> NEW_SEC_INFOR_QUEUE = new LinkedBlockingQueue<>();
	private final static BlockingQueue<String> NEW_INX_INFOR_QUEUE = new LinkedBlockingQueue<>();
	private final static BlockingQueue<String> NEW_TRANS_INFOR_QUEUE = new LinkedBlockingQueue<>();
	private final static BlockingQueue<String> NEW_BASKET_INFOR_QUEUE = new LinkedBlockingQueue<>();
	private final static BlockingQueue<String> NEW_REF_INFOR_QUEUE = new LinkedBlockingQueue<>();

	public enum InforType {
		SECURITY(NEW_SEC_INFOR_QUEUE),
		INDEX(NEW_INX_INFOR_QUEUE),
		BASKET(NEW_BASKET_INFOR_QUEUE),
		TRANSLOG(NEW_TRANS_INFOR_QUEUE), 
		REF(NEW_REF_INFOR_QUEUE)
		;
	
		@Getter(AccessLevel.PRIVATE)
		private Queue<String> queue;

		InforType(Queue<String> queue) {
			this.queue = queue;
		}
		
		public Stream<String> pollAll() {
			Set<String> set = new HashSet<>();
			while (!queue.isEmpty()) {
				String key = queue.poll();
				if (key != null)
					set.add(key);
			}
			return set.stream();
		}
	}
	
	public void addRealTimeChange(String input) {
		String[] arr = parseKey(input);
		String key = arr[0];
		String type = arr[1];
		
		InforType inforType = null;
		switch (type) {
		case ChangedValues.TYPE_SECURITY: {
			inforType = InforType.SECURITY;
			break;
		}
		case ChangedValues.TYPE_INDEX:
//		case ChangedValues.TYPE_MARKET: 
		{
			inforType = InforType.INDEX;
			break;
		}
		case ChangedValues.TYPE_TRANS: {
			inforType = InforType.SECURITY;
			break;
		}
		}
		summary(inforType, key);
	}
	
	public void summary(InforType inforType, String key) {
		if (inforType != null)
			inforType.getQueue().add(key);
	}

	private String[] parseKey(String input) {
		return input.split("_");
	}
	
//	public Stream<String> takeNewSecInforStream() {
//		Set<String> set = new HashSet<>();
//		while (!NEW_SEC_INFOR_QUEUE.isEmpty()) {
//			String key = NEW_SEC_INFOR_QUEUE.poll();
//			if (key != null)
//				set.add(key);
//		}
//		return set.stream();
//	}
//	
//	public Stream<String> takeNewIdxInforStream() {
//		Set<String> set = new HashSet<>();
//		while (!NEW_INX_INFOR_QUEUE.isEmpty()) {
//			String key = NEW_INX_INFOR_QUEUE.poll();
//			if (key != null)
//				set.add(key);
//		}
//		return set.stream();
//	}
//	
//	public Stream<String> takeNewTransInforStream() {
//		Set<String> set = new HashSet<>();
//		while (!NEW_TRANS_INFOR_QUEUE.isEmpty()) {
//			String key = NEW_TRANS_INFOR_QUEUE.poll();
//			if (key != null)
//				set.add(key);
//		}
//		return set.stream();
//	}
//	
//	public Stream<String> takeNewBasketInforStream() {
//		Set<String> set = new HashSet<>();
//		while (!NEW_BASKET_INFOR_QUEUE.isEmpty()) {
//			String key = NEW_BASKET_INFOR_QUEUE.poll();
//			if (key != null)
//				set.add(key);
//		}
//		return set.stream();
//	}
}
