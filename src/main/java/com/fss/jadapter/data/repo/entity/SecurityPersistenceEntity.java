package com.fss.jadapter.data.repo.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SecurityPersistenceEntity {
	private int securityNumber;
	private int sectorNumber;
	private String securitySymbol;
	private String securityType;
	private int ceilingPrice;
	private int floorPrice;
	private int lastSalePrice;
	private String marketId;
	private String securityName;
	private String suspension;
	private String delist;
	private String haltResumeFlag;
	private String split;
	private String benefit;
	private String meeting;
	private String notice;
	private String clientIdRequired;
	private int parValue;
	private String sdcFlag;
	private int priorClosePrice;
	private String priorCloseDate;
	private int openPrice;
	private int highestPrice;
	private int lowestPrice;
	private int totalSharesTraded;
	private int totalValuesTraded;
	private int boardLot;
	private String underlyingSymbol;
	private String issuerName;
	private String coveredWarrantType;
	private String maturityDate;
	private String lastTradingDate;
	private int exercisePrice;
	private String exerciseRatio;
	private int listedShare;
}
