package com.fss.jadapter.data.repo.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Stream;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractFileRepository implements FileRepository {
	
	private static final String DATA_PATH = "./data";
	private Map<String, Object> lockerMap = new HashMap<>();
	
	public void writeData(String key, byte[] data) {
		Object locker = getLocker(key);
		synchronized (locker) {
			try {
				Path path = getPath(key);
				Files.createDirectories(path.getParent());
				Files.write(path, data, StandardOpenOption.WRITE, StandardOpenOption.CREATE);
			} catch (IOException e) {
				log.error("", e);
			}
		}
	}
	
	public void readData(String key, Consumer<byte[]> consumer) {
		Object locker = getLocker(key);
		synchronized (locker) {
			try {
				Path path = getPath(key);
				if (path.toFile().exists())
					consumer.accept(Files.readAllBytes(path));
			} catch (IOException e) {
				log.error("key: " + key, e);
			}
		}	
	}

	@Override
	public void readAddData(BiConsumer<String, byte[]> biConsumer) {
		File[] files = Paths.get(DATA_PATH, getPath()).toFile().listFiles();
		if (files == null)
			return;
		Stream.of(files).forEach((file) -> {
			if (file.isDirectory()) {
				return;
			} else {
				String key = file.getName();
				readData(key, (data) -> {
					biConsumer.accept(key, data);
				});
			}
		});
					
	}

	private Path getPath(String key) {
		return Paths.get(DATA_PATH, getPath(), key);
	}

	private Object getLocker(String key) {
		if (!lockerMap.containsKey(key) || lockerMap.get(key) == null) {
			lockerMap.put(key, new Object());
		}
		return lockerMap.get(key);
	}

	protected abstract String getPath();
}
