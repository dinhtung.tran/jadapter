package com.fss.jadapter.data.repo.cache;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

import com.fss.jadapter.constant.BigDecimalConstant;

import quickfix.field.UnderlyingSymbol;

@Component
public class CwCachedMemory {
	
	private static final Map<String, Set<String>> REPO = new HashMap<>();
	
	public void addCw(String cwSymbol, String underlyingSymbol) {
		getSymbols(underlyingSymbol).add(cwSymbol);
	}
	
	public String getUnderlyingSymbol(String cwSymbol) {
		if (cwSymbol == null || cwSymbol.length() < 4)
			return "";
		String underlyingSymbol = cwSymbol.substring(1, 4);
		if (!REPO.containsKey(underlyingSymbol))
			return "";
		else {
			return underlyingSymbol;
		}
	}
	
	public Stream<String> cwSymbolsBy(String underlyingSymbol) {
		if (!REPO.containsKey(underlyingSymbol))
			return Stream.empty();
		return getSymbols(underlyingSymbol).stream();
	}
	
	private Set<String> getSymbols(String underlyingSymbol) {
		if (!REPO.containsKey(underlyingSymbol)) {
			REPO.put(underlyingSymbol, new HashSet<>());
		}
		return REPO.get(underlyingSymbol);
	}

	public BigDecimal caculateBasis(BigDecimal currentPrice, BigDecimal exercisePrice, BigDecimal exerciseRatio) {
		return currentPrice.subtract(exercisePrice.divide(BigDecimalConstant.HUNDRED)).divide(exerciseRatio, 2, RoundingMode.HALF_UP);
	}

	public BigDecimal caculateExerciseRatio(String exerciseRatio) {
		String[] arr = exerciseRatio.trim().split(":");
		BigDecimal num = new BigDecimal(arr[0]);
		BigDecimal den = new BigDecimal(arr[1]);
		return num.divide(den, MathContext.DECIMAL32);
	}

	public BigDecimal caculateBalancePrice(BigDecimal currentPrice, BigDecimal exercisePrice,
			BigDecimal exerciseRatio) {
		return exercisePrice.divide(BigDecimalConstant.TEN).add(currentPrice.multiply(exerciseRatio));
	}

}
