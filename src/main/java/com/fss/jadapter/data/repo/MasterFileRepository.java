package com.fss.jadapter.data.repo;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fss.jadapter.data.model.hose.udp.message.SU;
import com.fss.jadapter.data.persistence.model.v2.security.SecurityInfor;
import com.fss.jadapter.data.repo.entity.SecurityPersistenceEntity;
import com.fss.jadapter.data.repo.file.HoseSecurityNumberFileRepository;
import com.fss.jadapter.data.repo.file.SecurityFileRepository;
import com.fss.utilities.array.ByteArrayU;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class MasterFileRepository implements InitializingBean{
	
	private static final ObjectMapper MAPPER = new ObjectMapper();
	private static final Map<Integer, String> HOSE_SEC_NUM_MAP = new HashMap<>();

	@Autowired
	private SecurityFileRepository securityFileRepository;
	@Autowired
	private HoseSecurityNumberFileRepository hoseSecurityNumberFileRepository;
	
	public void saveSU(SU su) {
		log.info("Save SU: {}", su);
		int id = su.getSecurityNumberNew();
		
		SecurityPersistenceEntity securityJpaInfor = new SecurityPersistenceEntity();
		securityJpaInfor.setSecurityNumber(id);
		securityJpaInfor.setSecuritySymbol(su.getSecuritySymbol());
		securityJpaInfor.setCeilingPrice(su.getCeilingPrice());
		securityJpaInfor.setFloorPrice(su.getFloorPrice());
		securityJpaInfor.setPriorClosePrice(su.getPriorClosePrice());
		securityJpaInfor.setPriorCloseDate(su.getPriorCloseDate());
		securityJpaInfor.setSecurityType(su.getSecurityType());
		securityJpaInfor.setBenefit(su.getBenefit());
		securityJpaInfor.setSplit(su.getSplit());
		securityJpaInfor.setCoveredWarrantType(su.getCoveredWarrantType());
		securityJpaInfor.setExerciseRatio(su.getExerciseRatio());
		securityJpaInfor.setUnderlyingSymbol(su.getUnderlyingSymbol());
		securityJpaInfor.setLastSalePrice(su.getLastSalePrice());
		securityJpaInfor.setLastTradingDate(su.getLastTradingDate());

		try {
			hoseSecurityNumberFileRepository.writeData(String.valueOf(id), ByteArrayU.getBytes(securityJpaInfor.getSecuritySymbol()));
			securityFileRepository.writeData(securityJpaInfor.getSecuritySymbol(), MAPPER.writerWithDefaultPrettyPrinter().writeValueAsBytes(securityJpaInfor));
		} catch (JsonProcessingException e) {
			log.error("Error: saveSU SU: " + su, e);
		}
	}

	public String getSymbol(int secNum) {
		return HOSE_SEC_NUM_MAP.computeIfAbsent(Integer.valueOf(secNum), (k) -> {
			StringBuilder sb = new StringBuilder();
			hoseSecurityNumberFileRepository.readData(String.valueOf(secNum), (data) -> sb.append(new String(data)));
			String rs = sb.toString();
			return rs == null || rs.trim().isEmpty() ? null : rs;
		});		
	}

	public void processEndOfDay(Stream<SecurityInfor> streamAllSecurityInfor) {
		
	}

	public Stream<SecurityPersistenceEntity> getAllSecurityJpaInfor() {
		Stream.Builder<SecurityPersistenceEntity> builder = Stream.builder();
		securityFileRepository.readAddData((key, data) -> {
			try {
				builder.accept(MAPPER.readValue(data, SecurityPersistenceEntity.class));
			} catch (IOException e) {
				log.error(String.format("key: %s data: %s", key, new String(data)), e);
			} catch (Exception e) {
				log.error(String.format("key: %s data: %s", key, new String(data)), e);
			}
		});
		return builder.build();
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		log.info("init HOSE_SEC_NUM_MAP!");
		hoseSecurityNumberFileRepository.readAddData((key, data) -> {
			try {
				log.info("HOSE_SEC_NUM_MAP: put key: {} value: {}", key, ByteArrayU.toString(data));
				HOSE_SEC_NUM_MAP.putIfAbsent(Integer.valueOf(key), ByteArrayU.toString(data));
			} catch (Exception e) {
				log.error(String.format("key: %s data: %s", key, new String(data)), e);
			}
		});
	}
}
