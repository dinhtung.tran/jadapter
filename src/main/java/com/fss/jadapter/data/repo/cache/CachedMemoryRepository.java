package com.fss.jadapter.data.repo.cache;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fss.jadapter.constant.AlgoMessageFields;
import com.fss.jadapter.constant.BigDecimalConstant;
import com.fss.jadapter.constant.HnxBoardCode;
import com.fss.jadapter.constant.HoseSystemControlCode;
import com.fss.jadapter.constant.MilestoneLocalTime;
import com.fss.jadapter.constant.PublishEntityType;
import com.fss.jadapter.constant.TickSize;
import com.fss.jadapter.constant.TimeConstant;
import com.fss.jadapter.dao.algo.StockBoardDataAccess;
import com.fss.jadapter.data.model.ChangedValues;
import com.fss.jadapter.data.model.hose.file.record.BasketFileRecord;
import com.fss.jadapter.data.model.hose.file.record.IndexFileRecord;
import com.fss.jadapter.data.model.hose.udp.message.DC;
import com.fss.jadapter.data.model.hose.udp.message.IU;
import com.fss.jadapter.data.model.hose.udp.message.LS;
import com.fss.jadapter.data.model.hose.udp.message.PD;
import com.fss.jadapter.data.model.hose.udp.message.PO;
import com.fss.jadapter.data.model.hose.udp.message.SC;
import com.fss.jadapter.data.model.hose.udp.message.SS;
import com.fss.jadapter.data.model.hose.udp.message.SU;
import com.fss.jadapter.data.model.hose.udp.message.TP;
import com.fss.jadapter.data.model.hose.udp.message.TR;
import com.fss.jadapter.data.model.system.InitDaySignal;
import com.fss.jadapter.data.model.worker.AlgoMessage;
import com.fss.jadapter.data.model.worker.RefInfor;
import com.fss.jadapter.data.persistence.model.v2.market.BasketInfor;
import com.fss.jadapter.data.persistence.model.v2.market.IndexInfor;
import com.fss.jadapter.data.persistence.model.v2.market.MarketInfor;
import com.fss.jadapter.data.persistence.model.v2.security.AdvancedInfor;
import com.fss.jadapter.data.persistence.model.v2.security.DynamicInfor;
import com.fss.jadapter.data.persistence.model.v2.security.ForeignInfor;
import com.fss.jadapter.data.persistence.model.v2.security.MatchedOrderInfor;
import com.fss.jadapter.data.persistence.model.v2.security.ProjectedOpen;
import com.fss.jadapter.data.persistence.model.v2.security.SecurityInfor;
import com.fss.jadapter.data.persistence.model.v2.security.SecurityType;
import com.fss.jadapter.data.persistence.model.v2.security.StaticInfor;
import com.fss.jadapter.data.persistence.model.v2.security.TopPrice;
import com.fss.jadapter.data.process.persistence.minor.AlgoSubscribeMessageProcessor;
import com.fss.jadapter.data.process.system.SystemSignalProcessor;
import com.fss.jadapter.data.repo.MasterFileRepository;
import com.fss.jadapter.data.repo.cache.RedisSummaryInforRepo.InforType;
import com.fss.jadapter.data.repo.redis.RedisRefInforRepository;
import com.fss.jadapter.model.MultiMessagePublishObject;
import com.fss.utilities.DerivativeU;
import com.fss.utilities.DerivativeU.UnderlyingSymbol;

import lombok.extern.slf4j.Slf4j;
import quickfix.FieldMap;
import quickfix.FieldNotFound;
import quickfix.Group;
import quickfix.field.BasicPrice;
import quickfix.field.BestBidPrice;
import quickfix.field.BestBidQtty;
import quickfix.field.BestOfferPrice;
import quickfix.field.BestOfferQtty;
import quickfix.field.BoardCode;
import quickfix.field.BuyForeignQtty;
import quickfix.field.CalTime;
import quickfix.field.CeilingPrice;
import quickfix.field.Change;
import quickfix.field.CurrentPrice;
import quickfix.field.FloorPrice;
import quickfix.field.HighestPrice;
import quickfix.field.IndexCode;
import quickfix.field.LowestPrice;
import quickfix.field.MatchPrice;
import quickfix.field.NM_TotalTradedQtty;
import quickfix.field.NM_TotalTradedValue;
import quickfix.field.NoTopPrice;
import quickfix.field.NumSymbolAdvances;
import quickfix.field.NumSymbolDeclines;
import quickfix.field.NumSymbolNochange;
import quickfix.field.NumTopPrice;
import quickfix.field.OpenInterest;
import quickfix.field.OpenPrice;
import quickfix.field.PriorIndexVal;
import quickfix.field.RatioChange;
import quickfix.field.RemainForeignQtty;
import quickfix.field.SellForeignQtty;
import quickfix.field.Symbol;
import quickfix.field.Time;
import quickfix.field.TotalBidQtty;
import quickfix.field.TotalNormalTradedQttyOd;
import quickfix.field.TotalNormalTradedQttyRd;
import quickfix.field.TotalNormalTradedValueOd;
import quickfix.field.TotalNormalTradedValueRd;
import quickfix.field.TotalOfferQtty;
import quickfix.field.TotalPTTradedQtty;
import quickfix.field.TotalPTTradedValue;
import quickfix.field.TotalQtty;
import quickfix.field.TotalTrade;
import quickfix.field.TotalValue;
import quickfix.field.TradSesStatus;
import quickfix.field.TradingDate;
import quickfix.field.Value;
import quickfix.fixig.BasketInfo;
import quickfix.fixig.BoardInfo;
import quickfix.fixig.DerivativesInfo;
import quickfix.fixig.IndexInfo;
import quickfix.fixig.StockInfo;
import quickfix.fixig.TopNPrice;

@Component
@Slf4j
public class CachedMemoryRepository implements InitializingBean {
	
	private static final Logger PT_MATCH_LOG = LoggerFactory.getLogger("PutThroughLog");
	
	private static final String TIME_OF_DAY_PATTERN = "HH:mm:ss";
	private static final DateFormat MATCHED_ORDER_TIME_FORMATER = new SimpleDateFormat(TIME_OF_DAY_PATTERN);
	private static final int ONE_MILLION = 1000000;
	private static final int THOUSAND = 1000;
	private static final int TEN = 10;
	private static final int HUNDRED = 100;
	
	@org.springframework.beans.factory.annotation.Value("${hose.prs.file.index.list}") 
	private String indexListStr;
	@Autowired
	private MasterFileRepository masterJpaRepository;
	@Autowired
	private AlgoSubscribeMessageProcessor algoSubscribeMessageProcessor;
	@Autowired
	private StockBoardDataAccess stockBoardDataAccess;
	@Autowired
	private CwCachedMemory cwCachedMemory;
	@Autowired
	private RedisSummaryInforRepo redisSummaryInforRepo;
	@Autowired
	private SystemSignalProcessor systemSignalProcessor;
	@Autowired
	private RedisRefInforRepository redisRefInforRepository;
	
	private Timer manualCalculateIndexTimer;
	
	private static final Map<String, SecurityInfor> SECURITY_INFOR_REPO = new ConcurrentHashMap<>();
	private static final Map<String, MarketInfor> MARKET_INFOR_REPO = new ConcurrentHashMap<>();
	private static final Map<String, IndexInfor> INDEX_INFOR_REPO = new ConcurrentHashMap<>();
	private static final Map<String, BasketInfor> BASKET_INFOR_REPO = new ConcurrentHashMap<>();
	
	private static final Map<String, RefInfor> REF_INFOR_REPO = new ConcurrentHashMap<>();
	private static final Map<String, MultiMessagePublishObject> TRANSLOG_PUBLISH_OBJECT_REPO = new ConcurrentHashMap<>();
	
	public Stream<ChangedValues> processInfor(String key, BasketFileRecord message) {
		log.info("processInfor_BasketFileRecord, key {} message {}", key, message);
		Stream.Builder<ChangedValues> builder = Stream.builder();
		
		String basketId = BasketInfor.getBasketId(key);
		BasketInfor infor = getBasketInfor(basketId);
		String symbol = message.getSymbol();
		if (symbol != null)
			infor.add(symbol.trim());
		
		ChangedValues cv = ChangedValues.of(basketId, ChangedValues.TYPE_INDEX);
		builder.accept(cv);
		
		return builder.build();
	}
	
	public Stream<ChangedValues> processInfor(String key, IndexFileRecord message) {
		log.info("processInfor_IndexFileRecord, key {} message {}", key, message);
		if (manualCalculateIndexTimer != null) {
			log.info("processInfor_IndexFileRecord, manualCalculateIndexTimer is processing! Skip Key {} message {}", key, message);
			return Stream.empty();
		}
		Stream.Builder<ChangedValues> builder = Stream.builder();
		
		String indexId = IndexInfor.getIndexId(key);
		
		IndexInfor current = getIndexInfor(indexId);
		current.setMarketId(MarketInfor.HOSE);
		IndexInfor newInfor = current.clone();
		BasketInfor basketInfor = getBasketInfor(indexId);
		
		BigDecimal indexValue = BigDecimal.valueOf(message.getIndex()).divide(BigDecimal.valueOf(100));
		int advances = message.getUp();
		int declines = message.getDown();
		int noChange = message.getNoChange();
		
		long totalValuePT = (long) (message.getTotalValuesPT() * ONE_MILLION);
		long totalVolumePT = (long) (message.getTotalSharesPT() + 0);
		
		long totalValue = (long) (message.getTotalValuesAOM() + message.getTotalValuesPT()) * ONE_MILLION - totalValuePT;
		long totalVolume = (long) (message.getTotalSharesAOM() + message.getTotalSharesPT()) - totalVolumePT;
		
		BigDecimal priorIndex = newInfor.getPriorIndex();
		
		if (priorIndex != null && priorIndex.compareTo(BigDecimal.ZERO) != 0) {
			newInfor.setChange(indexValue.subtract(priorIndex));
			newInfor.setChangeRate(newInfor.getChange().multiply(BigDecimalConstant.HUNDRED).divide(priorIndex, 2, RoundingMode.HALF_UP));
			log.info("processInfor_IndexFileRecord: priorIndex {} indexValue {} change {} rate {}", priorIndex, indexValue, newInfor.getChange(), newInfor.getChangeRate());
		} else {
			log.info("processInfor_IndexFileRecord: priorIndex is null or zero {} indexValue {}", priorIndex, indexValue);
		}
		
		newInfor.setIndexValue(indexValue);
		newInfor.setAdvances(advances);
		newInfor.setDeclines(declines);
		newInfor.setNoChange(noChange);
		newInfor.setTotalVolume(totalVolume);
		newInfor.setTotalValue(totalValue);
		
		newInfor.setTotalVolumePt(totalVolumePT);
		newInfor.setTotalValuePt(totalValuePT);
		
		int numOfCe = (int) countCeFl(basketInfor.stream(), "ce");
		int numOfFl = (int) countCeFl(basketInfor.stream(), "fl");
		int numOfNoTrade = (int) countCeFl(basketInfor.stream(), "no");
		
		newInfor.setNumOfCe(numOfCe);
		newInfor.setNumOfFl(numOfFl);
		newInfor.setNoTrade(numOfNoTrade);
		
		ChangedValues cv = ChangedValues.of(indexId, ChangedValues.TYPE_INDEX);
		
		fillChangedValues(cv, current, newInfor);
		
		setIndexInfor(indexId, newInfor);
		
		if (IndexInfor.getIndexId("30").equals(indexId)) {
			getBasketSymbol(BasketInfor.HNX_FDS_VN30).forEach((symbol) -> {
				try {
					SecurityInfor secInfor = getSecurityInfor(symbol);
					IndexInfor vn30Infor = getIndexInfor(indexId);
					ChangedValues scv = ChangedValues.of(symbol, ChangedValues.TYPE_SECURITY);
					if (vn30Infor.getIndexValue().compareTo(BigDecimal.ZERO) > 0) {
						AdvancedInfor ainfor = secInfor.getDynamicInfor().getAdvancedInfor();
						int matchedPrice = secInfor.getLastMatchedOrder() == null ? secInfor.getStaticInfor().getBasicPrice() : secInfor.getLastMatchedOrder().getPrice();
						BigDecimal diff30 = BigDecimal.valueOf(matchedPrice).subtract(vn30Infor.getIndexValue().multiply(BigDecimalConstant.THOUSAND)).divide(BigDecimalConstant.THOUSAND, 2, RoundingMode.HALF_UP);
						if (diff30.compareTo(ainfor.getDiffVn30()) != 0) {
							ainfor.setDiffVn30(diff30);
							scv.put(AlgoMessageFields.DIFFVN30, diff30);
							builder.add(scv);
						}
					} else {
						log.warn("vn30Infor index not valid: {}", vn30Infor.getIndexValue());
					}
				} catch (Exception e) {
					log.error(String.format("error: symbol: %s", symbol), e);
				}
			});
		}
		
		builder.add(cv);
		
		return builder.build();
	}
	
	public Stream<ChangedValues> processInfor(TR hoseMessage) {
		log.info("processInfor_TR: {}", hoseMessage);
		Stream.Builder<ChangedValues> b = Stream.builder();
		
		int secNum = hoseMessage.getSecurityNumber();
		String symbol = masterJpaRepository.getSymbol(secNum);
		if (symbol == null || symbol.isEmpty())
			return Stream.empty();
		SecurityInfor cachedSecInfor = getSecurityInfor(symbol);
		ForeignInfor cInfor = cachedSecInfor.getDynamicInfor().getForeignInfor();
		
		ForeignInfor nInfor = ForeignInfor.of(); 
		nInfor.setBuyVolume(hoseMessage.getBuyVolume());
		nInfor.setSellVolume(hoseMessage.getSellVolume());
		nInfor.setCurrentRoom(hoseMessage.getCurrentRoom());
		nInfor.setTotalRoom(hoseMessage.getTotalRoom());
		nInfor.setRemainPercent(BigDecimal.valueOf(((long) hoseMessage.getCurrentRoom()) * 100).divide(BigDecimal.valueOf(hoseMessage.getTotalRoom()), 2, RoundingMode.HALF_UP));
		
		ChangedValues cv = ChangedValues.of(symbol, ChangedValues.TYPE_SECURITY);
		b.add(cv);
		fillChangedValues(cv, cInfor, nInfor);
		
		cachedSecInfor.getDynamicInfor().setForeignInfor(nInfor);
		
		return b.build();

	}
	public Stream<ChangedValues> processInfor(IU message) {
		log.info("processInfor_IU: {}", message);
		int pos = 0;
		try {
			String indexId = IndexInfor.HOSE;
			pos++; //1
			IndexInfor current = getIndexInfor(indexId);
			BasketInfor basketInfor = getBasketInfor(indexId);
			pos++;
			current.setMarketId(MarketInfor.HOSE);
			pos++; //3
			IndexInfor newInfor = current.clone();
			pos++;
			BigDecimal indexValue = BigDecimal.valueOf(message.getIndex()).divide(BigDecimalConstant.HUNDRED);
			pos++; //5
			int advances = message.getAdvances();
			pos++;
			int declines = message.getDeclines();
			pos++; //7
			int noChange = message.getNoChange();
			pos++;
			long totalValue = Long.valueOf(message.getTotalValuesTraded()) * ONE_MILLION - current.getTotalValuePt();
			pos++; //9
			long totalVolume = message.getTotalSharesTraded()  - current.getTotalVolumePt();
			pos++;
			int totalTrade = message.getTotalTrades();
			pos++; //11
			newInfor.setIndexValue(indexValue);
			pos++;
			newInfor.setAdvances(advances);
			pos++; //13
			newInfor.setDeclines(declines);
			pos++;
			newInfor.setNoChange(noChange);
			pos++; //15
			newInfor.setTotalVolume(totalVolume);
			pos++;
			newInfor.setTotalValue(totalValue);
			pos++; //17
			newInfor.setTotalTrade(totalTrade);
			pos++;
			int numOfCe = (int) countCeFl(basketInfor.stream(), "ce");
			pos++; //21
			int numOfFl = (int) countCeFl(basketInfor.stream(), "fl");
			pos++;
			int numOfNoTrade = (int) countCeFl(basketInfor.stream(), "no");
			pos++; //23
			
			newInfor.setNumOfCe(numOfCe);
			pos++;
			newInfor.setNumOfFl(numOfFl);
			pos++; //25
			newInfor.setNoTrade(numOfNoTrade);
			pos++;
			BigDecimal priorIndex = newInfor.getPriorIndex();
			pos++; //27
			if (priorIndex != null && priorIndex.compareTo(BigDecimal.ZERO) != 0) {
				pos++;
				newInfor.setChange(indexValue.subtract(priorIndex));
				pos++; //29
				newInfor.setChangeRate(newInfor.getChange().multiply(BigDecimalConstant.HUNDRED).divide(priorIndex, 2, RoundingMode.HALF_UP));
				pos++;
				log.info("processInfor_IU: priorIndex {} indexValue {} change {} rate {}", priorIndex, indexValue, newInfor.getChange(), newInfor.getChangeRate());
			} else {
				log.info("processInfor_IU: priorIndex is null or zero {} indexValue {}", priorIndex, indexValue);
			}
			pos++; //31
			ChangedValues cv = ChangedValues.of(indexId, ChangedValues.TYPE_INDEX);
			pos++;
			fillChangedValues(cv, current, newInfor);
			pos++; //33
			setIndexInfor(indexId, newInfor);
			pos++;
			return Stream.of(cv);
		} catch (Exception e) {
			log.error("processInfor_IU error: {} pos: {}", message, pos);
			log.error("", e);
			return Stream.empty();
		}
	}
	public Stream<ChangedValues> processInfor(PO hoseMessage) {
		log.info("processInfor_PO: {}", hoseMessage);
		Stream.Builder<ChangedValues> b = Stream.builder();

		int secNum = hoseMessage.getSecurityNumber();
		String symbol = masterJpaRepository.getSymbol(secNum);
		if (symbol == null || symbol.isEmpty())
			return Stream.empty();
		SecurityInfor cachedSecInfor = getSecurityInfor(symbol);
		ProjectedOpen cProjectedOpen = cachedSecInfor.getDynamicInfor().getProjectedOpen();
		int price = hoseMessage.getProjectOpenPrice();
		ProjectedOpen nProjectedOpen = ProjectedOpen.of(price * TEN);
		nProjectedOpen.calculateChange(cachedSecInfor.getStaticInfor().getBasicPrice());
		ChangedValues cv = ChangedValues.of(symbol, ChangedValues.TYPE_SECURITY);
		b.add(cv);
		fillChangedValues(cv, cProjectedOpen, nProjectedOpen);
		cachedSecInfor.getDynamicInfor().setProjectedOpen(nProjectedOpen);
		return b.build();
	}

	public Stream<ChangedValues> processInfor(LS hoseMessage) {
		log.info("processInfor_LS: {}", hoseMessage);
		
		int secNum = hoseMessage.getSecurityNumber();
		int volume = hoseMessage.getLotVolume() * HUNDRED;
		int price = hoseMessage.getPrice() * TEN;
		int confirmNumber = hoseMessage.getConfirmNumber();
		String time = MATCHED_ORDER_TIME_FORMATER.format(new Date()); 
		String side = hoseMessage.getSide();
				
		String symbol = masterJpaRepository.getSymbol(secNum);
		if (symbol == null || symbol.isEmpty())
			return Stream.empty();
		SecurityInfor cachedSecInfor = getSecurityInfor(symbol);
		
		ChangedValues cv = ChangedValues.of(symbol, ChangedValues.TYPE_TRANS);
		
		MatchedOrderInfor matchedOrderInfor = cachedSecInfor.addMatchedOrderInfor(symbol, price, volume, time);
		
		fillChangedValues(cv, matchedOrderInfor);
		
		DynamicInfor dynamicInfor = cachedSecInfor.getDynamicInfor();
		
		int highest = dynamicInfor.getHighest();
		int lowest = dynamicInfor.getLowest();
		
		if (highest <  price || highest == 0) {
			dynamicInfor.setHighest(price);
			cv.put(AlgoMessageFields.HIGHEST, price);
		} 
		if (lowest > price || lowest == 0) {
			dynamicInfor.setLowest(price);
			cv.put(AlgoMessageFields.LOWEST, price);
		}
		
		long newTotalValue = matchedOrderInfor.getAccumulatedValue();
		long newTotalVolume = matchedOrderInfor.getAccumulatedVolume();		
		
		dynamicInfor.setTotalValue(newTotalValue);
		dynamicInfor.setTotalVolume(newTotalVolume);
		
		if (newTotalVolume > 0) {
			int averagePrice = BigDecimal.valueOf(newTotalValue).divide(BigDecimal.valueOf(newTotalVolume), 0, RoundingMode.HALF_UP).intValue();
			dynamicInfor.setAverage(averagePrice);
			cv.put(AlgoMessageFields.AVERAGEPRICE, averagePrice);
		}

		cv.put(AlgoMessageFields.TOTALVALUE, newTotalValue);
		cv.put(AlgoMessageFields.TOTALSHARE, newTotalVolume);
				
		ChangedValues cvSec = ChangedValues.of(symbol, ChangedValues.TYPE_SECURITY);
		cvSec.putAll(cv);
						
		if (cachedSecInfor.getStaticInfor().getSecurityType() == SecurityType.CW) {
			SecurityInfor cwCachedSecInfor = cachedSecInfor;
			AdvancedInfor advancedInfor = cwCachedSecInfor.getDynamicInfor().getAdvancedInfor();
			BigDecimal currentPrice = BigDecimal.valueOf(
					(cwCachedSecInfor.getLastMatchedOrder() != null) ? cwCachedSecInfor.getLastMatchedOrder().getPrice()
							: cwCachedSecInfor.getStaticInfor().getBasicPrice());
			BigDecimal exercisePrice = BigDecimal.valueOf(cwCachedSecInfor.getStaticInfor().getExercisePrice());
			BigDecimal exerciseRatio = cwCachedMemory
					.caculateExerciseRatio(cwCachedSecInfor.getStaticInfor().getExerciseRatio());
			BigDecimal basis = cwCachedMemory.caculateBasis(currentPrice, exercisePrice, exerciseRatio);
			BigDecimal balancePrice = cwCachedMemory.caculateBalancePrice(currentPrice, exercisePrice, exerciseRatio);

			advancedInfor.setBalancePrice(balancePrice);
			advancedInfor.setBasis(basis);

			log.info("processInfor_LS: symbol {} caculateBasis {} caculateBalancePrice {} ", symbol, basis,
					balancePrice);
		}
		
		algoSubscribeMessageProcessor.processChangedValuesCollection(Collections.singleton(cv)).forEach((o) -> {
			MultiMessagePublishObject po = getTransLogPublishMessage(symbol);
			if (po.getUrl().equalsIgnoreCase(o.getUrl())) {
				po.addMessage(o.getMsg());
			} else {
				log.warn("Diff url : {} and {} ", po, o);
				po.addMessage(o.getMsg());
			}
		});;
		
		
		
		return Stream.of(cv, cvSec);
	}

	public Stream<ChangedValues> processInfor(SS message) {
		log.info("processInfor_SS: {}", message);
		int secNum = message.getSecurityNumber();
		String symbol = masterJpaRepository.getSymbol(secNum);
		if (symbol == null || symbol.isEmpty())
			return null;
		
		int ceilingPrice = message.getCeiling() * TEN;
		int floorPrice = message.getFloorPrice() * TEN;
		int basicPrice = message.getPriorClosePrice() * TEN;
		String benefit = String.valueOf(message.getBenefit()).trim();
		if ("ADR".contains(benefit)) {
			StaticInfor algoStaticInfor = getAlgoStaticInfor(symbol);
			if (algoStaticInfor != null && algoStaticInfor.getBasicPrice() > 0)
				basicPrice = algoStaticInfor.getBasicPrice();
			else {
				basicPrice = calculateBasiPrice(TickSize.HOSE, ceilingPrice, floorPrice);
				log.info("algoStaticInfor is NULL! symbol: {} basicPrice: {}", symbol, basicPrice);
			}
		}
		
		SecurityInfor newSecInfor = SecurityInfor.of(symbol);
		SecurityInfor cachedSecInfor = getSecurityInfor(symbol);
		ChangedValues cv = ChangedValues.of(symbol, ChangedValues.TYPE_SECURITY);

		if (cachedSecInfor == null) {
			cachedSecInfor = newSecInfor;
			log.info("New sec info: {}", newSecInfor);
		} else {
			newSecInfor = cachedSecInfor;
			log.info("Old sec info: {}", newSecInfor);
		}
		
		setSecurityInfor(symbol, newSecInfor);
		
		StaticInfor staticInfor = newSecInfor.getStaticInfor();
		staticInfor.setBasicPrice(basicPrice);
		staticInfor.setCeilingPrice(ceilingPrice);
		staticInfor.setFloorPrice(floorPrice);
		staticInfor.setSecurityType(getHoseSecurityType(message.getSecurityType()));
		
		if (newSecInfor.getStaticInfor().getSecurityType() == SecurityType.CW) {
			cwCachedMemory.addCw(symbol, message.getUnderlyingSymbol());
			newSecInfor.getStaticInfor().setExercisePrice(message.getExercisePrice());
			newSecInfor.getStaticInfor().setExerciseRatio(message.getExerciseRatio());
			
			BasketInfor infor = getBasketInfor(BasketInfor.HOSE_CW);
			infor.add(symbol);
			redisSummaryInforRepo.summary(InforType.BASKET, infor.getBasketId());
			log.info("processInfor_SS: cw sec detected: symbol {} underlyingSymbol {} exercisePrice {} exerciseRatio {}", symbol, message.getUnderlyingSymbol(), message.getExercisePrice(), message.getExerciseRatio());
		} else if (newSecInfor.getStaticInfor().getSecurityType() == SecurityType.STOCK) {
			BasketInfor infor = getBasketInfor(BasketInfor.HOSE);
			infor.add(symbol);
			redisSummaryInforRepo.summary(InforType.BASKET, infor.getBasketId());
			log.info("processInfor_SS: stock sec detected: symbol {} add to VNINDEX basket", symbol);
		}
				
		return Stream.of(cv);
	}

	public Stream<ChangedValues> processInfor(SU hoseMessage) {
		log.info("processInfor_SU: {}", hoseMessage);
		masterJpaRepository.saveSU(hoseMessage);
		return Stream.empty();
	}

	public Stream<ChangedValues> processInfor(TP hoseMessage) {
		log.info("processInfor_TP: {}", hoseMessage);

		int secNum = hoseMessage.getSecurityNumber();
		String side = hoseMessage.getSide();
		int volume1 = hoseMessage.getLotVolume1() * HUNDRED;
		int volume2 = hoseMessage.getLotVolume2() * HUNDRED;
		int volume3 = hoseMessage.getLotVolume3() * HUNDRED;
		
		int price1 = hoseMessage.getPrice1() * TEN;
		price1 = (price1 == 0 && volume1 > 0) ? getAtoAtcPrice() : price1;

		int price2 = hoseMessage.getPrice2() * TEN;
		int price3 = hoseMessage.getPrice3() * TEN;

		String symbol = masterJpaRepository.getSymbol(secNum);
		if (symbol == null || symbol.isEmpty())
			return Stream.empty();
		SecurityInfor cachedSecInfor = getSecurityInfor(symbol);
		if (cachedSecInfor == null) {
			log.warn("cachedSecInfor is null! Symbol: {} | TP: {}", symbol, hoseMessage);
			return Stream.empty();
		}
		DynamicInfor cachedDynInfor = cachedSecInfor.getDynamicInfor();
		TopPrice cachedTopPrice = TopPrice.SIDE_BUY.equalsIgnoreCase(side) ? cachedDynInfor.getTopBuy()
				: cachedDynInfor.getTopSell();
		

		TopPrice newTopPrice = TopPrice.of(side);
		newTopPrice.set(1, price1, volume1);
		newTopPrice.set(2, price2, volume2);
		newTopPrice.set(3, price3, volume3);

		ChangedValues cv = ChangedValues.of(symbol, ChangedValues.TYPE_SECURITY);
		fillChangedValues(cv, cachedTopPrice, newTopPrice);

		if (TopPrice.SIDE_BUY.equalsIgnoreCase(side)) {
			cachedDynInfor.setTopBuy(newTopPrice);
		} else {
			cachedDynInfor.setTopSell(newTopPrice);
		}

		return Stream.of(cv);
	}
	
	public Stream<ChangedValues> processInfor(PD hoseMessage) {
		PT_MATCH_LOG.info("processInfor_PD: {}", hoseMessage);
		Stream.Builder<ChangedValues> builder = Stream.builder();
		
		int secNum = hoseMessage.getSecurityNumber();
		String symbol = masterJpaRepository.getSymbol(secNum);
		if (symbol == null || symbol.isEmpty()) {
			PT_MATCH_LOG.info("processInfor_PD: symbol is null, use secNo {}", secNum);
//			return Stream.empty();
			symbol = String.valueOf(secNum);
		} else {
			
		}
		int confirmNo = hoseMessage.getConfirmNumber();

		SecurityInfor cachedSecInfor = getSecurityInfor(symbol);
		if (cachedSecInfor.getStaticInfor().getSecurityType() == SecurityType.STOCK || true) {
			PT_MATCH_LOG.info("processInfor_PD: confirmNo {} symbol {} is stock, SecurityType {}", confirmNo, symbol, cachedSecInfor.getStaticInfor().getSecurityType());
			int price = hoseMessage.getPrice() * 10;
			int matchedVol = hoseMessage.getVolume();
			long value = (long) price * matchedVol;
			String indexId = IndexInfor.HOSE;
			IndexInfor current = getIndexInfor(indexId);
			current.setMarketId(MarketInfor.HOSE);
			
			long currentTotalVolPt = current.getTotalVolumePt();
			long currentTotalValPt = current.getTotalValuePt();
			
			current.setTotalVolumePt(currentTotalVolPt + matchedVol);
			current.setTotalValuePt(currentTotalValPt + value);
			
			ChangedValues cv = ChangedValues.of(indexId, ChangedValues.TYPE_INDEX);
			builder.add(cv);
			
			cv.put(AlgoMessageFields.Index.PTTOTAL, current.getTotalValuePt());
			cv.put(AlgoMessageFields.Index.PTTOTALSHARE, current.getTotalVolumePt());
			
			PT_MATCH_LOG.info("processInfor_PD: confirmNo {} symbol {} matchedVol {}  price {} ", confirmNo, symbol, matchedVol, price);
			PT_MATCH_LOG.info("processInfor_PD: confirmNo {} symbol {} currentTotalVolPt {} currentTotalValPt {}", confirmNo, symbol, currentTotalVolPt, currentTotalValPt);
			PT_MATCH_LOG.info("processInfor_PD: confirmNo {} symbol {} newTotalVolPt {} newTotalValPt {}", confirmNo, symbol, current.getTotalVolumePt(), current.getTotalValuePt());
		} else {
			PT_MATCH_LOG.info("processInfor_PD: confirmNo {} symbol {} is NOT stock, SecurityType {}", confirmNo, symbol, cachedSecInfor.getStaticInfor().getSecurityType());
		}
		
		return builder.build();
	}
	
	public Stream<ChangedValues> processInfor(DC hoseMessage) {
		PT_MATCH_LOG.info("processInfor_DC: {}", hoseMessage);
		Stream.Builder<ChangedValues> builder = Stream.builder();
		
		int secNum = hoseMessage.getSecurityNumber();
		String symbol = masterJpaRepository.getSymbol(secNum);
		if (symbol == null || symbol.isEmpty()) {
			PT_MATCH_LOG.info("processInfor_DC: symbol is null, use secNo {}", secNum);
//			return Stream.empty();
			symbol = String.valueOf(secNum);
		} else {
			
		}
		int confirmNo = hoseMessage.getConfirmNumber();

		SecurityInfor cachedSecInfor = getSecurityInfor(symbol);
		if (cachedSecInfor.getStaticInfor().getSecurityType() == SecurityType.STOCK || true) {
			PT_MATCH_LOG.info("processInfor_DC: confirmNo {} symbol {} is stock, SecurityType {}", confirmNo, symbol, cachedSecInfor.getStaticInfor().getSecurityType());
			int price = hoseMessage.getPrice() * 10;
			int matchedVol = hoseMessage.getVolume();
			long value = (long) price * matchedVol;
			String indexId = IndexInfor.HOSE;
			IndexInfor current = getIndexInfor(indexId);
			current.setMarketId(MarketInfor.HOSE);
			
			long currentTotalVolPt = current.getTotalVolumePt();
			long currentTotalValPt = current.getTotalValuePt();
			
			current.setTotalVolumePt(currentTotalVolPt - matchedVol);
			current.setTotalValuePt(currentTotalValPt - value);
			
			ChangedValues cv = ChangedValues.of(indexId, ChangedValues.TYPE_INDEX);
			builder.add(cv);
			
			cv.put(AlgoMessageFields.Index.PTTOTAL, current.getTotalValuePt());
			cv.put(AlgoMessageFields.Index.PTTOTALSHARE, current.getTotalVolumePt());
			
			PT_MATCH_LOG.info("processInfor_DC: confirmNo {} symbol {} matchedVol {}  price {} ", confirmNo, symbol, matchedVol, price);
			PT_MATCH_LOG.info("processInfor_DC: confirmNo {} symbol {} currentTotalVolPt {} currentTotalValPt {}", confirmNo, symbol, currentTotalVolPt, currentTotalValPt);
			PT_MATCH_LOG.info("processInfor_DC: confirmNo {} symbol {} newTotalVolPt {} newTotalValPt {}", confirmNo, symbol, current.getTotalVolumePt(), current.getTotalValuePt());
		} else {
			PT_MATCH_LOG.info("processInfor_DC: confirmNo {} symbol {} is NOT stock, SecurityType {}", confirmNo, symbol, cachedSecInfor.getStaticInfor().getSecurityType());
		}
		
		return builder.build();
	
	}

	public Stream<ChangedValues> processInfor(SC hoseMessage) {
		log.info("processInfor_SC: {}", hoseMessage);

		String marketFlag = hoseMessage.getSystemControlCode();
		String marketId = MarketInfor.HOSE;
		
		MarketInfor marketInfor = getMarketInfor(marketId);
				
		if (marketInfor == null) {
			marketInfor = MarketInfor.of(marketId);
			marketInfor.setMarketFlag(HoseSystemControlCode.END_EOD_UPDATE);
		}
		marketInfor.setPriorMarketFlag(marketInfor.getMarketFlag());
		marketInfor.setMarketName(marketId);
		marketInfor.setMarketFlag(marketFlag);
		
		setMarketInfor(marketId, marketInfor);
		
		ChangedValues cv = ChangedValues.of(MarketInfor.HOSE, ChangedValues.TYPE_MARKET);

		refresh();
		if (HoseSystemControlCode.ATO.equals(marketInfor.getMarketFlag()) || HoseSystemControlCode.ATC.equals(marketInfor.getMarketFlag())) {
			startManualCalculateIndexTimer();
		} else {
			stopManualCalculateIndexTimer();
		}
		
		return Stream.empty();
	}
	
	private void stopManualCalculateIndexTimer() {
		if (manualCalculateIndexTimer != null) {
			log.info("stopManualCalculateIndexTimer!");
			manualCalculateIndexTimer.cancel();
			manualCalculateIndexTimer = null;
		} else {
			log.info("stopManualCalculateIndexTimer! manualCalculateIndexTimer is null!");
		}

	}

	private void startManualCalculateIndexTimer() {
		if (manualCalculateIndexTimer == null) {
			manualCalculateIndexTimer = new Timer("manualCalculateIndexTimer");
			log.info("startManualCalculateIndexTimer!");
			manualCalculateIndexTimer.schedule(getManualCalculateIndexTask(), 0, TimeConstant.ONE_SECOND * 4);
			log.info("startManualCalculateIndexTimer! schedule: {}", "manualCalculateIndexTask");
		} else {
			log.info("startManualCalculateIndexTimer! manualCalculateIndexTimer is not null!");
		}
	}

	public Stream<ChangedValues> processInfor(StockInfo message) {
		log.info("processInfor_StockInfo: {}", message);
		Stream.Builder<ChangedValues> stream = Stream.builder();
		String securityType = getString(message, quickfix.field.SecurityType.FIELD);
		if (securityType.isEmpty()) {
			log.warn("KEY_STOCKS_INFO_SECURITYTYPE_IS_NULL");
			return stream.build();
		}
		
		SecurityType type = getHnxSecurityType(securityType);
		
		String symbol = getString(message, Symbol.FIELD);
		
		SecurityInfor cachedSecInfor = getSecurityInfor(symbol);
		
		SecurityInfor newSecInfor = SecurityInfor.of(symbol);
		StaticInfor newStaticInfor = newSecInfor.getStaticInfor();
		newStaticInfor.setSecurityType(type);
		DynamicInfor newDynamicInfor = newSecInfor.getDynamicInfor();
		ForeignInfor newForeignInfor = newDynamicInfor.getForeignInfor();
		
		int basicPrice = getDecimal(message, BasicPrice.FIELD).intValue();
		int floorPrice = getDecimal(message, FloorPrice.FIELD).intValue();
		int ceilingPrice = getDecimal(message, CeilingPrice.FIELD).intValue();
		
		int open = getDecimal(message, OpenPrice.FIELD).intValue();
		int highest = getDecimal(message, HighestPrice.FIELD).intValue();
		int lowest = getDecimal(message, LowestPrice.FIELD).intValue();
		
		long buyVolumeTR = getDecimal(message, BuyForeignQtty.FIELD).longValue();
		long sellVolumeTR = getDecimal(message, SellForeignQtty.FIELD).longValue();
		long currentRoomTR = getDecimal(message, RemainForeignQtty.FIELD).longValue();
	
		newDynamicInfor.setOpen(open);
		newDynamicInfor.setHighest(highest);
		newDynamicInfor.setLowest(lowest);
		
		newForeignInfor.setBuyVolume(buyVolumeTR);
		newForeignInfor.setSellVolume(sellVolumeTR);
		newForeignInfor.setCurrentRoom(currentRoomTR);
		
		newStaticInfor.setBasicPrice(basicPrice);
		newStaticInfor.setFloorPrice(floorPrice);
		newStaticInfor.setCeilingPrice(ceilingPrice);
				
		long totalVolume = getDecimal(message, NM_TotalTradedQtty.FIELD).longValue();
		long totalValue = getDecimal(message, NM_TotalTradedValue.FIELD).longValue();
		
		newSecInfor.getDynamicInfor().setTotalValue(totalValue);
		newSecInfor.getDynamicInfor().setTotalVolume(totalVolume);
		
		long cachedTotalVolume = cachedSecInfor == null ? 0 : cachedSecInfor.getDynamicInfor().getTotalVolume();
		long cachedTotalValue = cachedSecInfor == null ? 0 : cachedSecInfor.getDynamicInfor().getTotalValue();
		long matchPrice = getDecimal(message, MatchPrice.FIELD).longValue();

		long totalBuyQtty = getDecimal(message, TotalBidQtty.FIELD).longValue();
		long totalSellQtty = getDecimal(message, TotalOfferQtty.FIELD).longValue();
		long nmTotalTradedBuyQtty = getDecimal(message, NM_TotalTradedQtty.FIELD).longValue();
		
		newSecInfor.getDynamicInfor().setTotalBuyQtty(totalBuyQtty - nmTotalTradedBuyQtty);
		newSecInfor.getDynamicInfor().setTotalSellQtty(totalSellQtty - nmTotalTradedBuyQtty);
		
		if (cachedSecInfor == null)
			cachedSecInfor = newSecInfor;
		
		newSecInfor.copyMatchedOrderInfor(cachedSecInfor);
		newSecInfor.getDynamicInfor().setTopBuy(cachedSecInfor.getDynamicInfor().getTopBuy());
		newSecInfor.getDynamicInfor().setTopSell(cachedSecInfor.getDynamicInfor().getTopSell());
		newSecInfor.getDynamicInfor().setAverage(cachedSecInfor.getDynamicInfor().getAverage());

		ChangedValues cv = ChangedValues.of(symbol, ChangedValues.TYPE_SECURITY);
		
		if (symbol.equals("ACB"))
			log.info("debugggggggg: totalVolume {} cachedTotalVolume {}", totalVolume, cachedTotalVolume);
		if (totalVolume > cachedTotalVolume) {
//			int value = (int) (totalValue - cachedTotalValue);			
			int volume = (int) (totalVolume - cachedTotalVolume);
		
//			int matchedPrice = BigDecimal.valueOf(value).divide(BigDecimal.valueOf(volume), -2, RoundingMode.HALF_UP).intValue();
			int matchedPrice = (int) matchPrice;

			String time = getString(message, Time.FIELD);
			MatchedOrderInfor matchedOrderInfor = newSecInfor.addMatchedOrderInfor(symbol, matchedPrice, volume, time);
			
			ChangedValues tcv = ChangedValues.of(symbol, ChangedValues.TYPE_TRANS);
			
			fillChangedValues(tcv, matchedOrderInfor);
						
			tcv.put(AlgoMessageFields.TOTALVALUE, totalValue);
			tcv.put(AlgoMessageFields.TOTALSHARE, totalVolume);
			
			if (totalVolume > 0) {
				int averagePrice = BigDecimal.valueOf(totalValue).divide(BigDecimal.valueOf(totalVolume), 0, RoundingMode.HALF_UP).intValue();
				newSecInfor.getDynamicInfor().setAverage(averagePrice);
				cv.put(AlgoMessageFields.AVERAGEPRICE, averagePrice);
			}
			
			cv.putAll(tcv);
			stream.add(tcv);
			algoSubscribeMessageProcessor.processChangedValuesCollection(Collections.singleton(tcv)).forEach((o) -> {
				MultiMessagePublishObject po = getTransLogPublishMessage(symbol);
				if (po.getUrl().equalsIgnoreCase(o.getUrl())) {
					po.addMessage(o.getMsg());
				} else {
					log.warn("Diff url : {} and {} ", po, o);
					po.addMessage(o.getMsg());
				}
			});;		
		} else {
			int poPrice = getDecimal(message, CurrentPrice.FIELD).intValue();
			String hnxServerTime = getString(message, Time.FIELD);

			if (poPrice > 0) {				
				if (hnxServerTime != null && MilestoneLocalTime.of(hnxServerTime).isAfter(MilestoneLocalTime.HNX_PLO)) {
					log.info("hnxServerTime: {} is after HNX_PLO, reject projectOpen price!", hnxServerTime);
					newDynamicInfor.setProjectedOpen(ProjectedOpen.INACTIVE_PO);
					fillChangedValues(cv, newSecInfor.getLastMatchedOrder());
				} else {
					ProjectedOpen projectedOpen = ProjectedOpen.of(poPrice);
					projectedOpen.calculateChange(basicPrice);
					newDynamicInfor.setProjectedOpen(projectedOpen);
					fillChangedValues(cv, cachedSecInfor.getDynamicInfor().getProjectedOpen(), newDynamicInfor.getProjectedOpen());
				}
			} else {
				if (hnxServerTime != null && MilestoneLocalTime.of(hnxServerTime).isAfter(MilestoneLocalTime.HNX_ATC) && MilestoneLocalTime.of(hnxServerTime).isBefore(MilestoneLocalTime.HNX_PLO)) {
					log.info("hnxServerTime: {} is after HNX_ATC and before HNX_PLO, set projectOpen price to ZERO!", hnxServerTime);
					newDynamicInfor.setProjectedOpen(ProjectedOpen.NULL_PO);
					fillChangedValues(cv, cachedSecInfor.getDynamicInfor().getProjectedOpen(), newDynamicInfor.getProjectedOpen());
				}
			}
		}
		
		fillChangedValues(cv, cachedSecInfor, newSecInfor);
		fillChangedValues(cv, cachedSecInfor.getDynamicInfor(), newDynamicInfor);
		fillChangedValues(cv, cachedSecInfor.getDynamicInfor().getForeignInfor(), newForeignInfor);
		
		setSecurityInfor(symbol, newSecInfor);
		
		stream.add(cv);		
		return stream.build();
	}

	public Stream<ChangedValues> processInfor(DerivativesInfo message) {
			log.info("processInfor_DerivativesInfo: {}", message);
			Stream.Builder<ChangedValues> stream = Stream.builder();
			String securityType = getString(message, quickfix.field.SecurityType.FIELD);
			if (securityType.isEmpty()) {
				log.warn("KEY_FDS_INFO_SECURITYTYPE_IS_NULL");
				return stream.build();
			}
					
			String symbol = getString(message, Symbol.FIELD);
			
			SecurityInfor cachedSecInfor = getSecurityInfor(symbol);
			
			SecurityInfor newSecInfor = SecurityInfor.of(symbol);
			StaticInfor newStaticInfor = newSecInfor.getStaticInfor();
			newStaticInfor.setSecurityType(SecurityType.FDS);
			DynamicInfor newDynamicInfor = newSecInfor.getDynamicInfor();
			ForeignInfor newForeignInfor = newDynamicInfor.getForeignInfor();
			
			int basicPrice = getDecimal(message, BasicPrice.FIELD).multiply(BigDecimalConstant.THOUSAND).intValue();
			int floorPrice = getDecimal(message, FloorPrice.FIELD).multiply(BigDecimalConstant.THOUSAND).intValue();
			int ceilingPrice = getDecimal(message, CeilingPrice.FIELD).multiply(BigDecimalConstant.THOUSAND).intValue();
			
			int open = getDecimal(message, OpenPrice.FIELD).multiply(BigDecimalConstant.THOUSAND).intValue();
			int highest = getDecimal(message, HighestPrice.FIELD).multiply(BigDecimalConstant.THOUSAND).intValue();
			int lowest = getDecimal(message, LowestPrice.FIELD).multiply(BigDecimalConstant.THOUSAND).intValue();
			
			long buyVolumeTR = getDecimal(message, BuyForeignQtty.FIELD).longValue() * 10;
			long sellVolumeTR = getDecimal(message, SellForeignQtty.FIELD).longValue() * 10;
			long currentRoomTR = getDecimal(message, RemainForeignQtty.FIELD).longValue() * 10;
		
			newDynamicInfor.setOpen(open);
			newDynamicInfor.setHighest(highest);
			newDynamicInfor.setLowest(lowest);
			
			newForeignInfor.setBuyVolume(buyVolumeTR);
			newForeignInfor.setSellVolume(sellVolumeTR);
			newForeignInfor.setCurrentRoom(currentRoomTR);
			
			newStaticInfor.setBasicPrice(basicPrice);
			newStaticInfor.setFloorPrice(floorPrice);
			newStaticInfor.setCeilingPrice(ceilingPrice);
					
			long totalVolume = getDecimal(message, NM_TotalTradedQtty.FIELD).longValue() * 10;
			long totalValue = getDecimal(message, NM_TotalTradedValue.FIELD).longValue();
			
			newSecInfor.getDynamicInfor().setTotalValue(totalValue);
			newSecInfor.getDynamicInfor().setTotalVolume(totalVolume);
						
			long cachedTotalVolume = cachedSecInfor == null ? 0 : cachedSecInfor.getDynamicInfor().getTotalVolume();
			long cachedTotalValue = cachedSecInfor == null ? 0 : cachedSecInfor.getDynamicInfor().getTotalValue();
			long matchPrice = getDecimal(message, MatchPrice.FIELD).multiply(BigDecimalConstant.THOUSAND).longValue();
			BigDecimal oi = getDecimal(message, OpenInterest.FIELD);
			
			long totalBuyQtty = getDecimal(message, TotalBidQtty.FIELD).longValue() * 10;
			long totalSellQtty = getDecimal(message, TotalOfferQtty.FIELD).longValue() * 10;
			newSecInfor.getDynamicInfor().setTotalBuyQtty(totalBuyQtty);
			newSecInfor.getDynamicInfor().setTotalSellQtty(totalSellQtty);
			
			if (cachedSecInfor == null)
				cachedSecInfor = newSecInfor;
			
			newSecInfor.copyMatchedOrderInfor(cachedSecInfor);
			newSecInfor.getDynamicInfor().setTopBuy(cachedSecInfor.getDynamicInfor().getTopBuy());
			newSecInfor.getDynamicInfor().setTopSell(cachedSecInfor.getDynamicInfor().getTopSell());
			
			newSecInfor.getDynamicInfor().getAdvancedInfor().setDiffVn30(cachedSecInfor.getDynamicInfor().getAdvancedInfor().getDiffVn30());
			newSecInfor.getDynamicInfor().setAverage(cachedSecInfor.getDynamicInfor().getAverage());
			newSecInfor.getDynamicInfor().getAdvancedInfor().setOpenInterest(oi);

			ChangedValues cv = ChangedValues.of(symbol, ChangedValues.TYPE_SECURITY);
			
			if (totalVolume > cachedTotalVolume) {
	//			int value = (int) (totalValue - cachedTotalValue);			
				int volume = (int) (totalVolume - cachedTotalVolume);
				
	//			int matchedPrice = BigDecimal.valueOf(value).divide(BigDecimal.valueOf(volume), -2, RoundingMode.HALF_UP).intValue();
				int matchedPrice = (int) matchPrice;
	
				String time = getString(message, Time.FIELD);
				MatchedOrderInfor matchedOrderInfor = newSecInfor.addMatchedOrderInfor(symbol, matchedPrice, volume, time);
				
				ChangedValues tcv = ChangedValues.of(symbol, ChangedValues.TYPE_TRANS);
				
				fillChangedValues(tcv, matchedOrderInfor);
				
				tcv.put(AlgoMessageFields.TOTALVALUE, totalValue);
				tcv.put(AlgoMessageFields.TOTALSHARE, totalVolume);
				
				if (totalVolume > 0) {
					int averagePrice = BigDecimal.valueOf(totalValue).divide(BigDecimal.valueOf(totalVolume*100/10), 0, RoundingMode.HALF_UP).intValue();
					newSecInfor.getDynamicInfor().setAverage(averagePrice);
					cv.put(AlgoMessageFields.AVERAGEPRICE, averagePrice);
				}
				
				cv.putAll(tcv);
				stream.add(tcv);				
				IndexInfor vn30Infor = getIndexInfor(IndexInfor.getIndexId("30"));
				if (vn30Infor.getIndexValue().compareTo(BigDecimal.ZERO) > 0) {
					AdvancedInfor ainfor = newDynamicInfor.getAdvancedInfor();
					BigDecimal diff30 = BigDecimal.valueOf(matchedPrice).subtract(vn30Infor.getIndexValue().multiply(BigDecimalConstant.THOUSAND)).divide(BigDecimalConstant.THOUSAND, 2, RoundingMode.HALF_UP);
					ainfor.setDiffVn30(diff30);
//					fillChangedValues(cv, cachedSecInfor.getDynamicInfor().getAdvancedInfor(), ainfor);
					cv.put(AlgoMessageFields.DIFFVN30, diff30);
				} else {
					log.warn("vn30Infor index not valid: {}", vn30Infor.getIndexValue());
				}
				
				algoSubscribeMessageProcessor.processChangedValuesCollection(Collections.singleton(tcv)).forEach((o) -> {
					MultiMessagePublishObject po = getTransLogPublishMessage(symbol);
					if (po.getUrl().equalsIgnoreCase(o.getUrl())) {
						po.addMessage(o.getMsg());
					} else {
						log.warn("Diff url : {} and {} ", po, o);
						po.addMessage(o.getMsg());
					}
				});;
			} else {
				int poPrice = getDecimal(message, CurrentPrice.FIELD).multiply(BigDecimalConstant.THOUSAND).intValue();
				String hnxServerTime = getString(message, Time.FIELD);

				if (poPrice > 0) {
					//Sau phien ATC van tra projectOpen, vi vay phai if else
					if (hnxServerTime != null && MilestoneLocalTime.of(hnxServerTime).isAfter(MilestoneLocalTime.FDS_CLOSE_MARKET)) {
						log.info("hnxServerTime: {} is after FDS_CLOSE_MARKET, reject projectOpen price!", hnxServerTime);
						if (newDynamicInfor.getProjectedOpen() != ProjectedOpen.INACTIVE_PO) {
							newDynamicInfor.setProjectedOpen(ProjectedOpen.INACTIVE_PO);
							fillChangedValues(cv, newSecInfor.getLastMatchedOrder());
						}
					} else {
						ProjectedOpen projectedOpen = ProjectedOpen.of(poPrice);
						projectedOpen.calculateChange(basicPrice);
						newDynamicInfor.setProjectedOpen(projectedOpen);
						fillChangedValues(cv, cachedSecInfor.getDynamicInfor().getProjectedOpen(), newDynamicInfor.getProjectedOpen());
					}
				} else {
					if (hnxServerTime != null && MilestoneLocalTime.of(hnxServerTime).isAfter(MilestoneLocalTime.FDS_ATC) && MilestoneLocalTime.of(hnxServerTime).isBefore(MilestoneLocalTime.FDS_CLOSE_MARKET)) {
						log.info("hnxServerTime: {} is after FDS_ATC and before FDS_CLOSE_MARKET, set projectOpen price to ZERO!", hnxServerTime);
						newDynamicInfor.setProjectedOpen(ProjectedOpen.NULL_PO);
						fillChangedValues(cv, cachedSecInfor.getDynamicInfor().getProjectedOpen(), newDynamicInfor.getProjectedOpen());
					}
				}
			}
			
			int closePrice = newSecInfor.getLastMatchedOrder() == null ? 0 : newSecInfor.getLastMatchedOrder().getPrice();
			int diff30CalPrice = newSecInfor.getDynamicInfor().getProjectedOpen() == ProjectedOpen.INACTIVE_PO ? closePrice : newSecInfor.getDynamicInfor().getProjectedOpen().getPrice();
			
			//Tinh diff30
			IndexInfor vn30Infor = getIndexInfor(IndexInfor.getIndexId("30"));			
			if (vn30Infor.getIndexValue().compareTo(BigDecimal.ZERO) > 0) {
				AdvancedInfor ainfor = newDynamicInfor.getAdvancedInfor();
				if (diff30CalPrice != 0) {
					BigDecimal diff30 = BigDecimal.valueOf(diff30CalPrice).subtract(vn30Infor.getIndexValue().multiply(BigDecimalConstant.THOUSAND)).divide(BigDecimalConstant.THOUSAND, 2, RoundingMode.HALF_UP);
					ainfor.setDiffVn30(diff30);
					log.info("diff30 cal: Symbol {} diff30CalPrice {} diff30 {}", symbol, diff30CalPrice, diff30);
				} else {
					ainfor.setDiffVn30(BigDecimal.ZERO);
					log.warn("diff30CalPrice not valid: {}", diff30CalPrice);
				}
				fillChangedValues(cv, cachedSecInfor.getDynamicInfor().getAdvancedInfor(), ainfor);
//				cv.put(AlgoMessageFields.DIFFVN30, diff30);
			} else {
				log.warn("vn30Infor index not validdd: {}", vn30Infor.getIndexValue());
			}
			
			fillChangedValues(cv, cachedSecInfor, newSecInfor);
			fillChangedValues(cv, cachedSecInfor.getDynamicInfor(), newDynamicInfor);
			fillChangedValues(cv, cachedSecInfor.getDynamicInfor().getForeignInfor(), newForeignInfor);
			fillChangedValues(cv, cachedSecInfor.getDynamicInfor().getAdvancedInfor(), newSecInfor.getDynamicInfor().getAdvancedInfor());
						
			setSecurityInfor(symbol, newSecInfor);
			
			stream.add(cv);
			return stream.build();
		}

	public Stream<ChangedValues> processInfor(TopNPrice message) {
		log.info("processInfor_TopNPrice: {}", message);
		Stream.Builder<ChangedValues> stream = Stream.builder();
		
		String symbol = getString(message, Symbol.FIELD);
		SecurityInfor cachedSecInfor = getSecurityInfor(symbol);
		
		if (symbol == null || symbol.isEmpty())
			return Stream.empty();
		if (cachedSecInfor == null) {
			log.warn("cachedSecInfor is null! Symbol: {}", symbol);
			return Stream.empty();
		}
		
		TopPrice cTopBuy = cachedSecInfor.getDynamicInfor().getTopBuy();
		TopPrice cTopSell = cachedSecInfor.getDynamicInfor().getTopSell();
				
		TopPrice nTopBuy = TopPrice.of(TopPrice.SIDE_BUY);
		TopPrice nTopSell = TopPrice.of(TopPrice.SIDE_SELL);		
		int noTopPrice = getInt(message, NoTopPrice.FIELD);
		Group topBuyGroup = new Group(NoTopPrice.FIELD, NumTopPrice.FIELD);
		
		BigDecimal multiplicandPrice = cachedSecInfor.getStaticInfor().getSecurityType() == SecurityType.FDS ? BigDecimalConstant.THOUSAND : BigDecimalConstant.ONE;
		BigDecimal multiplicandQtty = cachedSecInfor.getStaticInfor().getSecurityType() == SecurityType.FDS ? BigDecimalConstant.TEN : BigDecimalConstant.ONE;
		
		message.getGroups(NoTopPrice.FIELD).forEach((g) -> {			
			int top = getInt(g, NumTopPrice.FIELD);
			int topBuyPrice = getDecimal(g, BestBidPrice.FIELD).multiply(multiplicandPrice).intValue();
			int topBuyQtty = getDecimal(g, BestBidQtty.FIELD).multiply(multiplicandQtty).intValue();
			int topSellPrice = getDecimal(g, BestOfferPrice.FIELD).multiply(multiplicandPrice).intValue();
			int topSellQtty = getDecimal(g, BestOfferQtty.FIELD).multiply(multiplicandQtty).intValue();
			
			if (top == 1) {
				topBuyPrice = (topBuyPrice == 0 && topBuyQtty > 0) ? getAtoAtcPrice() : topBuyPrice;
				topSellPrice = (topSellPrice == 0 && topSellQtty > 0) ? getAtoAtcPrice() : topSellPrice;
			}
			
			nTopBuy.set(top, topBuyPrice, topBuyQtty);
			nTopSell.set(top, topSellPrice, topSellQtty);
		});				
		
		ChangedValues cv = ChangedValues.of(symbol, ChangedValues.TYPE_SECURITY);
		fillChangedValues(cv, cTopBuy, nTopBuy);
		fillChangedValues(cv, cTopSell, nTopSell);
	
		cachedSecInfor.getDynamicInfor().setTopBuy(nTopBuy);
		cachedSecInfor.getDynamicInfor().setTopSell(nTopSell);
		
		stream.add(cv);
		
		return stream.build();
	}

	public Stream<ChangedValues> processInfor(BasketInfo message) {
		log.info("processInfor_BasketInfo: {}", message);

		String indexId = getString(message, IndexCode.FIELD);
		String symbol = getString(message, Symbol.FIELD);
		BasketInfor infor = getBasketInfor(indexId);
		infor.add(symbol);		
		log.info("processInfor_BasketInfo: add symbol {} to basket {}", symbol, indexId);
		
		redisSummaryInforRepo.summary(InforType.BASKET, indexId);
		
		return Stream.empty();
	}
	
	public Stream<ChangedValues> processInfor(BoardInfo message) {
		log.info("processInfor_BoardInfo: {}", message);
		Stream.Builder<ChangedValues> stream = Stream.builder();
		
		String boardCode = getString(message, BoardCode.FIELD);
		String indexId = HnxBoardCode.getIndexIdByBoard(boardCode);
		if (indexId == null || indexId.isEmpty()) {
			log.warn("Skip boardCode: {}", boardCode);
			return Stream.empty();
		}
		IndexInfor current = getIndexInfor(indexId);
		current.setMarketId(MarketInfor.HNX);
		
		IndexInfor newInfor = current.clone();
		
		long totalTrade = getDecimal(message, TotalTrade.FIELD).longValue();
		long totalVolumePT = getDecimal(message, TotalPTTradedQtty.FIELD).longValue();
		long totalValuePT = getDecimal(message, TotalPTTradedValue.FIELD).longValue();
		
		long totalVolumeRD = getDecimal(message, TotalNormalTradedQttyRd.FIELD).longValue();
		long totalValueRD = getDecimal(message, TotalNormalTradedValueRd.FIELD).longValue();
		
		long totalVolumeOD = getDecimal(message, TotalNormalTradedQttyOd.FIELD).longValue();
		long totalValueOD = getDecimal(message, TotalNormalTradedValueOd.FIELD).longValue();
		
		long totalVolume = totalVolumeOD + totalVolumeRD;
		long totalValue = totalValueOD + totalValueRD;
		
		int advances = getInt(message, NumSymbolAdvances.FIELD);
		int declines = getInt(message, NumSymbolDeclines.FIELD);
		int noChange = getInt(message, NumSymbolNochange.FIELD);
		
		newInfor.setTotalTrade(totalTrade);
		newInfor.setTotalValuePt(totalValuePT);
		newInfor.setTotalVolumePt(totalVolumePT);
		newInfor.setTotalValue(totalValue);
		newInfor.setTotalVolume(totalVolume);
		newInfor.setAdvances(advances);
		newInfor.setDeclines(declines);
		newInfor.setNoChange(noChange);
		
		ChangedValues cv = ChangedValues.of(indexId, ChangedValues.TYPE_INDEX);
		stream.add(cv);
		fillChangedValues(cv, current, newInfor);
		
		setIndexInfor(indexId, newInfor);
		String sesStatus = getString(message, TradSesStatus.FIELD);
//		String marketId = getHnxMarketId();
		MarketInfor infor = getMarketInfor(MarketInfor.HNX);
		infor.setMarketId(MarketInfor.HNX);
		infor.setMarketFlag(sesStatus);
		setMarketInfor(MarketInfor.HNX, infor);
		
		String hnxServerDate = getString(message, TradingDate.FIELD);
		String hnxServerTime = getString(message, Time.FIELD);
		
		processHnxServerTime(hnxServerTime);
		processHnxServerDate(hnxServerDate);
		
		return stream.build();
	
	}	
	
	public Stream<ChangedValues> processInfor(IndexInfo message) {
		log.info("processInfor_IndexInfo: {}", message);
		Stream.Builder<ChangedValues> stream = Stream.builder();
		
		String indexId = getString(message, IndexCode.FIELD);
		
		IndexInfor current = getIndexInfor(indexId);
		current.setMarketId(MarketInfor.HNX);
		
		IndexInfor newInfor = current.clone();
		BasketInfor basketInfor = getBasketInfor(indexId);

		BigDecimal indexValue = getDecimal(message, Value.FIELD);
		BigDecimal indexChange = getDecimal(message, Change.FIELD);
		BigDecimal indexPercentChange = getDecimal(message, RatioChange.FIELD);
		BigDecimal priorIndexValue = getDecimal(message, PriorIndexVal.FIELD);
		String indexTime = getString(message, CalTime.FIELD);
		
		newInfor.setIndexValue(indexValue);
		newInfor.setChange(indexChange);
		newInfor.setChangeRate(indexPercentChange);
		newInfor.setPriorIndex(priorIndexValue);
		
		int numOfCe = (int) countCeFl(basketInfor.stream(), "ce");
		int numOfFl = (int) countCeFl(basketInfor.stream(), "fl");
		int numOfNoTrade = (int) countCeFl(basketInfor.stream(), "no");

		
		newInfor.setNumOfCe(numOfCe);
		newInfor.setNumOfFl(numOfFl);
		newInfor.setNoTrade(numOfNoTrade);
		
		if (IndexInfor.HNX_30.equals(indexId)) {
			long totalVolume = getDecimal(message, TotalQtty.FIELD).longValue();
			long totalValue = getDecimal(message, TotalValue.FIELD).longValue();
			
			newInfor.setTotalValue(totalValue);
			newInfor.setTotalVolume(totalVolume);
		}
		
		ChangedValues cv = ChangedValues.of(indexId, ChangedValues.TYPE_INDEX);
		stream.add(cv);
		fillChangedValues(cv, current, newInfor);
		
		setIndexInfor(indexId, newInfor);
		
		return stream.build();
	}

	public MultiMessagePublishObject getTransLogPublishMessage(String symbol) {
		if (!TRANSLOG_PUBLISH_OBJECT_REPO.containsKey(symbol))
			TRANSLOG_PUBLISH_OBJECT_REPO.put(symbol, MultiMessagePublishObject.of(PublishEntityType.getTransLogUrl(symbol)));
		return TRANSLOG_PUBLISH_OBJECT_REPO.get(symbol);
	}

	public Stream<ChangedValues> getPublishStockInfor(String symbol) {
		log.debug("getPublishStockInfor: key: {}", symbol);
		ChangedValues cv = ChangedValues.of(symbol, ChangedValues.TYPE_SECURITY);
		SecurityInfor infor = getSecurityInfor(symbol);
		if (infor == null) {
			log.info("Cannot find SecurityInfor with key: {}", symbol);
		} else {
			fillChangedValues(cv, infor);
			ProjectedOpen po = infor.getDynamicInfor().getProjectedOpen();
			if (po == null || po.getPrice() < 0)
				fillChangedValues(cv, infor.getLastMatchedOrder());
			//log.info("Debugggg 00001: {} ", cv);
		}
		if ("GAS,BID,CTG".contains(symbol)) {
			log.info("getPublishStockInfor debuggg: key: {} cv {}", symbol, cv);
		}
		return Stream.of(cv);
	}
	
	public Stream<ChangedValues> getPublishMarketInfor(String indexId) {
		log.debug("getPublishMarketInfor: key: {}", indexId);
		ChangedValues cv = ChangedValues.of(indexId, ChangedValues.TYPE_INDEX);
		IndexInfor infor = getIndexInfor(indexId);
		if (infor == null) {
			log.info("Cannot find IndexInfor with key: {}", indexId);
		} else {
			fillChangedValues(cv, infor);
		}
		return Stream.of(cv);
	}
	
	public Stream<ChangedValues> getPublishTransLog(String symbol) {
		log.info("getPublishTransLog: key: {}", symbol);
		SecurityInfor infor = getSecurityInfor(symbol);
		Stream.Builder<ChangedValues> builder = Stream.builder();
		if (infor == null) {
			log.info("Cannot find SecurityInfor with key: {}", symbol);
		} else {
			infor.streamMatchedOrderInfor().forEach((m) -> {
				ChangedValues cv = ChangedValues.of(symbol, ChangedValues.TYPE_TRANS);
				fillChangedValues(cv, m);
				builder.accept(cv);
			});
		}
		return builder.build();
	}
	
	public Stream<String> getBasketSymbol(String basketId) {
		log.info("getBasketSymbol:: basketId {}", basketId);
		BasketInfor infor = getBasketInfor(basketId);
		return infor.stream();
	}

	public String getMarketFlag(String marketId) {
		MarketInfor marketInfor = MARKET_INFOR_REPO.get(marketId);
		if (marketInfor == null) {
			return "";
		}
		return marketInfor.getMarketFlag();
	}
	
	public String getIndexFlag(String indexId) {
		IndexInfor indexInfor = INDEX_INFOR_REPO.get(indexId);
		if (indexInfor == null) {
			return "";
		}
		return getMarketFlag(indexInfor.getMarketId());
	}
	
	public RefInfor getRefInfor(String symbol) {
		StaticInfor infor = null;
		if (getSecurityInfor(symbol) != null) {
			infor = getSecurityInfor(symbol).getStaticInfor();
		} else {
			log.warn("getRefInfor: getSecurityInfor by symbol return null! Get data from stockBoardDataAccess! symbol {}", symbol);
			infor = stockBoardDataAccess.getStaticInfor(symbol);
		}
		
//		infor = stockBoardDataAccess.getStaticInfor(symbol);
		
		if (infor != null) {
			String ceiling = AlgoMessage.NUMBER_FORMAT.format(BigDecimal.valueOf(infor.getCeilingPrice()).divide(BigDecimalConstant.THOUSAND, 2, RoundingMode.HALF_UP));
			String floor = AlgoMessage.NUMBER_FORMAT.format(BigDecimal.valueOf(infor.getFloorPrice()).divide(BigDecimalConstant.THOUSAND, 2, RoundingMode.HALF_UP));
			String basic = AlgoMessage.NUMBER_FORMAT.format(BigDecimal.valueOf(infor.getBasicPrice()).divide(BigDecimalConstant.THOUSAND, 2, RoundingMode.HALF_UP));
			String fullName = infor.getFullName();
			String benefit = infor.getBenefit();
			String meeting = infor.getMeeting();
			String split = infor.getSplit();
			String isMargin = infor.getIsMargin();
			
//			RefInfor refInfor = RefInfor.of(symbol, ceiling, floor, basic, fullName, benefit, meeting, split, isMargin);
			RefInfor refInfor = RefInfor.builder().symbol(symbol).ceiling(ceiling).floor(floor).basic(basic)
					.fullName(fullName).benefit(benefit).meeting(meeting).split(split).isMargin(isMargin).build();
			return refInfor;
		} else {
			log.warn("getRefInfor: StaticInfor by symbol is null! symbol {}", symbol);
			return null;
		}
	}

	public Stream<ChangedValues> initDay() {
		log.info("INIT_DAY!!!!!!!!!");
		Stream.Builder<ChangedValues> builder = Stream.builder();
		
		try {
			log.info("initDay: prepared call: initIndexInfor");
			initIndexInfor();
			log.info("initDay: done call: initIndexInfor");
		} catch (Exception e) {
			log.error("initDay: call error: initIndexInfor", e);
		}

		
		streamAllSecurityInfor()
			.filter((s) -> s != null).forEach((s) -> {
			try {
				s.initDay();
				ChangedValues cv = ChangedValues.of(s.getSymbol(), ChangedValues.TYPE_SECURITY);
				fillChangedValues(cv, s);
				builder.accept(cv);
			} catch (Exception e) {
				log.error("streamAllSecurityInfor::: error", e);
			}
		});
		
		streamAllIndexInfor()
			.filter((s) -> s != null).forEach((s) -> {
			try {
				s.initDay();
				ChangedValues cv = ChangedValues.of(s.getIndexId(), ChangedValues.TYPE_INDEX);
				fillChangedValues(cv, s);
				builder.accept(cv);
			} catch (Exception e) {
				log.error("streamAllIndexInfor::: error", e);
			}
		});
		
		return builder.build();
	}
	
	private void  processHnxServerDate(String hnxServerDate) {
		log.info("processHnxServerDate: {}", hnxServerDate);
		try {
			LocalDate hnxServerLocalDate = LocalDate.parse(hnxServerDate, DateTimeFormatter.ofPattern("yyyyMMdd"));
			MilestoneLocalTime.setCurrentDate(hnxServerLocalDate);
		} catch (Exception e) {
			log.error(String.format("processHnxServerDate: %s ERROR!", hnxServerDate), e);
		}
	}
	
	private void processHnxServerTime(String hnxServerTime) {
			log.info("processHnxServerTime: {}", hnxServerTime);
			LocalTime hnxServerLocalTime = MilestoneLocalTime.of(hnxServerTime);
			LocalTime priorHnxServerLocalTime = MilestoneLocalTime.getCurrentTime();
			
			if (priorHnxServerLocalTime != null && priorHnxServerLocalTime.compareTo(hnxServerLocalTime) == 0) {
				log.info("processHnxServerTime: {} return by no change! ", hnxServerTime);
				return;
			}
			
			MilestoneLocalTime.setCurrentTime(hnxServerLocalTime);

			if (hnxServerLocalTime.isAfter(MilestoneLocalTime.FDS_ATO) && hnxServerLocalTime.isBefore(MilestoneLocalTime.FDS_CNT_MORNING)) {
				MilestoneLocalTime.doTaskEverySecond(hnxServerLocalTime, 15, () -> {
					log.info("processHnxServerTime: setMarketFlag {}", "DER_P");
					MarketInfor infor = getMarketInfor(MarketInfor.HNX);
					infor.setMarketFlag("DER_P");
				});
			}
			
			if (hnxServerLocalTime.isAfter(MilestoneLocalTime.INIT_DAY_START) && hnxServerLocalTime.isBefore(MilestoneLocalTime.INIT_DAY_END)) {
				MilestoneLocalTime.doTaskEverySecond(hnxServerLocalTime, 30, () -> {
					log.info("processHnxServerTime: INIT_DAY");
					systemSignalProcessor.enqueueSystemSignal(InitDaySignal.of(hnxServerLocalTime));
					log.info("processHnxServerTime: NEW HNX_FDS_VN30");
					BasketInfor infor = getBasketInfor(BasketInfor.HNX_FDS_VN30);
					infor.clear();
					DerivativeU.getAvailableSymbols(UnderlyingSymbol.VN30, MilestoneLocalTime.getCurrentDate())
						.peek((s) -> log.info("processHnxServerTime: getAvailableSymbols: VN30: symbol: {}", s))
						.forEach(infor::add);
				});
			}
			
			if (hnxServerLocalTime.isAfter(MilestoneLocalTime.HNX_CLOSE_MARKET)) {
				MilestoneLocalTime.doTaskEverySecond(hnxServerLocalTime, 30, () -> {
					log.info("processHnxServerTime: CLOSE_MARKET start");
					masterJpaRepository.processEndOfDay(streamAllSecurityInfor());
					streamAllIndexInfor()
						.filter((s) -> s != null).forEach((s) -> {
							try {
								log.info("processHnxServerTime: CLOSE_MARKET, summary INDEX: IndexId: {}", s.getIndexId());
								redisSummaryInforRepo.summary(InforType.INDEX, s.getIndexId());
							} catch (Exception e) {
								log.error("processHnxServerTime: CLOSE_MARKET::: error", e);
							}
						});
					log.info("processHnxServerTime: CLOSE_MARKET end");
				});
			}
			
			
		}

	private void refresh() {
		log.info("refresh!");
//		Executors.newSingleThreadExecutor().execute(stockBoardDataAccess::refreshStaticInfor);
		stockBoardDataAccess.refreshStaticInfor();
	}
	
	private Stream<SecurityInfor> streamAllSecurityInfor() {
		log.info("streamAllSecurityInfor:: SECURITY_INFOR_REPO.size: {}", SECURITY_INFOR_REPO.size());
		Stream.Builder<SecurityInfor> builder = Stream.builder();
		SECURITY_INFOR_REPO.values().stream().forEach(builder::accept);
		return builder.build();
	}
	
	private Stream<IndexInfor> streamAllIndexInfor() {
		log.info("streamAllIndexInfor:: INDEX_INFOR_REPO.size: {}", INDEX_INFOR_REPO.size());
		Stream.Builder<IndexInfor> builder = Stream.builder();
		INDEX_INFOR_REPO.values().stream().forEach(builder::accept);
		return builder.build();
	}

	private MarketInfor getMarketInfor(String marketInforId) {
		return MARKET_INFOR_REPO.get(marketInforId);
	}

	private SecurityInfor getSecurityInfor(String symbol) {
		return SECURITY_INFOR_REPO.get(symbol);
	}
	
	private IndexInfor getIndexInfor(String indexId) {
		if (!INDEX_INFOR_REPO.containsKey(indexId)) {
			INDEX_INFOR_REPO.put(indexId, IndexInfor.of(indexId));
		}
		return INDEX_INFOR_REPO.get(indexId);
	}
	
	private BasketInfor getBasketInfor(String basketId) {
		if (!BASKET_INFOR_REPO.containsKey(basketId)) {
			BASKET_INFOR_REPO.put(basketId, BasketInfor.of(basketId));
		}
		return BASKET_INFOR_REPO.get(basketId);
	}

	private void setMarketInfor(String marketInforId, MarketInfor marketInfor) {
		MARKET_INFOR_REPO.put(marketInforId, marketInfor);
	}

	private void setSecurityInfor(String symbol, SecurityInfor newSecInfor) {
		symbol = symbol.trim();
		SecurityInfor cachedSecurityInfor = getSecurityInfor(symbol);
		if (cachedSecurityInfor == null) {
			SECURITY_INFOR_REPO.put(symbol, newSecInfor);
		} else {
			SECURITY_INFOR_REPO.put(symbol, newSecInfor);
		}
	}
	
	private void setIndexInfor(String indexId, IndexInfor infor) {
		INDEX_INFOR_REPO.put(indexId, infor);
	}
	
	private void setBasketInfor(String basketId, BasketInfor infor) {
		BASKET_INFOR_REPO.put(basketId, infor);
	}
	
	private void fillChangedValues(ChangedValues cv, AdvancedInfor infor) {
		fillChangedValues(cv, null, infor);
	} 

	private void fillChangedValues(ChangedValues cv, ProjectedOpen cInfor, ProjectedOpen nInfor) {
		if (cInfor != null && nInfor != null) {
			if (nInfor.getPrice() >= 0)
				if (cInfor.getPrice() != nInfor.getPrice()) {
					cv.put(AlgoMessageFields.FINISHPRICE, nInfor.getPrice());
					cv.put(AlgoMessageFields.FINISHAMOUNT, 0);
					cv.put(AlgoMessageFields.DIFF, nInfor.getChange());
					cv.put(AlgoMessageFields.DIFFRATE, nInfor.getChangePercent());
				}
		} else if (nInfor != null) {
			if (nInfor.getPrice() >= 0) {
				cv.put(AlgoMessageFields.FINISHPRICE, nInfor.getPrice());
				cv.put(AlgoMessageFields.FINISHAMOUNT, 0);
				cv.put(AlgoMessageFields.DIFF, nInfor.getChange());
				cv.put(AlgoMessageFields.DIFFRATE, nInfor.getChangePercent());	
			}
		}
	}

	private void fillChangedValues(ChangedValues cv, IndexInfor nInfor) {
		fillChangedValues(cv, null, nInfor);
	}


	private void fillChangedValues(ChangedValues cv, ForeignInfor cInfor) {
		fillChangedValues(cv, null, cInfor);
	}

	private void fillChangedValues(ChangedValues cv, ProjectedOpen infor) {
		fillChangedValues(cv, null, infor);
	}

	private void fillChangedValues(ChangedValues cv, SecurityInfor infor) {
		fillChangedValues(cv, null, infor);
	}
	
	private void fillChangedValues(ChangedValues cv, StaticInfor infor) {
		fillChangedValues(cv, null, infor);
	}
	
	private void fillChangedValues(ChangedValues cv, DynamicInfor infor) {
		fillChangedValues(cv, null, infor);
	}
	
	private void fillChangedValues(ChangedValues cv, MatchedOrderInfor infor) {
		if (infor != null) {
			int price = infor.getPrice();
			long volume = infor.getVolume();
			int change = infor.getChange();
			String changePercent = infor.getChangePercent();
			String time = infor.getTime();

			cv.put(AlgoMessageFields.FINISHPRICE, price);
			cv.put(AlgoMessageFields.FINISHAMOUNT, volume);
			cv.put(AlgoMessageFields.DIFF, change);
			cv.put(AlgoMessageFields.DIFFRATE, changePercent);
//			cv.put(AlgoMessageFields.TIME, time));
		} else {
			cv.put(AlgoMessageFields.FINISHPRICE, "");
			cv.put(AlgoMessageFields.FINISHAMOUNT, "");
			cv.put(AlgoMessageFields.DIFF, "");
			cv.put(AlgoMessageFields.DIFFRATE, "");
		}
	}

	private void fillChangedValues(ChangedValues cv, IndexInfor cInfor, IndexInfor nInfor) {
		if (cInfor != null && nInfor != null && cInfor != nInfor) {
			int cAdvances = cInfor.getAdvances();
			BigDecimal cChange = cInfor.getChange();
			BigDecimal cChangeRate = cInfor.getChangeRate();
			int cDeclines = cInfor.getDeclines();
			BigDecimal cIndexValue = cInfor.getIndexValue();
			int cNoChange = cInfor.getNoChange();
			long cTotalTrade = cInfor.getTotalTrade();
			long cTotalValue = cInfor.getTotalValue();
			long cTotalVolume = cInfor.getTotalVolume();
			long cTotalValuePt = cInfor.getTotalValuePt();
			long cTotalVolumePt = cInfor.getTotalVolumePt();
			int cCe = cInfor.getNumOfCe();
			int cFl = cInfor.getNumOfFl();
			int cNt = cInfor.getNoTrade();
			
			int nAdvances = nInfor.getAdvances();
			BigDecimal nChange = nInfor.getChange();
			BigDecimal nChangeRate = nInfor.getChangeRate();
			int nDeclines = nInfor.getDeclines();
			BigDecimal nIndexValue = nInfor.getIndexValue();
			int nNoChange = nInfor.getNoChange();
			long nTotalTrade = nInfor.getTotalTrade();
			long nTotalValue = nInfor.getTotalValue();
			long nTotalVolume = nInfor.getTotalVolume();
			long nTotalValuePt = nInfor.getTotalValuePt();
			long nTotalVolumePt = nInfor.getTotalVolumePt();
			int nCe = nInfor.getNumOfCe();
			int nFl = nInfor.getNumOfFl();
			int nNt = nInfor.getNoTrade();
			
			if (cIndexValue == null || cIndexValue.compareTo(nIndexValue) != 0) {
				cv.put(AlgoMessageFields.Index.INDEX, nIndexValue);
			}
			
			if (cAdvances != nAdvances) {
				cv.put(AlgoMessageFields.Index.ADVANCES, nAdvances);
			}
			if (cDeclines != nDeclines) {
				cv.put(AlgoMessageFields.Index.DECLINES, nDeclines);
			}
			if (cNoChange != nNoChange) {
				cv.put(AlgoMessageFields.Index.NOCHANGE, nNoChange);
			}
			
			if (cChange == null || cChange.compareTo(nChange) != 0) {
				cv.put(AlgoMessageFields.Index.DIFF, nChange);
				cv.put(AlgoMessageFields.Index.DIFFRATE, nChangeRate);
			}
			
			if (cTotalTrade != nTotalTrade) {
				cv.put(AlgoMessageFields.Index.TOTALTRADE, nTotalTrade);
			}
			if (cTotalValue != nTotalValue) {
				cv.put(AlgoMessageFields.Index.TOTAL, nTotalValue);
			}
			if (cTotalVolume != nTotalVolume) {
				cv.put(AlgoMessageFields.Index.TOTALSHARE, nTotalVolume);
			}
			if (cTotalValuePt != nTotalValuePt) {
				cv.put(AlgoMessageFields.Index.PTTOTAL, nTotalValuePt);
			}
			if (cTotalVolumePt != nTotalVolumePt) {
				cv.put(AlgoMessageFields.Index.PTTOTALSHARE, nTotalVolumePt);
			}
			if (cCe != nCe) {
				cv.put(AlgoMessageFields.Index.CEILING, nCe);
			}
			if (cFl != nFl) {
				cv.put(AlgoMessageFields.Index.FLOOR, nFl);
			}
			if (cNt != nNt) {
				cv.put(AlgoMessageFields.Index.NOTRADE, nNt);
			}
		} else if (nInfor != null) {
			int nAdvances = nInfor.getAdvances();
			BigDecimal nChange = nInfor.getChange();
			BigDecimal nChangeRate = nInfor.getChangeRate();
			int nDeclines = nInfor.getDeclines();
			BigDecimal nIndexValue = nInfor.getIndexValue();
			int nNoChange = nInfor.getNoChange();
			long nTotalTrade = nInfor.getTotalTrade();
			long nTotalValue = nInfor.getTotalValue();
			long nTotalVolume = nInfor.getTotalVolume();
			long nTotalValuePt = nInfor.getTotalValuePt();
			long nTotalVolumePt = nInfor.getTotalVolumePt();
			
			int nCe = nInfor.getNumOfCe();
			int nFl = nInfor.getNumOfFl();
			int nNt = nInfor.getNoTrade();
			
			cv.put(AlgoMessageFields.Index.INDEX, nIndexValue);
			cv.put(AlgoMessageFields.Index.ADVANCES, nAdvances);
			cv.put(AlgoMessageFields.Index.DECLINES, nDeclines);
			cv.put(AlgoMessageFields.Index.NOCHANGE, nNoChange);
			cv.put(AlgoMessageFields.Index.DIFF, nChange);
			cv.put(AlgoMessageFields.Index.DIFFRATE, nChangeRate);
			cv.put(AlgoMessageFields.Index.TOTALTRADE, nTotalTrade);
			cv.put(AlgoMessageFields.Index.TOTAL, nTotalValue);
			cv.put(AlgoMessageFields.Index.TOTALSHARE, nTotalVolume);
			cv.put(AlgoMessageFields.Index.PTTOTAL, nTotalValuePt);
			cv.put(AlgoMessageFields.Index.PTTOTALSHARE, nTotalVolumePt);
			
			cv.put(AlgoMessageFields.Index.CEILING, nCe);
			cv.put(AlgoMessageFields.Index.FLOOR, nFl);
			cv.put(AlgoMessageFields.Index.NOTRADE, nNt);

		}

	}

	private void fillChangedValues(ChangedValues cv, AdvancedInfor cinfor, AdvancedInfor ninfor) {
		if (cinfor != null && ninfor != null && cinfor != ninfor) {
			BigDecimal cDiff30 = cinfor.getDiffVn30();
			BigDecimal cBasis = cinfor.getBasis();
			BigDecimal cBalancePrice = cinfor.getBalancePrice();
			BigDecimal cOi = cinfor.getOpenInterest();

			BigDecimal nDiff30 = ninfor.getDiffVn30();
			BigDecimal nBasis = ninfor.getBasis();
			BigDecimal nBalancePrice = ninfor.getBalancePrice();
			BigDecimal nOi = ninfor.getOpenInterest();


			if (cDiff30.compareTo(nDiff30) != 0) {
				cv.put(AlgoMessageFields.DIFFVN30, nDiff30);
			}
			if (cBasis.compareTo(nBasis) != 0) {
				cv.put(AlgoMessageFields.BASIS, nBasis);
			}
			if (cBalancePrice.compareTo(nBalancePrice) != 0) {
				cv.put(AlgoMessageFields.BALANCEPRICE, nBalancePrice);
			}
			if (cOi.compareTo(nOi) != 0) {
				cv.put(AlgoMessageFields.OPENINTEREST, nOi);
			}
		} else if (ninfor != null) {
			BigDecimal nDiff30 = ninfor.getDiffVn30();
			BigDecimal nBasis = ninfor.getBasis();
			BigDecimal nBalancePrice = ninfor.getBalancePrice();
			BigDecimal nOi = ninfor.getOpenInterest();

			cv.put(AlgoMessageFields.DIFFVN30, nDiff30);
			cv.put(AlgoMessageFields.BASIS, nBasis);
			cv.put(AlgoMessageFields.BALANCEPRICE, nBalancePrice);
			cv.put(AlgoMessageFields.OPENINTEREST, nOi);
		}
	}

	private void fillChangedValues(ChangedValues cv, SecurityInfor cachedSecInfor, SecurityInfor newSecInfor) {
		if (cachedSecInfor != null && newSecInfor != null && cachedSecInfor != newSecInfor) {			
//			fillChangedValues(cv, cachedSecInfor.getStaticInfor(), newSecInfor.getStaticInfor());
//			fillChangedValues(cv, cachedSecInfor.getDynamicInfor(), newSecInfor.getDynamicInfor());
		} else
		if (newSecInfor != null) {
			SecurityInfor infor = newSecInfor;
			
			fillChangedValues(cv, infor.getStaticInfor());
			fillChangedValues(cv, infor.getLastMatchedOrder());
			fillChangedValues(cv, infor.getDynamicInfor());
		}
	}

	private void fillChangedValues(ChangedValues cv, DynamicInfor cachedInfor, DynamicInfor newInfor) {
		if (cachedInfor != null && newInfor != null && cachedInfor != newInfor) {
			int cHighest = cachedInfor.getHighest();
			int cLowest = cachedInfor.getLowest();
			int cOpen = cachedInfor.getOpen();
			
			int nHighest = newInfor.getHighest();
			int nLowest = newInfor.getLowest();
			int nOpen = newInfor.getOpen();
			
			long cachedTotalValue = cachedInfor.getTotalValue();
			long cachedTotalVolume = cachedInfor.getTotalVolume();
			long newTotalValue = newInfor.getTotalValue();
			long newTotalVolume = newInfor.getTotalVolume();
			
			long cachedAverage = cachedInfor.getAverage();
			long newAverage = newInfor.getAverage();
			
			long cTotalBuyQtty = cachedInfor.getTotalBuyQtty();
			long nTotalBuyQtty = newInfor.getTotalBuyQtty();
			long cTotalSellQtty = cachedInfor.getTotalSellQtty();
			long nTotalSellQtty = newInfor.getTotalSellQtty();
			
			if (cTotalBuyQtty != nTotalBuyQtty) {
				cv.put(AlgoMessageFields.TOTALBIDQUANTITY, nTotalBuyQtty);
			}
			
			if (cTotalSellQtty != nTotalSellQtty) {
				cv.put(AlgoMessageFields.TOTALOFFERQUANTITY, nTotalSellQtty);
			}
			
			if (newTotalValue > cachedTotalValue) {
				cv.put(AlgoMessageFields.TOTALVALUE, newTotalValue);
				cv.put(AlgoMessageFields.TOTALSHARE, newTotalVolume);
//				int averagePrice = BigDecimal.valueOf(newTotalValue).divide(BigDecimal.valueOf(newTotalVolume), 0, RoundingMode.HALF_UP).intValue();
//				cv.put(AlgoMessageFields.AVERAGEPRICE, averagePrice);
			}
			
			if (cachedAverage != newAverage) {
				cv.put(AlgoMessageFields.AVERAGEPRICE, newAverage);
			}
												
			if (cHighest != nHighest) {
				cv.put(AlgoMessageFields.HIGHEST, nHighest);
			}
			if (cLowest != nLowest) {
				cv.put(AlgoMessageFields.LOWEST, nLowest);
			}
			if (cOpen != nOpen) {
				cv.put(AlgoMessageFields.OPENPRICE, nOpen);
			}
//			fillChangedValues(cv, cachedInfor.getTopBuy(), newInfor.getTopBuy());
//			fillChangedValues(cv, cachedInfor.getTopSell(), newInfor.getTopSell());
//			fillChangedValues(cv, cachedInfor.getForeignInfor(), newInfor.getForeignInfor());
//			fillChangedValues(cv, cachedInfor.getProjectedOpen(), newInfor.getProjectedOpen());
		} else if (newInfor != null) {
			int nHighest = newInfor.getHighest();
			int nLowest = newInfor.getLowest();
			int nOpen = newInfor.getOpen();
			
			long totalValue = newInfor.getTotalValue();
			long totalVolume = newInfor.getTotalVolume();
			long newAverage = newInfor.getAverage();
			long nTotalBuyQtty = newInfor.getTotalBuyQtty();
			long nTotalSellQtty = newInfor.getTotalSellQtty();
		
			cv.put(AlgoMessageFields.TOTALBIDQUANTITY, nTotalBuyQtty);
			cv.put(AlgoMessageFields.TOTALOFFERQUANTITY, nTotalSellQtty);
			
			cv.put(AlgoMessageFields.TOTALVALUE, totalValue);
			cv.put(AlgoMessageFields.TOTALSHARE, totalVolume);
//			if (totalVolume > 0) {
//				int averagePrice = BigDecimal.valueOf(totalValue).divide(BigDecimal.valueOf(totalVolume), 0, RoundingMode.HALF_UP).intValue();
//				cv.put(AlgoMessageFields.AVERAGEPRICE, averagePrice);	
//			}
			
			cv.put(AlgoMessageFields.HIGHEST, nHighest);
			cv.put(AlgoMessageFields.LOWEST, nLowest);
			cv.put(AlgoMessageFields.OPENPRICE, nOpen);
			cv.put(AlgoMessageFields.AVERAGEPRICE, newAverage);
			fillChangedValues(cv, null, newInfor.getTopBuy());
			fillChangedValues(cv, null, newInfor.getTopSell());
			fillChangedValues(cv, newInfor.getForeignInfor());
			fillChangedValues(cv, newInfor.getAdvancedInfor());
			if (newInfor.getProjectedOpen() != null && newInfor.getProjectedOpen().getPrice() >= 0)				
				fillChangedValues(cv, newInfor.getProjectedOpen());
		}

	}

	private void fillChangedValues(ChangedValues cv, ForeignInfor cInfor, ForeignInfor nInfor) {
		if (cInfor != null && nInfor != null && cInfor != nInfor) {
			long cTotalRoom = cInfor.getTotalRoom();
			long cCurrentRoom = cInfor.getCurrentRoom();
			long cBuyVolume = cInfor.getBuyVolume();
			long cSellVolume = cInfor.getSellVolume();
			BigDecimal cRemainPercent = cInfor.getRemainPercent();

			long totalRoom = nInfor.getTotalRoom();
			long currentRoom = nInfor.getCurrentRoom();
			long buyVolume = nInfor.getBuyVolume();
			long sellVolume = nInfor.getSellVolume();
			BigDecimal nRemainPercent = nInfor.getRemainPercent();
			
			if (cCurrentRoom != currentRoom)
				cv.put(AlgoMessageFields.REMAINROOM, currentRoom);
			if (cTotalRoom != totalRoom)
				cv.put(AlgoMessageFields.TOTALROOM, totalRoom);
			if (cSellVolume != sellVolume)
				cv.put(AlgoMessageFields.FGSELLQUANTITY, sellVolume);
			if (cBuyVolume != buyVolume)
				cv.put(AlgoMessageFields.FGBUYQUANTITY, buyVolume);
			if (cRemainPercent.compareTo(nRemainPercent)  != 0)
				cv.put(AlgoMessageFields.REMAINROOMPCT, nRemainPercent);

		} else if (cInfor == null && nInfor != null) {
			long totalRoom = nInfor.getTotalRoom();
			long currentRoom = nInfor.getCurrentRoom();
			long buyVolume = nInfor.getBuyVolume();
			long sellVolume = nInfor.getSellVolume();
			BigDecimal nRemainPercent = nInfor.getRemainPercent();

			cv.put(AlgoMessageFields.REMAINROOM, currentRoom);
			cv.put(AlgoMessageFields.TOTALROOM, totalRoom);
			cv.put(AlgoMessageFields.FGSELLQUANTITY, sellVolume);
			cv.put(AlgoMessageFields.FGBUYQUANTITY, buyVolume);
			cv.put(AlgoMessageFields.REMAINROOMPCT, nRemainPercent);
		}
	}

	private void fillChangedValues(ChangedValues cv, StaticInfor cachedInfor, StaticInfor newInfor) {
		if (cachedInfor != null && newInfor != null) {
			int cBasic = cachedInfor.getBasicPrice();
			int cCeil = cachedInfor.getCeilingPrice();
			int cFloor = cachedInfor.getFloorPrice();
			SecurityType cSecType = cachedInfor.getSecurityType();

			int nBasic = newInfor.getBasicPrice();
			int nCeil = newInfor.getCeilingPrice();
			int nFloor = newInfor.getFloorPrice();
			SecurityType nSecType = newInfor.getSecurityType();

			if (cBasic != nBasic) {
//				cv.put(AlgoMessageFields.BASIS, nBasic));
			}

			if (cCeil != nCeil) {
//				cv.put(AlgoMessageFields.BASIS, nCeil));
			}

			if (cFloor != nFloor) {

			}
			
			if (cSecType != nSecType) {

			}

		} else if (newInfor != null || cachedInfor == newInfor) {
			
		}

	}

	private void fillChangedValues(ChangedValues cv, TopPrice cachedInfor, TopPrice newInfor) {
		String[] priceFieldSet = TopPrice.SIDE_BUY.equalsIgnoreCase(newInfor.getSide())
				? AlgoMessageFields.TOP_10_BUY_PRICE_FIELDS
				: AlgoMessageFields.TOP_10_SELL_PRICE_FIELDS;
		String[] priceVolumeSet = TopPrice.SIDE_BUY.equalsIgnoreCase(newInfor.getSide())
				? AlgoMessageFields.TOP_10_BUY_VOLUME_FIELDS
				: AlgoMessageFields.TOP_10_SELL_VOLUME_FIELDS;
		
		if (cachedInfor != null && newInfor != null && cachedInfor != newInfor) {
			if (cachedInfor.getSide().equalsIgnoreCase(newInfor.getSide())) {
				int newSize = newInfor.size();
				int cachedSize = cachedInfor.size();
				int size = Math.max(newSize, cachedSize);
				for (int i = 1; i <= size; i++) {
					int cachedPrice = cachedInfor.getTopPrice(i);
					int cachedVolume = cachedInfor.getTopVolume(i);
					int newPrice = newInfor.getTopPrice(i);
					int newVolume = newInfor.getTopVolume(i);
					if (cachedPrice != newPrice) {
						String newValue = String.valueOf(newPrice);
						if (i == 1 && newPrice < 0) {
							newValue = newPrice == TopPrice.ATC_PRICE ? "ATC" : newPrice == TopPrice.ATO_PRICE ? "ATO" : "";
							log.info("Found ATO ATC price: newPrice {} newValue {}", newPrice, newValue);
						}
						cv.put(priceFieldSet[i], newValue);
					}
					if (cachedVolume != newVolume) {
						cv.put(priceVolumeSet[i], newVolume);
					}
				}
			}
		} else
		if (newInfor != null) {
//			int size = newInfor.size();
			int size = priceFieldSet.length - 1;
			for (int i = 1; i <= size; i++) {
				int newPrice = newInfor.getTopPrice(i);
				int newVolume = newInfor.getTopVolume(i);
				String newValue = String.valueOf(newPrice);

				if (i == 1 && newPrice < 0) {
					newValue = (newPrice == TopPrice.ATC_PRICE) ? "ATC" : ((newPrice == TopPrice.ATO_PRICE) ? "ATO" : "");
					log.info("Found ATO ATC price: newPrice {} newValue {}", newPrice, newValue);
				}
				cv.put(priceFieldSet[i], newValue);
				cv.put(priceVolumeSet[i], newVolume);
			}
		}
	}

	private int getAtoAtcPrice() {
		if (HoseSystemControlCode.ATO.equalsIgnoreCase(getMarketFlag(MarketInfor.HOSE))) {
			return TopPrice.ATO_PRICE;
		} else 
		if (HoseSystemControlCode.ATC.equalsIgnoreCase(getMarketFlag(MarketInfor.HOSE))) {
			return TopPrice.ATC_PRICE;
		} else {
			LocalTime currentTime = MilestoneLocalTime.getCurrentTime();
			log.info("getAtoAtcPrice use hnxTime to detect: {}", currentTime);
			if (currentTime.isAfter(MilestoneLocalTime.FDS_ATO) && currentTime.isBefore(MilestoneLocalTime.HOSE_CNT_MORNING.plusSeconds(10))) {
				return TopPrice.ATO_PRICE;
			} else if (currentTime.isAfter(MilestoneLocalTime.HOSE_ATC.minusSeconds(10))) {
				return TopPrice.ATC_PRICE;
			}
			return 0;
		}
		
		
	}

	private long countCeFl(Stream<String> symbols, String ce_Or_fl) {
		return symbols.filter((symbol) -> {
			SecurityInfor secInfor = getSecurityInfor(symbol);
			if (secInfor == null) {
				log.warn("countCeFl: cannot get SecurityInfor, symbol {}", symbol);
				return false;
			} else {
				if (secInfor.getLastMatchedOrder() != null) {
					if ("ce".equalsIgnoreCase(ce_Or_fl)) {
						return secInfor.getLastMatchedOrder().getPrice() == secInfor.getStaticInfor().getCeilingPrice();
					}
					if ("fl".equalsIgnoreCase(ce_Or_fl)) {
						return secInfor.getLastMatchedOrder().getPrice() == secInfor.getStaticInfor().getFloorPrice();
					}
					return false;
				} else {
					if ("no".equalsIgnoreCase(ce_Or_fl)) {
						return true;
					}
					return false;
				}
			}
		}).count();
	}

	private SecurityInfor newCurrentSecInfor(String symbol, SS message) {
		SecurityInfor securityInfor = SecurityInfor.of(symbol);
		int ceilingPrice = message.getCeiling() * TEN;
		int floorPrice = message.getFloorPrice() * TEN;
		int basicPrice = message.getPriorClosePrice() * TEN;
		String benefit = String.valueOf(message.getBenefit()).trim();
		if ("ADR".contains(benefit)) {
			StaticInfor algoStaticInfor = getAlgoStaticInfor(symbol);
			if (algoStaticInfor != null && algoStaticInfor.getBasicPrice() > 0)
				basicPrice = algoStaticInfor.getBasicPrice();
			else {
				basicPrice = calculateBasiPrice(TickSize.HOSE, ceilingPrice, floorPrice);
				log.info("algoStaticInfor is NULL! symbol: {} basicPrice: {}", symbol, basicPrice);
			}
		}
		StaticInfor staticInfor = securityInfor.getStaticInfor();
		staticInfor.setBasicPrice(basicPrice);
		staticInfor.setCeilingPrice(ceilingPrice);
		staticInfor.setFloorPrice(floorPrice);
		staticInfor.setSecurityType(getHoseSecurityType(message.getSecurityType()));
		return securityInfor;
	}

	private StaticInfor getAlgoStaticInfor(String symbol) {
		return stockBoardDataAccess.getStaticInfor(symbol);
	}

	private int calculateBasiPrice(TickSize tickSize, int ceiling, int floor) {
		int average = (ceiling + floor) / 2;
		int tick = tickSize.getTick(average);
		try {
			if (tick > 0) {
				return BigDecimal.valueOf(average).divide(BigDecimal.valueOf(tick), 0, RoundingMode.HALF_UP).intValue() * tick;
			} else {
				return average;
			}
		} catch (Exception e) {
			log.error("calculateBasiPrice error: tickSize: {} ce {} fl {}", tickSize, ceiling, floor);
			log.error("", e);
			return average;
		}
	}

	private SecurityType getHnxSecurityType(String securityType) {
		securityType = securityType.trim();
		if ("BO".equalsIgnoreCase(securityType)) {
			return SecurityType.BOND;
		} else if ("ST".equalsIgnoreCase(securityType)) {
			return SecurityType.STOCK;
		} else if ("EF".equalsIgnoreCase(securityType)) {
			return SecurityType.ETF;
		} else if ("BO".equalsIgnoreCase(securityType)) {
			return SecurityType.BOND;
		} else if ("MF".equalsIgnoreCase(securityType)) {
			return SecurityType.ETF;
		} else if ("FU".equalsIgnoreCase(securityType)) {
			return SecurityType.FDS;
		}
		return SecurityType.UNDEFINE;
	}

	private SecurityType getHoseSecurityType(String securityType) {
		securityType = securityType.trim();
		if ("B".equalsIgnoreCase(securityType)) {
			return SecurityType.BOND;
		} else if ("S".equalsIgnoreCase(securityType)) {
			return SecurityType.STOCK;
		} else if ("U".equalsIgnoreCase(securityType)) {
			return SecurityType.ETF;
		} else if ("D".equalsIgnoreCase(securityType)) {
			return SecurityType.BOND;
		} else if ("E".equalsIgnoreCase(securityType)) {
			return SecurityType.ETF;
		} else if ("W".equalsIgnoreCase(securityType)) {
			return SecurityType.CW;
		}
		return SecurityType.UNDEFINE;
	}

	private void initRepo() {
		SECURITY_INFOR_REPO.clear();
		MARKET_INFOR_REPO.clear();
		INDEX_INFOR_REPO.clear();
		TRANSLOG_PUBLISH_OBJECT_REPO.clear();
		
		initSecurityInfor();
		initMarketInfor();
		initIndexInfor();
		initTransLogPublishObject();
	}
	
	private TimerTask getManualCalculateIndexTask() {
		TimerTask manualCalculateIndexTask = new TimerTask() {
			@Override
			public void run() {
				log.info("manualCalculateIndexTask start!");
				Stream.of(indexListStr.split(",")).forEach((hoseIdx) -> {
					String indexId = IndexInfor.getIndexId(hoseIdx);
					processManualCalculateIndexTask(indexId);
				});
				{
					String indexId = IndexInfor.HOSE;
					processManualCalculateIndexTask(indexId);
				}
				log.info("manualCalculateIndexTask end!");
			}
		};
		return manualCalculateIndexTask;
	};
		
	private void processManualCalculateIndexTask(String indexId) {
		stockBoardDataAccess.calculatedIndex(indexId, (e) -> {
			log.info("processManualCalculateIndexTask: {}, calculatedIndex {}", indexId, e);
			
			IndexInfor current = getIndexInfor(indexId);
			current.setMarketId(MarketInfor.HOSE);
			IndexInfor newInfor = current.clone();
			
			BigDecimal indexValue = e;
			
			newInfor.setIndexValue(indexValue);
			BigDecimal priorIndex = newInfor.getPriorIndex();
			
			if (indexValue == null || indexValue.compareTo(BigDecimal.ZERO) == 0) {
				log.info("processManualCalculateIndexTask: indexValue is null or equal ZERO, do nothing! value: {}", indexValue);
				return;
			}
			
			if (priorIndex != null && priorIndex.compareTo(BigDecimal.ZERO) != 0) {
				newInfor.setChange(indexValue.subtract(priorIndex));
				newInfor.setChangeRate(newInfor.getChange().multiply(BigDecimalConstant.HUNDRED).divide(priorIndex, 2, RoundingMode.HALF_UP));
				log.info("processManualCalculateIndexTask: priorIndex {} indexValue {} change {} rate {}", priorIndex, indexValue, newInfor.getChange(), newInfor.getChangeRate());
			} else {
				log.info("processManualCalculateIndexTask: priorIndex is null or zero {} indexValue {}", priorIndex, indexValue);
			}
			
			ChangedValues cv = ChangedValues.of(indexId, ChangedValues.TYPE_INDEX);
			fillChangedValues(cv, current, newInfor);
			setIndexInfor(indexId, newInfor);
			Map<String, ChangedValues> sigletonMap = Collections.singletonMap("calculatedIndex", cv);
			log.info("processManualCalculateIndexTask: sigletonChangedValuesMap : {}", sigletonMap);
			
			log.info("processManualCalculateIndexTask: summary INDEX: IndexId: {}", indexId);
			redisSummaryInforRepo.summary(InforType.INDEX, indexId);
			
			algoSubscribeMessageProcessor.process(sigletonMap);					
		});
	
	}

	private void initTransLogPublishObject() {
		
	}

	private void initIndexInfor() {
		Stream.of(IndexInfor.ALL).forEach((idx) -> {
			String indexId = idx;
			stockBoardDataAccess.getPriorIndex(indexId, (e) -> {
				log.info("initIndexInfor: {}, getPriorIndex {}", indexId, e);
				
				IndexInfor current = getIndexInfor(indexId);
				current.setMarketId(MarketInfor.HOSE);
				IndexInfor newInfor = current.clone();
				
				BigDecimal indexValue = e;
				
				newInfor.setIndexValue(indexValue);
				newInfor.setPriorIndex(indexValue);
				setIndexInfor(indexId, newInfor);
			});
		});
		Stream.of(indexListStr.split(",")).forEach((hoseIdx) -> {
			String indexId = IndexInfor.getIndexId(hoseIdx);
			stockBoardDataAccess.getPriorIndex(indexId, (e) -> {
				log.info("initIndexInfor: {}, getPriorIndex {}", indexId, e);
				
				IndexInfor current = getIndexInfor(indexId);
				current.setMarketId(MarketInfor.HOSE);
				IndexInfor newInfor = current.clone();
				
				BigDecimal indexValue = e;
				
				newInfor.setIndexValue(indexValue);
				newInfor.setPriorIndex(indexValue);
				setIndexInfor(indexId, newInfor);
			});
		});
		
	}

	private void initMarketInfor() {
		for (String marketId : MarketInfor.ALL) {
			MarketInfor infor = MarketInfor.of(marketId);
			infor.setMarketFlag(HoseSystemControlCode.END_EOD_UPDATE);
			setMarketInfor(marketId, infor);
		}
	}

	private void initSecurityInfor() {
		redisRefInforRepository.getAllRefInfor().forEach((refInfor) -> {
			String symbol = refInfor.getSymbol();
			int ceiling = new BigDecimal(refInfor.getCeiling()).multiply(BigDecimalConstant.THOUSAND).intValue();
			int floor = new BigDecimal(refInfor.getFloor()).multiply(BigDecimalConstant.THOUSAND).intValue();
			int basic = new BigDecimal(refInfor.getBasic()).multiply(BigDecimalConstant.THOUSAND).intValue();

			SecurityInfor securityInfor = SecurityInfor.of(symbol);
			StaticInfor staticInfor = securityInfor.getStaticInfor();
			staticInfor.setBasicPrice(basic);
			staticInfor.setCeilingPrice(ceiling);
			staticInfor.setFloorPrice(floor);
			
			setSecurityInfor(symbol, securityInfor);
			
			log.info("initSecurityInfor:: redisRefInforRepository:: symbol: {} staticInfor: {}", symbol, staticInfor);
		});
		
		masterJpaRepository.getAllSecurityJpaInfor().forEach((input) -> {
			String symbol = input.getSecuritySymbol().trim();
			SecurityInfor securityInfor = SecurityInfor.of(symbol);
			StaticInfor staticInfor = securityInfor.getStaticInfor();
			
			int ceilingPrice = input.getCeilingPrice() * TEN;
			int floorPrice = input.getFloorPrice() * TEN;
			int basicPrice = input.getLastSalePrice() * TEN;
			String benefit = String.valueOf(input.getBenefit()).trim();
			if (benefit != null && !benefit.isEmpty() && "ADR".contains(benefit)) {
				StaticInfor algoStaticInfor = getAlgoStaticInfor(symbol);
				if (algoStaticInfor != null && algoStaticInfor.getBasicPrice() > 0)
					basicPrice = algoStaticInfor.getBasicPrice();
				else {
					basicPrice = calculateBasiPrice(TickSize.HOSE, ceilingPrice, floorPrice);
					log.info("algoStaticInfor is NULL! symbol: {} basicPrice: {}", symbol, basicPrice);
				}
			}
			
			staticInfor.setBasicPrice(basicPrice);
			staticInfor.setCeilingPrice(ceilingPrice);
			staticInfor.setFloorPrice(floorPrice);			
			
			setSecurityInfor(symbol, securityInfor);
			
			log.info("initSecurityInfor:: masterJpaRepository:: symbol: {} staticInfor: {}", symbol, staticInfor);
		});
	}


	private String getString(FieldMap message, int field) {
		try {
			String value = message.getString(field);
			if (value != null)
				return value;
			else
				return "";
		} catch (FieldNotFound e) {
			log.debug("getString, default value! field: {}", field);
			return "";
		}
	}
	
	private BigDecimal getDecimal(FieldMap message, int field) {
		try {
			String value = message.getString(field);
			if (value != null)
				return new BigDecimal(value);
			else {
				log.debug("getDecimal1, default value! field: {}", field);
				return BigDecimal.ZERO;
			}
		} catch (FieldNotFound | NumberFormatException e) {
			log.debug("getDecimal2, default value! field: {}", field);
			return BigDecimal.ZERO;
		} catch (Exception e) {
			log.debug("getDecimal3, default value! field: {}", field);
			return BigDecimal.ZERO;
		}
	}
	
	private int getInt(FieldMap message, int field) {
		try {
			int value = message.getInt(field);
			return value;
		} catch (FieldNotFound e) {
			log.debug("getInt, default value! field: {}", field);
			return 0;
		}
	}
	
	private long getLong(FieldMap message, int field) {
		try {
			long value = message.getInt(field);
			return value;
		} catch (FieldNotFound e) {
			log.warn("getLong, default value! field: {}", field);
			return 0;
		}
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		initRepo();
	}

}
