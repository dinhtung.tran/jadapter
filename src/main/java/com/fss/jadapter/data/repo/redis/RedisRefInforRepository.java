package com.fss.jadapter.data.repo.redis;

import java.util.Optional;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fss.jadapter.data.model.worker.RefInfor;
import com.fss.jadapter.data.persistence.model.v2.security.SecurityInfor;
import com.fss.jedis.dao.RedisConnectionManager;
import com.fss.jedis.model.adapter.RedisMarketInfor;
import com.fss.jedis.model.adapter.RedisRefInfor;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class RedisRefInforRepository {
		
	@Autowired
	private RedisConnectionManager redisConnectionManager;
	
	public Stream<RefInfor> getAllRefInfor() {
		Stream.Builder<RefInfor> builder = Stream.builder();
		Optional.of(redisConnectionManager.doTask((conn) -> conn.keys(RedisRefInfor.PREFIX + "*"))).ifPresent((set) -> {
			set.forEach((key) -> {
				try {
					String value = redisConnectionManager.doTask((conn) -> conn.get(key));
					RefInfor refInfor = parse(value);
					builder.accept(refInfor);
					log.info("getAllRefInfor:: builder accept: {}", refInfor);
				} catch (Exception e) {
					log.error("", e);
				}
			});
		});
		return builder.build();
	}

	private RefInfor parse(String value) {
		String[] arr = value.split(";");
		int i = 0;
		return RefInfor.builder()
				.symbol(arr[0])
				.ceiling(arr[1])
				.floor(arr[2])
				.basic(arr[3])
//				.fullName(arr[i++])
//				.benefit(arr[i++])
//				.meeting(arr[i++])
//				.split(arr[i++])
//				.isMargin(arr[i++])
				.build();
	}
}
