package com.fss.jadapter.data.repo.cache;

import java.time.format.DateTimeFormatter;
import java.util.List;

import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class HosePrsFileRepository {
	
	public static final String DATA_PATH_DATE_PATTERN = "dd/MM/yyyy";
	public static final DateTimeFormatter DATA_PATH_DATE_FORMATTER = DateTimeFormatter.ofPattern(DATA_PATH_DATE_PATTERN);
	
	private String dataPathDate;
	private String backupName;
	private List<String> indexList;
}
