package com.fss.jadapter.data.repo.file;

import org.springframework.stereotype.Component;

@Component
public class SecurityFileRepository extends AbstractFileRepository {
	
	@Override
	protected String getPath() {
		return "/security";
	}
}
