package com.fss.jadapter.data.repo.file;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public interface FileRepository {
	public void writeData(String key, byte[] data);
	public void readData(String key, Consumer<byte[]> consumer);
	public void readAddData(BiConsumer<String, byte[]> biConsumer);
}
