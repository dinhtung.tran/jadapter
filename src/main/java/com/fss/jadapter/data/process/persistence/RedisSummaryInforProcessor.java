package com.fss.jadapter.data.process.persistence;

import java.util.Map;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fss.jadapter.data.model.ChangedValues;
import com.fss.jadapter.data.repo.cache.RedisSummaryInforRepo;

@Component
public class RedisSummaryInforProcessor implements Processor {
	
	@Autowired
	private RedisSummaryInforRepo redisSummaryInforRepo;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		Map<String, ChangedValues> map = exchange.getMessage().getBody(Map.class);
		if (map != null && !map.isEmpty())
			map.keySet().forEach(redisSummaryInforRepo::addRealTimeChange);
	}

}
