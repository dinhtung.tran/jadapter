package com.fss.jadapter.data.process.system.counter;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.fss.jadapter.data.model.hose.udp.HoseUdpBroadcastMessage;
import com.fss.jadapter.data.model.hose.udp.HoseUdpBroadcastPackage;
import com.fss.jadapter.data.model.hose.udp.message.TS;

import lombok.extern.slf4j.Slf4j;
import quickfix.FieldNotFound;
import quickfix.Message;
import quickfix.field.MsgType;

@Component
@Slf4j
public class CounterProcessor implements Processor, Counter, InitializingBean {
	
	private static final String HOSE_PRS_UDP = "HOSE_PRS_UDP";
	private static final String HNX_INFOGATE = "HNX_INFOGATE";
	
	private static Map<String, Map<String, Integer>> countMap = new HashMap<>();
	
	private static boolean hasChange = true;
	private static String countResultCache = "";
	
    public void process(Exchange exchange) throws Exception {
    	try {
			Object body = exchange.getMessage().getBody();
			if (body instanceof HoseUdpBroadcastPackage) {
				HoseUdpBroadcastPackage hoseUdpBroadcastPackage = (HoseUdpBroadcastPackage) body;
				hoseUdpBroadcastPackage.getPackedContent().forEach((m) -> count(HOSE_PRS_UDP, m.getMessageType()));
			} else if (body instanceof Message) {
				Message message = (Message) body;
				String msgType = message.getHeader().getString(MsgType.FIELD);
				count(HNX_INFOGATE, msgType);
			}
			hasChange = true;
		} catch (Exception e) {
			log.error("exchange: " + exchange, e);
		}
    }


	private void count(String key, String msgType) {
		if (!countMap.containsKey(key)) {
			countMap.put(key, new HashMap<>());
		}
		
		Map<String, Integer> subMap = countMap.get(key);
		if (subMap.containsKey(msgType)) {
			subMap.put(msgType, subMap.get(msgType) + 1);
		} else {
			subMap.put(msgType, 1);
		}
	}


	@Override
	public String countResult() {
		if (hasChange == true) {
			countResultCache = countMap.toString();
			hasChange = false;
		} else {
			
		}
		return countResultCache;
	}


	@Override
	public void afterPropertiesSet() throws Exception {
		log.info("countResult(): {}", countResult());
	}
    
}
