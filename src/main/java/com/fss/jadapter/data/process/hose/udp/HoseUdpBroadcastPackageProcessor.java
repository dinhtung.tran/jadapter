package com.fss.jadapter.data.process.hose.udp;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import com.fss.jadapter.data.model.hose.udp.HoseUdpBroadcastMessage;
import com.fss.jadapter.data.model.hose.udp.HoseUdpBroadcastPackage;

@Component
public class HoseUdpBroadcastPackageProcessor implements Processor {
	
	@Override
	public void process(Exchange exchange) throws Exception {
		HoseUdpBroadcastPackage pck = exchange.getMessage().getBody(HoseUdpBroadcastPackage.class);
		for (HoseUdpBroadcastMessage message : pck.getPackedContent()) {
			message.getMessageType();
		}
	}
	
}
