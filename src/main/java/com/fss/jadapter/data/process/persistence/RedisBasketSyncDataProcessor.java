package com.fss.jadapter.data.process.persistence;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fss.jadapter.data.repo.cache.CachedMemoryRepository;
import com.fss.jedis.dao.RedisConnectionManager;
import com.fss.jedis.dao.RedisDataAccess;
import com.fss.jedis.dao.RedisPipelineTask;
import com.fss.jedis.model.adapter.RedisBasketInfor;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j(topic = "RedisPersistenceProcessor")
public class RedisBasketSyncDataProcessor implements Processor {
	
	@Autowired
	private CachedMemoryRepository cachedMemoryRepository;
	@Autowired
	private RedisConnectionManager redisConnectionManager;
	@Autowired
	private RedisDataAccess redisDataAccess;
		
	@Override
	public void process(Exchange exchange) throws Exception {
		log.info("RedisBasketSyncDataProcessor start!");
		Stream.Builder<RedisPipelineTask> pipeLineTaskBuilder = Stream.builder();
		redisDataAccess.getKeys(RedisBasketInfor.PREFIX + "*").forEach((basketKey) -> {
			String basketId = basketKey.substring(RedisBasketInfor.PREFIX.length());
			log.info("RedisBasketSyncDataProcessor: process basketId: {}", basketId);
			Set<String> cachedMemSet = cachedMemoryRepository.getBasketSymbol(basketId).collect(Collectors.toSet());
			Stream.Builder<String> deletedMemberBuilder = Stream.builder(); 
			redisDataAccess.getSet(basketKey).forEach((symbol) -> {
				if (!cachedMemSet.contains(symbol)) {
					deletedMemberBuilder.accept(symbol);
					log.info("RedisBasketSyncDataProcessor: delete: basketId: {} symbol: {}", basketId, symbol);
				} else {
				}
			});
			pipeLineTaskBuilder.accept((conn) ->
				conn.srem(basketKey, deletedMemberBuilder.build().collect(Collectors.toSet()).toArray(new String[0]))
				);
		});;
		
		redisDataAccess.pipeline(pipeLineTaskBuilder.build());
		
		log.info("RedisBasketSyncDataProcessor done!");
	}

}
