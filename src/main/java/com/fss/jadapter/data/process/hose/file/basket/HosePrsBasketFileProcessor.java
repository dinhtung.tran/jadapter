package com.fss.jadapter.data.process.hose.file.basket;

import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fss.jadapter.data.model.hose.file.HosePrsFilePackage;
import com.fss.jadapter.data.model.hose.file.record.BasketFileRecord;
import com.fss.jadapter.data.persistence.model.v2.market.BasketInfor;
import com.fss.jadapter.data.repo.cache.CachedMemoryRepository;
import com.fss.jadapter.data.repo.cache.RedisSummaryInforRepo;
import com.fss.jadapter.data.repo.cache.RedisSummaryInforRepo.InforType;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class HosePrsBasketFileProcessor implements Processor {

	@Autowired
	private CachedMemoryRepository cachedMemoryRepository;
	@Autowired
	private RedisSummaryInforRepo redisSummaryInforRepo;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		Map<String, HosePrsFilePackage<BasketFileRecord>> wrapper = exchange.getMessage().getBody(Map.class);
		wrapper.forEach((k, v) -> {
			v.getRecords().forEach((b) -> {
				cachedMemoryRepository.processInfor(k, b);
			});
			redisSummaryInforRepo.summary(InforType.BASKET, BasketInfor.getBasketId(k));
		});
	}

}
