package com.fss.jadapter.data.process.system.status;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class SystemStatusProcessor implements Processor, InitializingBean {

	@Override
	public void process(Exchange exchange) throws Exception {
		
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		
	}

}
