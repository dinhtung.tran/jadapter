package com.fss.jadapter.data.process.persistence.hnx.infogate;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fss.jadapter.data.model.ChangedValues;
import com.fss.jadapter.data.repo.cache.CachedMemoryRepository;

import lombok.extern.slf4j.Slf4j;
import quickfix.Message;
import quickfix.fixig.BasketInfo;
import quickfix.fixig.BoardInfo;
import quickfix.fixig.DerivativesInfo;
import quickfix.fixig.IndexInfo;
import quickfix.fixig.StockInfo;
import quickfix.fixig.TopNPrice;

@Component
@Slf4j
public class HnxInfogateCachedMemoryProcessor implements Processor {

	@Autowired
	private CachedMemoryRepository cachedMemoryRepository;

	@Override
	public void process(Exchange exchange) throws Exception {
//		HnxInfogateMessage message = exchange.getMessage().getBody(HnxInfogateMessage.class);
		Message message = exchange.getMessage().getBody(Message.class);
		
		Map<String, ChangedValues> cvm = new HashMap<>();

		Stream<ChangedValues> stream = null;
		
//		if (message instanceof BoardInfoIG) {
//			stream = cachedMemoryRepository.processInfor((BoardInfoIG) message);
//		} else if (message instanceof StockInfoIG) {
//			stream = cachedMemoryRepository.processInfor((StockInfoIG) message);
//		} else if (message instanceof TopPriceInfoIG) {
//			stream = cachedMemoryRepository.processInfor((TopPriceInfoIG) message);
//		} else if (message instanceof BasketInfoIG) {
//			stream = cachedMemoryRepository.processInfor((BasketInfoIG) message);
//		} else if (message instanceof IndexInfoIG) {
//			stream = cachedMemoryRepository.processInfor((IndexInfoIG) message);
//		} else if (message instanceof DerivativeInfoIG) {
//			stream = cachedMemoryRepository.processInfor((DerivativeInfoIG) message);
//		} else {
//			return;
//		}
		
		if (message instanceof BoardInfo) {
			stream = cachedMemoryRepository.processInfor((BoardInfo) message);
		} else if (message instanceof StockInfo) {
			stream = cachedMemoryRepository.processInfor((StockInfo) message);
		} else if (message instanceof TopNPrice) {
			stream = cachedMemoryRepository.processInfor((TopNPrice) message);
		} else if (message instanceof DerivativesInfo) {
			stream = cachedMemoryRepository.processInfor((DerivativesInfo) message);
		} else if (message instanceof BasketInfo) {
			stream = cachedMemoryRepository.processInfor((BasketInfo) message);
		}  else if (message instanceof IndexInfo) {
			stream = cachedMemoryRepository.processInfor((IndexInfo) message);
		} else {
			return;
		}
			

		if (stream != null) {
			stream.forEach((obj) -> {
				if (obj != null) {
					String key = buildKey(obj);
					if (!cvm.containsKey(key)) {
						cvm.put(key, obj);
					} else {
						ChangedValues cv = cvm.get(key);
						obj.forEach(cv::put);
					}
				}
			});
		}

		log.info("Res cvm: {}", cvm);
		exchange.getMessage().setBody(cvm);
	}

	private String buildKey(ChangedValues cv) {
		StringBuffer buffer = new StringBuffer(32);
		buffer.append(cv.getKey()).append('_').append(cv.getType());
		return buffer.toString();
	}
}
