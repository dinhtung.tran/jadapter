package com.fss.jadapter.data.process.system;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Stream;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fss.jadapter.data.model.ChangedValues;
import com.fss.jadapter.data.model.system.InitDaySignal;
import com.fss.jadapter.data.model.system.SystemSignal;
import com.fss.jadapter.data.repo.cache.CachedMemoryRepository;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SystemSignalProcessor implements Processor {
	
	private static final BlockingQueue<SystemSignal> SIGNAL_QUEUE = new LinkedBlockingQueue<>();
	
	@Autowired
	private CachedMemoryRepository cachedMemoryRepository;

	@Override
	public void process(Exchange exchange) throws Exception {
		try {
			Map<String, ChangedValues> cvm = new HashMap<>();
			Stream<ChangedValues> stream = null;
			while(!SIGNAL_QUEUE.isEmpty()) {
				SystemSignal signal = SIGNAL_QUEUE.take();
				if (signal instanceof InitDaySignal) {
					stream = processSignal((InitDaySignal) signal);
				} else {
					
				}
			}
			
			if (stream != null) {
				stream.forEach((obj) -> {
					if (obj != null && !obj.isEmpty()) {
						String key = buildKey(obj);
						if (!cvm.containsKey(key)) {
							cvm.put(key, obj);
						} else {
							ChangedValues cv = cvm.get(key);
							obj.forEach(cv::put);
						}
					}
				});
			}
			if (!cvm.isEmpty()) {
				log.info("Res cvm: {}", cvm);
			}
			exchange.getMessage().setBody(cvm);
		} catch (Exception e) {
			log.error("", e);
		}
	}
	
	private Stream<ChangedValues> processSignal(InitDaySignal signal) {
		log.info("processSignal InitDaySignal:: {}", signal);
		return cachedMemoryRepository.initDay();
	}

	public void enqueueSystemSignal(SystemSignal systemSignal) {
		SIGNAL_QUEUE.add(systemSignal);
	}
	
	private String buildKey(ChangedValues cv) {
		StringBuffer buffer = new StringBuffer(32);
		buffer.append(cv.getKey()).append('_').append(cv.getType());
		return buffer.toString();
	}
	
}
