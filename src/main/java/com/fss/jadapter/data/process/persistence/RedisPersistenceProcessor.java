package com.fss.jadapter.data.process.persistence;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fss.jadapter.data.model.worker.RefInfor;
import com.fss.jadapter.data.process.persistence.minor.AlgoSubscribeMessageProcessor;
import com.fss.jadapter.data.repo.cache.CachedMemoryRepository;
import com.fss.jadapter.data.repo.cache.RedisSummaryInforRepo;
import com.fss.jadapter.data.repo.cache.RedisSummaryInforRepo.InforType;
import com.fss.jedis.dao.RedisDataAccess;
import com.fss.jedis.dao.RedisPipelineTask;
import com.fss.jedis.model.adapter.RedisBasketInfor;
import com.fss.jedis.model.adapter.RedisMarketInfor;
import com.fss.jedis.model.adapter.RedisRefInfor;
import com.fss.jedis.model.adapter.RedisSecurityInfor;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j(topic = "RedisPersistenceProcessor")
public class RedisPersistenceProcessor implements Processor {
	
	@Autowired
	private RedisSummaryInforRepo redisPersistenceRepo;
	@Autowired
	private CachedMemoryRepository cachedMemoryRepository;
	@Autowired
	private AlgoSubscribeMessageProcessor algoSubscribeMessageProcessor;
	@Autowired
	private RedisDataAccess redisDataAccess;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		try {
			List<RedisPipelineTask> listRedisObject = new ArrayList<>();
			
			fillSecurityInfor(listRedisObject, InforType.SECURITY.pollAll());
			fillIndexInfor(listRedisObject, InforType.INDEX.pollAll());
			fillBasketInfor(listRedisObject, InforType.BASKET.pollAll());
			fillRefInfor(listRedisObject, InforType.REF.pollAll());
			
			redisDataAccess.pipeline(listRedisObject.stream());
		} catch (Exception e) {
			log.error("", e);
		}
	}
	
	private void fillRefInfor(List<RedisPipelineTask> listRedisObject, Stream<String> inforStream) {
		inforStream
		.forEach((symbol) -> {
			log.info("fillRefInfor:: symbol: {}", symbol);
			RedisRefInfor refInfo = RedisRefInfor.of(symbol);
			listRedisObject.add(refInfo);
			RefInfor refInfor = cachedMemoryRepository.getRefInfor(symbol);
			String ref = refInfor == null ? null : refInfor.getTransformedSnapshotMessage();
			refInfo.setValue(ref);
			log.debug("fillRefInfor:: symbol: {} ref: {}", symbol, ref);
			});
	}
	
	private void fillBasketInfor(List<RedisPipelineTask> listRedisObject, Stream<String> inforStream) {
			inforStream
			.forEach((indexId) -> {
				try {
					log.info("fillBasketInfor:: indexId: {}", indexId);
					RedisBasketInfor obj = RedisBasketInfor.of(indexId);
					listRedisObject.add(obj);
					obj.setValue(cachedMemoryRepository.getBasketSymbol(indexId)
							.peek((symbol) -> log.info("fillBasketInfor:: indexId: {} symbol: {}", indexId, symbol))
							.collect(Collectors.toSet()));
				} catch (Exception e) {
					log.error("fillBasketInfor: indexId: " + indexId, e);
				}
			});
		
	}

	private void fillIndexInfor(List<RedisPipelineTask> listRedisObject, Stream<String> inforStream) {
		inforStream
		.forEach((indexId) -> {
			try {
				log.info("fillIndexInfor:: indexId: {}", indexId);
				RedisMarketInfor obj = RedisMarketInfor.of(indexId);
				listRedisObject.add(obj);
				cachedMemoryRepository.getPublishMarketInfor(indexId).forEach((cv) -> {
					algoSubscribeMessageProcessor.processChangedValuesCollectionForSnapshot(Collections.singletonList(cv))
					.peek((p) -> log.info("fillIndexInfor:: indexId: {} msg: {}", indexId, p.getMsg()))
					.forEach((p) -> obj.setValue(p.getMsg()));
				});
			} catch (Exception e) {
				log.error("fillIndexInfor: indexId: " + indexId, e);
			}
		});		
	}

	private void fillSecurityInfor(List<RedisPipelineTask> listRedisObject, Stream<String> inforStream) {
		inforStream
		.forEach((symbol) -> {
			try {
				log.debug("fillSecurityInfor:: symbol: {}", symbol);
				RedisSecurityInfor obj = RedisSecurityInfor.of(symbol);
				RedisRefInfor refInfo = RedisRefInfor.of(symbol);
				listRedisObject.add(obj);
				listRedisObject.add(refInfo);
				cachedMemoryRepository.getPublishStockInfor(symbol).forEach((cv) -> {
					algoSubscribeMessageProcessor.processChangedValuesCollectionForSnapshot(Collections.singletonList(cv))
					.forEach((p) -> {
						log.debug("fillSecurityInfor:: msg: {} ref: {}", p.getMsg(), p.getRef());
						obj.setValue(p.getMsg());
						refInfo.setValue(p.getRef());
						});
				});
			} catch (Exception e) {
				log.error("fillSecurityInfor: symbol: " + symbol, e);
			}
		});		
	}

}
