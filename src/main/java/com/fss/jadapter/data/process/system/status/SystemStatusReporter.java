package com.fss.jadapter.data.process.system.status;

public interface SystemStatusReporter {
	public String getReport();
}
