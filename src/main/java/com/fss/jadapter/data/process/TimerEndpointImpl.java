package com.fss.jadapter.data.process;

import lombok.Data;

@Data
public class TimerEndpointImpl implements TimerEndpoint {
		
	private String name = this.getClass().getSimpleName();
	private long periodInMilis = 3000;
	private boolean daemon = false;	
	
	@Override
	public String getTimerEndpoint() {
		StringBuilder sb = new StringBuilder();
		sb.append("")
		.append("timer:")
		.append(name)
		.append('?')
		.append("period=").append(periodInMilis)
		.append('&')
		.append("daemon=").append(daemon)
		;
		return sb.toString();
	}
}
