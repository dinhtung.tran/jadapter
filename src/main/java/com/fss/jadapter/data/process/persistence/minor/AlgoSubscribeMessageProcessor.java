package com.fss.jadapter.data.process.persistence.minor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fss.jadapter.constant.PublishEntityType;
import com.fss.jadapter.data.model.ChangedValues;
import com.fss.jadapter.data.model.worker.DataVersionMessage;
import com.fss.jadapter.data.model.worker.IndexMessage;
import com.fss.jadapter.data.model.worker.RefInfor;
import com.fss.jadapter.data.model.worker.SecurityMessage;
import com.fss.jadapter.data.persistence.model.v2.market.MarketInfor;
import com.fss.jadapter.data.repo.cache.CachedMemoryRepository;
import com.fss.jadapter.model.PublishObject;
import com.fss.jadapter.publisher.DataPublisher;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AlgoSubscribeMessageProcessor implements Processor {
	
	private static final String SERVER_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";
	private static final DateFormat SERVER_TIME_FORMATTER = new SimpleDateFormat(SERVER_TIME_PATTERN);
		
	private static volatile int version = 0;
	
	private static final ObjectMapper JSON_MAPPER = new ObjectMapper();
	
	@Autowired
	@Qualifier("zeroMQPublisher")
	private DataPublisher zeroMQPublisher;
	@Autowired
	private CachedMemoryRepository cachedMemoryRepository;
	

	@Override
	public void process(Exchange exchange) throws Exception {
		Map<String, ChangedValues> map = exchange.getMessage().getBody(Map.class);
		process(map);
	}
	
	public void process(Map<String, ChangedValues> map) {
		if (map != null && !map.isEmpty()) {
			Collection<ChangedValues> coll = map.values();
			processChangedValuesCollection(coll).forEach(this::publish);
		}
	}

	public Stream<PublishObject> processChangedValuesCollection(Collection<ChangedValues> coll) {
			if (coll.size() == 0)
				return Stream.of();
			Stream.Builder<PublishObject> builder = Stream.builder();
			String serverTime = getServerTime();
			int version = getNextVersion();
			String marketStatus = getMarketStatus();
			coll.forEach((v) -> {
				if (v.isEmpty())
					return;
				DataVersionMessage dataVersionMessage = DataVersionMessage.of(serverTime, marketStatus, version);
				List<IndexMessage> indexMessageList = dataVersionMessage.getIndexMessageList();
				List<SecurityMessage> securityMessageList = dataVersionMessage.getSecurityMessageList();
				
				String type = v.getType();
				String key = v.getKey();
				
				if (ChangedValues.TYPE_SECURITY.equals(type) || ChangedValues.TYPE_TRANS.equals(type)) {
					securityMessageList.add(SecurityMessage.of(key, v));
				} else if (ChangedValues.TYPE_INDEX.equals(type)) {
					String status = cachedMemoryRepository.getIndexFlag(key);
					IndexMessage m = IndexMessage.of(key, status, v);
					indexMessageList.add(m);
				} else {
					log.info("Unsupported changedValues type: {}", type);
					return;
				}
				String outData = dataVersionMessage.getTransformedMessage();
				
				String url = buildUrl(type, key);
	//			log.info("AlgoSubMessage: {}", outData);
				PublishObject publishObject = wrapMessage(url, outData, "");
				builder.add(publishObject);
			});
			return builder.build();
		}

	public Stream<PublishObject> processChangedValuesCollectionForSnapshot(Collection<ChangedValues> coll) {
		try {
			if (coll.size() == 0)
				return Stream.empty();
			Stream.Builder<PublishObject> builder = Stream.builder();
			String serverTime = getServerTime();
			int version = getNextVersion();
			String marketStatus = getMarketStatus();
			coll.forEach((v) -> {
				DataVersionMessage dataVersionMessage = DataVersionMessage.of(serverTime, marketStatus, version);
				List<IndexMessage> indexMessageList = dataVersionMessage.getIndexMessageList();
				List<SecurityMessage> securityMessageList = dataVersionMessage.getSecurityMessageList();
				
				String type = v.getType();
				String key = v.getKey();
				
				if (ChangedValues.TYPE_SECURITY.equals(type) || ChangedValues.TYPE_TRANS.equals(type)) {
					securityMessageList.add(SecurityMessage.of(key, v));
				} else if (ChangedValues.TYPE_INDEX.equals(type)) {
					String status = cachedMemoryRepository.getIndexFlag(key);
					IndexMessage m = IndexMessage.of(key, status, v);
					indexMessageList.add(m);
				} else {
					log.info("Unsupported changedValues type: {}", type);
					return;
				}
				
				RefInfor refInfor = cachedMemoryRepository.getRefInfor(key);
				String ref = refInfor == null ? null : refInfor.getTransformedSnapshotMessage();
				
				String outData = dataVersionMessage.getTransformedSnapshotMessage();
				String url = buildUrl(type, key);
//			log.info("AlgoSubMessage: {}", outData);
				PublishObject publishObject = wrapMessage(url, outData, ref);
				builder.add(publishObject);
			});
			return builder.build();
		} catch (Exception e) {
			log.error("processChangedValuesCollectionForSnapshot: coll: " + coll, e);
			return Stream.empty();
		}
	}
	
	public void publish(PublishObject publishObject) {
		String msg = "";
		try {
			msg = JSON_MAPPER.writeValueAsString(publishObject);
		} catch (JsonProcessingException e) {
			log.error("msg: {}", msg);
			log.error("", e);
		}
		log.info("AlgoSubMessage: {}", msg);
		if (msg == null || msg.length() == 0) {

		} else {
			zeroMQPublisher.send(msg);
		}
	}

	private String buildUrl(String type, String key) {
		switch (type) {
		case ChangedValues.TYPE_SECURITY:
			return PublishEntityType.getStockInforUrl(key);
		case ChangedValues.TYPE_INDEX:
			return PublishEntityType.getMarketInforUrl(key);
		case ChangedValues.TYPE_TRANS:
			return PublishEntityType.getTransLogUrl(key);
		case ChangedValues.TYPE_MARKET:
		default:
			return "undefined/key";
		}
	}

	private PublishObject wrapMessage(String url, String msg, String ref) {
//		PublishObject wrapObject = new PublishObject(url, msg, ref);
		PublishObject wrapObject = PublishObject.builder().url(url).msg(msg).ref(ref).build();
		return wrapObject;
	}

	private synchronized int getNextVersion() {
		return ++version;
	}

	private String getMarketStatus() {
		return cachedMemoryRepository.getMarketFlag(MarketInfor.HOSE) + "," + cachedMemoryRepository.getMarketFlag(MarketInfor.HNX);
	}

	private String getServerTime() {
		return SERVER_TIME_FORMATTER.format(new Date());
	}
}
