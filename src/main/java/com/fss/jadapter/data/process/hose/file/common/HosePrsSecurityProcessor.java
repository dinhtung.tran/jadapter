package com.fss.jadapter.data.process.hose.file.common;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fss.jadapter.data.model.hose.file.HosePrsFilePackage;
import com.fss.jadapter.data.model.hose.file.record.DataPathFileRecord;
import com.fss.jadapter.data.repo.cache.HosePrsFileRepository;

import lombok.extern.slf4j.Slf4j;
@Component
@Slf4j
public class HosePrsSecurityProcessor implements Processor {

	@Autowired
	private HosePrsFileRepository hosePrsFileRepository;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		HosePrsFilePackage<DataPathFileRecord> pck = exchange.getMessage().getBody(HosePrsFilePackage.class);
		if (pck != null && pck.getLast() != null) {
			log.info("Rcv DataPath, last: {}", pck.getLast());
			String currDate = pck.getLast().getDate();
			String priorDate = hosePrsFileRepository.getDataPathDate();
			if (priorDate == null || !priorDate.equals(currDate)) {
				hosePrsFileRepository.setDataPathDate(currDate);
			}
		} else {
//			log.error("Body is null!");
		}
	}



}
