package com.fss.jadapter.data.process.persistence;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fss.jadapter.data.model.hnx.infogate.HnxInfogateMessage;
import com.fss.jadapter.data.model.hose.udp.HoseUdpBroadcastPackage;

import lombok.extern.slf4j.Slf4j;
import quickfix.Message;

@Component
@Slf4j
public class CachedMemoryProcessor implements Processor {
	
	@Autowired
	private Processor hoseUdpCachedMemoryProcessor;
	@Autowired
	private Processor hnxInfogateCachedMemoryProcessor;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		Object body = exchange.getMessage().getBody();
		if (body instanceof HoseUdpBroadcastPackage) {
			hoseUdpCachedMemoryProcessor.process(exchange);
		} else
		if (body instanceof Message) {
			hnxInfogateCachedMemoryProcessor.process(exchange);
		} else if (body instanceof Message) {
			hnxInfogateCachedMemoryProcessor.process(exchange);
		} else {
			log.warn("unsupported message type: class: {}", body.getClass());
		}
	}
}
