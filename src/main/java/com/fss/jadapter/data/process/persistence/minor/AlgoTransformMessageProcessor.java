package com.fss.jadapter.data.process.persistence.minor;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fss.jadapter.data.model.ChangedValues;
import com.fss.jadapter.data.model.worker.DataVersionMessage;
import com.fss.jadapter.data.model.worker.IndexMessage;
import com.fss.jadapter.data.model.worker.SecurityMessage;
import com.fss.jadapter.data.persistence.model.v2.market.MarketInfor;
import com.fss.jadapter.data.repo.cache.CachedMemoryRepository;
import com.fss.jadapter.publisher.DataPublisher;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class AlgoTransformMessageProcessor implements Processor {
	
	private static final String SERVER_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";
	private static final DateFormat SERVER_TIME_FORMATTER = new SimpleDateFormat(SERVER_TIME_PATTERN);
	private static volatile int version = 0;
	
	private static final ObjectMapper JSON_MAPPER = new ObjectMapper();
	
	@Autowired
	@Qualifier("zeroMQPublisher")
	private DataPublisher zeroMQPublisher;
	@Autowired
	private CachedMemoryRepository cachedMemoryRepository;
	

	@Override
	public void process(Exchange exchange) throws Exception {
		Map<String, ChangedValues> map = exchange.getMessage().getBody(Map.class);
		if (map.size() == 0)
			return;
		DataVersionMessage dataVersionMessage = DataVersionMessage.of(getServerTime(), getMarketStatus(), getNextVersion());
		List<IndexMessage> indexMessageList = dataVersionMessage.getIndexMessageList();
		List<SecurityMessage> securityMessageList = dataVersionMessage.getSecurityMessageList();
		map.forEach((k, v) -> {
			String type = v.getType();
			String key = v.getKey();
			if (ChangedValues.TYPE_SECURITY.equals(type)) {
				securityMessageList.add(SecurityMessage.of(key, v));
			} else if (ChangedValues.TYPE_INDEX.equals(type)) {
				String status = cachedMemoryRepository.getIndexFlag(key);
				IndexMessage m = IndexMessage.of(key, status, v);
				indexMessageList.add(m);
			}  else {
				log.info("Unsupported changedValues type: {}", type);
				return;
			}
		});
		String outdata = dataVersionMessage.getTransformedMessage();
		log.info("AlgoMessage: {}", outdata);
		byte[] msg = wrapMessage(outdata);
		if (msg == null || msg.length == 0) {
			
		} else {
			zeroMQPublisher.send(msg);
		}
	}

	private byte[] wrapMessage(String msg) {
		WrapObject wrapObject = new WrapObject(msg);
		byte[] rs = null;
		try {
			rs = JSON_MAPPER.writeValueAsBytes(wrapObject);
		} catch (JsonProcessingException e) {
			log.error("msg: {}", msg);
			log.error("", e);
		}
		return rs;
	}

	private synchronized int getNextVersion() {
		return ++version;
	}

	private String getMarketStatus() {
		return cachedMemoryRepository.getMarketFlag(MarketInfor.HOSE);
	}

	private String getServerTime() {
		return SERVER_TIME_FORMATTER.format(new Date());
	}
	
	@RequiredArgsConstructor
	@Getter
	@NoArgsConstructor
	private static class WrapObject {
		private String url = "";
		@NonNull
		private String msg;
	}
}
