package com.fss.jadapter.data.process.hose.file.index;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fss.jadapter.data.model.ChangedValues;
import com.fss.jadapter.data.model.hose.file.HosePrsFilePackage;
import com.fss.jadapter.data.model.hose.file.record.IndexFileRecord;
import com.fss.jadapter.data.repo.cache.CachedMemoryRepository;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class HosePrsIndexFileProcessor implements Processor {
	
	private ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	private CachedMemoryRepository cachedMemoryRepository;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		try {
			Map<String, HosePrsFilePackage<IndexFileRecord>> wrapper = exchange.getMessage().getBody(Map.class);
			if (wrapper != null && !wrapper.isEmpty()) {
				Stream.Builder<ChangedValues> builder = Stream.builder();
				wrapper.forEach((k, v) -> {
					if (v != null && v.getRecords().isEmpty())
						return;
					log.info("Index {} IndexFileRecord: {}", k, v);
					v.forEach((r) -> {
						cachedMemoryRepository.processInfor(k, r).forEach(builder::add);
					});
				});
				
				Map<String, ChangedValues> cvm = new HashMap<>();
				Stream<ChangedValues> stream = builder.build();
				
				if (stream != null) {
					stream.forEach((obj) -> {
						if (obj != null && !obj.isEmpty()) {
							String key = buildKey(obj);
							if (!cvm.containsKey(key)) {
								cvm.put(key, obj);
							} else {
								ChangedValues cv = cvm.get(key);
								obj.forEach(cv::put);
							}
						}
					});
				}
				if (!cvm.isEmpty())
					log.info("Res cvm: {}", cvm);
				exchange.getMessage().setBody(cvm);
			} else {
			}
		} catch (Exception e) {
			log.error("Error", e);
			exchange.getMessage().setBody(Collections.emptyMap());
		}
	}

	private String buildKey(ChangedValues cv) {
		StringBuffer buffer = new StringBuffer(32);
		buffer.append(cv.getKey()).append('_').append(cv.getType());
		return buffer.toString();
	}
}
