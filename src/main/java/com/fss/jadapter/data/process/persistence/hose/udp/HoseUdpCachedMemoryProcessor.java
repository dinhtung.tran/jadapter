package com.fss.jadapter.data.process.persistence.hose.udp;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fss.jadapter.data.model.ChangedValues;
import com.fss.jadapter.data.model.hose.udp.HoseUdpBroadcastMessage;
import com.fss.jadapter.data.model.hose.udp.HoseUdpBroadcastPackage;
import com.fss.jadapter.data.model.hose.udp.message.DC;
import com.fss.jadapter.data.model.hose.udp.message.IU;
import com.fss.jadapter.data.model.hose.udp.message.LS;
import com.fss.jadapter.data.model.hose.udp.message.PD;
import com.fss.jadapter.data.model.hose.udp.message.PO;
import com.fss.jadapter.data.model.hose.udp.message.SC;
import com.fss.jadapter.data.model.hose.udp.message.SS;
import com.fss.jadapter.data.model.hose.udp.message.SU;
import com.fss.jadapter.data.model.hose.udp.message.TP;
import com.fss.jadapter.data.model.hose.udp.message.TR;
import com.fss.jadapter.data.repo.cache.CachedMemoryRepository;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class HoseUdpCachedMemoryProcessor implements Processor {

	@Autowired
	private CachedMemoryRepository cachedMemoryRepository;

	@Override
	public void process(Exchange exchange) throws Exception {
		HoseUdpBroadcastPackage pck = exchange.getMessage().getBody(HoseUdpBroadcastPackage.class);
		Map<String, ChangedValues> cvm = new HashMap<>();

		for (HoseUdpBroadcastMessage message : pck.getPackedContent()) {
			try {
				Stream<ChangedValues> stream = null;
				if (message instanceof SS) {
					stream = cachedMemoryRepository.processInfor((SS) message);
				} else if (message instanceof SU) {
					stream = cachedMemoryRepository.processInfor((SU) message);
				} else if (message instanceof TP) {
					stream = cachedMemoryRepository.processInfor((TP) message);
				} else if (message instanceof LS) {
					stream = cachedMemoryRepository.processInfor((LS) message);
				} else if (message instanceof SC) {
					stream = cachedMemoryRepository.processInfor((SC) message);
				} else if (message instanceof IU) {
					stream = cachedMemoryRepository.processInfor((IU) message);
				} else if (message instanceof TR) {
					stream = cachedMemoryRepository.processInfor((TR) message);
				} else if (message instanceof PO) {
					stream = cachedMemoryRepository.processInfor((PO) message);
				} else if (message instanceof PD) {
					stream = cachedMemoryRepository.processInfor((PD) message);
				} else if (message instanceof PD) {
					stream = cachedMemoryRepository.processInfor((DC) message);
				}

				if (stream != null) {
					stream.forEach((obj) -> {
						if (obj != null) {
							String key = buildKey(obj);
							if (!cvm.containsKey(key)) {
								cvm.put(key, obj);
							} else {
								ChangedValues cv = cvm.get(key);
								obj.forEach(cv::put);
							}
						}
					});
				}
			} catch (Exception e) {
				log.error("HoseUdpBroadcastMessage: {}", message);
				log.error("Error: ", e);
			}
		}
		log.info("Res cvm: {}", cvm);
		exchange.getMessage().setBody(cvm);
	}

	private String buildKey(ChangedValues cv) {
		StringBuffer buffer = new StringBuffer(32);
		buffer.append(cv.getKey()).append('_').append(cv.getType());
		return buffer.toString();
	}
}
