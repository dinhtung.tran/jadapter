package com.fss.jadapter.data.persistence.model.v2.security;

import java.math.BigDecimal;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor(staticName = "of")
@ToString(includeFieldNames = true)
public class AdvancedInfor {
	@ToString.Include
	private BigDecimal diffVn30 = BigDecimal.ZERO;
	
	@ToString.Include
	private BigDecimal basis = BigDecimal.ZERO;
	@ToString.Include
	private BigDecimal balancePrice = BigDecimal.ZERO;
	@ToString.Include
	private BigDecimal openInterest = BigDecimal.ZERO;
}
