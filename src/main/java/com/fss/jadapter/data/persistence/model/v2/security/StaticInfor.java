package com.fss.jadapter.data.persistence.model.v2.security;

import java.math.BigDecimal;

import com.fss.jadapter.data.persistence.model.v2.Information;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor(staticName = "of")
@ToString(includeFieldNames = true)
public class StaticInfor  implements Information {
	@ToString.Include
	private int ceilingPrice;
	@ToString.Include
	private int floorPrice;
	@ToString.Include
	private int basicPrice;
	@ToString.Include
	private SecurityType securityType;
	@ToString.Include
	private int exercisePrice;
	@ToString.Include
	private String exerciseRatio;
	@ToString.Include
	private String fullName;
	@ToString.Include
	private String benefit;
	@ToString.Include
	private String meeting;
	@ToString.Include
	private String split;
	@ToString.Include
	private String isMargin;
}