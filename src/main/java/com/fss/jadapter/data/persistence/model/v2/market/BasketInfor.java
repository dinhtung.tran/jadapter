package com.fss.jadapter.data.persistence.model.v2.market;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data(staticConstructor = "of")
public class BasketInfor {
	
	public static final String HOSE = IndexInfor.HOSE;
	public static final String HNX = IndexInfor.HNX;
	public static final String UPCOM = IndexInfor.UPCOM;
	public static final String HNX_30 = IndexInfor.HNX_30;
	
	public static final String getBasketId(String index) {
		return "VN" + index;
	}
	
	public static final String HOSE_CW = "HOSE_CW";
	public static final String HOSE_BOND = "HOSE_BOND";
	
	public static final String HNX_FDS_VN30 = "HNX_FDS_VN30";
	public static final String HNX_FDS_VGB5 = "HNX_FDS_VGB5";
	public static final String HNX_BOND = "HNX_BOND";
	
	private final String basketId;
	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	private Set<String> basketSymbol = new HashSet<>();
	
	public void add(String symbol) {
		synchronized (basketSymbol) {
			basketSymbol.add(symbol);
		}
	}
	
	public boolean remove(String symbol) {
		synchronized (basketSymbol) {
			return basketSymbol.remove(symbol);
		} 
	}
	
	public void clear() {
		synchronized (basketSymbol) {
			basketSymbol.clear();
		} 
	}
	
	public Stream<String> stream() {
		synchronized (basketSymbol) {
			return Stream.of(basketSymbol.toArray(new String[basketSymbol.size()]));
		}
	}
	
}
