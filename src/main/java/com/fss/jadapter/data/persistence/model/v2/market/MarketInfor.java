package com.fss.jadapter.data.persistence.model.v2.market;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.NonNull;

@Data(staticConstructor = "of")
public class MarketInfor {
	public static final String HOSE = "HOSE";
	public static final String HNX = "HNX";
	public static final String UPCOM = "UPCOM";
	public static final String FDS = "FDS";
	public static final String[] ALL = {HOSE, HNX, UPCOM, FDS};
	
	@NonNull
	private String marketId;
	private String marketName;
	private String marketFlag;
	private String priorMarketFlag;
	
	private List<String> indexList = new ArrayList<>();
}
