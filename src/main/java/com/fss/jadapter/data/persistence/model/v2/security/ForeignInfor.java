package com.fss.jadapter.data.persistence.model.v2.security;

import java.math.BigDecimal;

import lombok.Data;

@Data(staticConstructor = "of")
public class ForeignInfor {
	private long totalRoom;
	private long currentRoom;
	private long buyVolume;
	private long sellVolume;
	private BigDecimal remainPercent = BigDecimal.ZERO;

}
