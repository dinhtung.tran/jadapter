package com.fss.jadapter.data.persistence.model.v2.security;

import java.math.BigDecimal;
import java.math.RoundingMode;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import lombok.ToString;

@Data(staticConstructor = "of")
//@Setter(AccessLevel.NONE)
@ToString(includeFieldNames = true)
public class ProjectedOpen {
	
	public static final ProjectedOpen INACTIVE_PO = of(-1);
	public static final ProjectedOpen NULL_PO = of(0);
	
	static {
		NULL_PO.setChange(0);
		NULL_PO.setChangePercent("0");
	}

	
	// Neu price >= 0 == co gia tri
	private final int price;
	@Setter(AccessLevel.PRIVATE)
	private int change;
	@Setter(AccessLevel.PRIVATE)
	private String changePercent;
	
	public void calculateChange(int basicPrice) {
		if (price > 0) {
			setChange(price - basicPrice);
			BigDecimal diffRate = BigDecimal.ZERO;
			if (basicPrice != 0)
				diffRate = (new BigDecimal(change * 100)).divide(new BigDecimal(basicPrice), 2, RoundingMode.HALF_UP);
			setChangePercent(diffRate.toString());
		} else {
			setChange(0);
			setChangePercent("0");
		}
	}
	
}
