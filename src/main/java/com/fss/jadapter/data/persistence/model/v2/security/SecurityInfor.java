package com.fss.jadapter.data.persistence.model.v2.security;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.fss.jadapter.data.persistence.model.v2.Information;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data
@RequiredArgsConstructor(staticName = "of")
@Setter(AccessLevel.NONE)
@ToString(includeFieldNames = true)
public class SecurityInfor implements Information {
		
	@NonNull
	private String symbol;
	@ToString.Include
	private StaticInfor staticInfor = StaticInfor.of();
	@ToString.Include
	private DynamicInfor dynamicInfor = DynamicInfor.of();
	@Getter(AccessLevel.NONE)
	@ToString.Include
	private List<MatchedOrderInfor> matchedOrderHistory =  new ArrayList<>();
	
	private void processNewMatchedOrderInfor(MatchedOrderInfor matchedOrderInfor) {
		int basicPrice = staticInfor.getBasicPrice();
		int change = matchedOrderInfor.getPrice() - staticInfor.getBasicPrice();
		matchedOrderInfor.setChange(change);
		BigDecimal diffRate = BigDecimal.ZERO;
		if (basicPrice != 0)
			diffRate = (new BigDecimal(change * 100)).divide(new BigDecimal(basicPrice), 2, RoundingMode.HALF_UP);
		matchedOrderInfor.setChangePercent(diffRate.toString());
		
		if (matchedOrderHistory.isEmpty()) {
			matchedOrderInfor.setAccumulatedVolume(matchedOrderInfor.getVolume());
			matchedOrderInfor.setAccumulatedValue(((long) matchedOrderInfor.getVolume()) * matchedOrderInfor.getPrice());
		} else {
			MatchedOrderInfor preMatchedOrderInfor = getLastMatchedOrder();
			long preAccVol = preMatchedOrderInfor.getAccumulatedVolume();
			long preAccVal = preMatchedOrderInfor.getAccumulatedValue();
			
			long additionVol = matchedOrderInfor.getVolume();
			long additionVal = matchedOrderInfor.getPrice() * additionVol;
			
			matchedOrderInfor.setAccumulatedVolume(preAccVol + additionVol);
			matchedOrderInfor.setAccumulatedValue(preAccVal + additionVal);
		}
		
		matchedOrderHistory.add(matchedOrderInfor);
	}

	public MatchedOrderInfor getLastMatchedOrder() {
		if (matchedOrderHistory.isEmpty())
			return null;
		int lastIndext = matchedOrderHistory.size() - 1;
		return matchedOrderHistory.get(lastIndext);
	}

	public synchronized MatchedOrderInfor addMatchedOrderInfor(String symbol, int price, int volume, String time) {
		MatchedOrderInfor matchedOrderInfor = MatchedOrderInfor.of(matchedOrderHistory.size(), symbol);
		matchedOrderInfor.setPrice(price);
		matchedOrderInfor.setVolume(volume);
		matchedOrderInfor.setTime(time);
		
		processNewMatchedOrderInfor(matchedOrderInfor);
		getDynamicInfor().setProjectedOpen(ProjectedOpen.INACTIVE_PO);
		
		return matchedOrderInfor;
	}
	
	public Stream<MatchedOrderInfor> streamMatchedOrderInfor() {
		return Stream.of(matchedOrderHistory.toArray(new MatchedOrderInfor[matchedOrderHistory.size()]));
	}
		
//	public long getTotalValue() {
//		if (matchedOrderHistory.isEmpty())
//			return 0;
//		return getLastMatchedOrder().getAccumulatedValue();
//	}
//	
//	public long getTotalVolume() {
//		if (matchedOrderHistory.isEmpty())
//			return 0;
//		return getLastMatchedOrder().getAccumulatedVolume();
//	}

//	public void copyMatchedOrderInfor() {
//		
//	}

	public void copyMatchedOrderInfor(SecurityInfor otherInfor) {
		this.matchedOrderHistory.addAll(otherInfor.matchedOrderHistory);
//		this.matchedOrderHistory = otherInfor.matchedOrderHistory;
	}

	public void initDay() {
		DynamicInfor newDynamicInfor = DynamicInfor.of();
		List<MatchedOrderInfor> newMatchedOrderHistory =  new ArrayList<>();
		
		dynamicInfor = newDynamicInfor;
		matchedOrderHistory = newMatchedOrderHistory;
	}

}
