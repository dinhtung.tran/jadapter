package com.fss.jadapter.data.persistence.model.v2.market;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Stream;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;

@Data(staticConstructor = "of")
public class IndexInfor implements Cloneable {
	public static final String HOSE = "VNIndex";
	public static final String HNX = "HNXIndex";
	public static final String UPCOM = "HNXUpcomIndex";
	public static final String HNX_30 = "HNX30";
	public static final String[] ALL = {HOSE, HNX, UPCOM, HNX_30};
	
	public static final String getIndexId(String index) {
		return "VN" + index;
	}
	
	@NonNull
	private String indexId;
	private String marketId;
	private String indexName;
	
	private BigDecimal indexValue = BigDecimal.ZERO;
	private BigDecimal priorIndex = BigDecimal.ZERO;
	private BigDecimal change = BigDecimal.ZERO;
	private BigDecimal changeRate = BigDecimal.ZERO;
	
	private int advances;
	private int declines;
	private int noChange;
	
	private int numOfCe;
	private int numOfFl;
	private int noTrade;
	
	private long totalValue;
	private long totalVolume;
	private long totalTrade;

	private long totalValuePt;
	private long totalVolumePt;
	private long totalTradePt;
		
	public IndexInfor clone() {
		IndexInfor infor = null;
		try {
			infor = (IndexInfor) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return infor;
	}

	public void initDay() {
		change = BigDecimal.ZERO;
		changeRate = BigDecimal.ZERO;
		
		advances = 0;
		declines = 0;
		noChange = 0;
		
		numOfCe = 0;
		numOfFl = 0;
		noTrade = 0;
		
		totalValue = 0;
		totalVolume = 0;
		totalTrade = 0;

		totalValuePt = 0;
		totalVolumePt = 0;
		totalTradePt = 0;
	}
}
