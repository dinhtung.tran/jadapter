package com.fss.jadapter.data.persistence.model.v2.security;

import com.fss.jadapter.data.persistence.model.v2.Information;

public enum SecurityType {
	STOCK, BOND, CW, FDS, ETF,
	UNDEFINE
	;
	SecurityType() {
		
	}
}
