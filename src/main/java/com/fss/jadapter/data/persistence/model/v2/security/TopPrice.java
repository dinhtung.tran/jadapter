package com.fss.jadapter.data.persistence.model.v2.security;

import java.util.HashMap;
import java.util.Map;

import com.fss.jadapter.data.persistence.model.v2.Information;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Data(staticConstructor = "of")
@Setter(AccessLevel.NONE)
@ToString(includeFieldNames = true)
public class TopPrice  implements Information {
	
	public static final int ATC_PRICE = -1;
	public static final int ATO_PRICE = -2;
	public static final String SIDE_BUY = "B";
	public static final String SIDE_SELL = "S";
	
	@NonNull
	@ToString.Include
	private String side;
	@Getter(AccessLevel.NONE)
	@ToString.Include
	private Map<Integer, Entry> repo = new HashMap<>();
	
	public int getTopPrice(int top) {
		Entry p = null;
		if (repo.containsKey(Integer.valueOf(top))) {
			p = repo.get(top); 
		}
		if (p == null)
			return 0;
		return p.getPrice();
	}
	
	public int getTopVolume(int top) {
		Entry p = null;		
		if (repo.containsKey(Integer.valueOf(top))) {
			p = repo.get(top); 
		}
		if (p == null)
			return 0;
		return p.getVolume();
	}
	
	public void set(int top, int price, int volume) {
		Entry e = new Entry (top, price, volume);
		Integer t = Integer.valueOf(top);
		repo.put(t, e);
	}
	
	public void remove(int top) {
		repo.remove(Integer.valueOf(top));
	}
	
	public int size() {
		return repo.size();
	}
	
	@Getter
	@Setter
	@RequiredArgsConstructor
	private class Entry {
		@NonNull
		private int top;
		@NonNull
		private int price;
		@NonNull
		private int volume;
	}
}
