package com.fss.jadapter.data.persistence.model.v2.security;

import com.fss.jadapter.data.persistence.model.v2.Information;

import lombok.Data;
import lombok.NonNull;

@Data(staticConstructor = "of")
public class MatchedOrderInfor {
	@NonNull
	private int seqNum;
	@NonNull
	private String symbol;
	private String time;
	private int price;
	private long volume;
	private int change;
	private long accumulatedVolume;
	private long accumulatedValue;
	private String changePercent;
	private String side;

}
