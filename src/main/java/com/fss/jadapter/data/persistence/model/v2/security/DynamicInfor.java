package com.fss.jadapter.data.persistence.model.v2.security;

import com.fss.jadapter.data.persistence.model.v2.Information;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import lombok.ToString;

@Data(staticConstructor = "of")
//@Setter(AccessLevel.NONE)
@ToString(includeFieldNames = true)
public class DynamicInfor  implements Information{
	@ToString.Include
	private TopPrice topBuy = TopPrice.of(TopPrice.SIDE_BUY);
	@ToString.Include
	private TopPrice topSell = TopPrice.of(TopPrice.SIDE_SELL);
	@ToString.Include
	private ForeignInfor foreignInfor = ForeignInfor.of();
	@ToString.Include
	private AdvancedInfor advancedInfor = AdvancedInfor.of();
	@ToString.Include
	private ProjectedOpen projectedOpen = ProjectedOpen.INACTIVE_PO;
	@ToString.Include
	private int open;
	@ToString.Include
	private int highest;
	@ToString.Include
	private int lowest;
	@ToString.Include
	private int average;
	@ToString.Include
	private long totalVolume;
	@ToString.Include
	private long totalValue;
	@ToString.Include
	private long totalBuyQtty;
	@ToString.Include
	private long totalSellQtty;
	
}
