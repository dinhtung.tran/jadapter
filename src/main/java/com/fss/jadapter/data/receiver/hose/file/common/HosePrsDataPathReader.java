package com.fss.jadapter.data.receiver.hose.file.common;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fss.jadapter.data.model.hose.file.record.DataPathFileRecord;
import com.fss.jadapter.data.receiver.hose.file.AbstractHosePrsFileReader;

@Component
public class HosePrsDataPathReader extends AbstractHosePrsFileReader<DataPathFileRecord> {
	
	private static final String FILE_NAME = "DATAPATH.MAP";
	
	@Value("${hose.prs.file.common.data.dir}") 
	private String parentCommonFileDir;
	
	private Path path;

	@Override
	public Path getPath() {
		return path;
	}

	@Override
	public Class<DataPathFileRecord> getRecordType() {
		return DataPathFileRecord.class;
	}

	@Override
	protected void init() {
		path = Paths.get(parentCommonFileDir, FILE_NAME);
	}

}
