package com.fss.jadapter.data.receiver.hose.udp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fss.jadapter.camel.CamelManager;
import com.fss.jadapter.constant.definition.hose.udp.HoseUdpBroadcastPackageFactory;
import com.fss.jadapter.data.model.hose.udp.HoseUdpBroadcastPackage;
import com.fss.jadapter.data.receiver.AbstractConvertedDataReceiver;

@Component
public class HoseUdpBroadcastPackageReceiver extends AbstractConvertedDataReceiver<byte[], HoseUdpBroadcastPackage> {

	@Autowired
	private HoseUdpBroadcastPackageFactory factory;
	
	@Autowired
	private CamelManager camelManager;
	
	@Override
	public HoseUdpBroadcastPackage convert(byte[] data) {
		HoseUdpBroadcastPackage hoseMessagePackage = factory.product(data);
		return hoseMessagePackage;
	}

	@Override
	public Class<byte[]> getInClass() {
		return byte[].class;
	}

	@Override
	public Class<HoseUdpBroadcastPackage> getOutClass() {
		return HoseUdpBroadcastPackage.class;
	}

}
