package com.fss.jadapter.data.receiver.hose.file.basket;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fss.jadapter.constant.definition.hose.file.HosePrsFileDefinitionHolder;
import com.fss.jadapter.constant.definition.hose.file.HosePrsFilePackageFactory;
import com.fss.jadapter.constant.definition.hose.file.model.FileRecordStruct;
import com.fss.jadapter.data.model.hose.file.HosePrsFilePackage;
import com.fss.jadapter.data.model.hose.file.record.BasketFileRecord;
import com.fss.jadapter.data.receiver.hose.file.HosePrsFileReader;
import com.fss.jadapter.data.repo.cache.HosePrsFileRepository;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Component
public class HosePrsBasketFileReader implements Processor, HosePrsFileReader<BasketFileRecord>, InitializingBean{
	
	public static final String FILENAME_DATE_PATTERN = "yyyyMMdd";
	public static final DateFormat FILENAME_DATE_FORMATTER = new SimpleDateFormat(FILENAME_DATE_PATTERN);
	
	@Value("${hose.prs.file.basket.data.dir}") 
	private String parentIndexFileDir;
	@Value("${hose.prs.file.index.list}") 
	private String indexListStr;
	
	@Setter
	@Getter
	private Path path = null;
	@Setter
	@Getter
	private String indexName;
	
	@Autowired
	private HosePrsFilePackageFactory hosePrsFilePackageFactory;
	@Autowired
	private HosePrsFileDefinitionHolder hosePrsFileDefinitionHolder;
	
	private Map<String, SeekableByteChannel> byteChannelMap = new HashMap<>();
	private Map<String, Path> pathMap = new HashMap<>();

	private ByteBuffer buffer;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		try {
			log.info("HosePrsBasketFileReader processor!");
			Map<String, HosePrsFilePackage<BasketFileRecord>> pck = getPackages();
			exchange.getMessage().setBody(pck);
		} catch (Exception e) {
			log.error("HosePrsBasketFileReader  processor error!", e);		}
	}

	private Map<String, HosePrsFilePackage<BasketFileRecord>> getPackages() {
		Map<String, HosePrsFilePackage<BasketFileRecord>> map = new HashMap<>();
		for (String k : byteChannelMap.keySet()) {
			doTaskOnChannel(k, (c) -> map.put(k, getPackage(k, c)));
		}
		return map;
	}

	private HosePrsFilePackage<BasketFileRecord> getPackage(String index, SeekableByteChannel byteChannel) {
		Stream.Builder<byte[]> builder = Stream.builder();
		try {
			while ((byteChannel.size() - byteChannel.position()) >= bufferSize()) {
				buffer.clear();
				byteChannel.read(buffer);
				buffer.flip();
				byte[] data = new byte[buffer.limit()];
				buffer.get(data);
				builder.add(data);
			}
			byteChannel.position(0);
			return hosePrsFilePackageFactory.product(index, builder.build(), getRecordType());
		} catch (IOException e) {
			log.error("getPackage error! ", e);
			return null;
		}
		
	}
	
	private void doTaskOnChannel(String index, Consumer<SeekableByteChannel> consumer) {
		Path path = Paths.get(parentIndexFileDir, "CS" + "_VN" + index  + ".DAT");
		SeekableByteChannel byteChannel = null;
		try {
			if (path == null) {
				log.info("doTaskOnChannel fail! Path is null!");
			} else {
				byteChannel = FileChannel.open(path, StandardOpenOption.READ);
				if (byteChannel != null && byteChannel.isOpen()) {
					consumer.accept(byteChannel);
					byteChannel.close();
				}
				else
					log.error("doTaskOnChannel error!");
			}
		} catch (IOException e) {
			log.error("openChannel error! ", e);
		}
	}
	
	@Override
	public final void afterPropertiesSet() throws Exception {		
		log.info("thisClass: {} RecordType: {}, Path: {}", this.getClass(), getRecordType(), getPath());
		initBuffer();
		initPathMap();
		initChannel();
	}

	private void initChannel() {
		Stream.of(indexListStr.split(",")).forEach((s) -> {
			byteChannelMap.put(s, null);
		});;
	}

	private void initPathMap() {
		Stream.of(indexListStr.split(",")).forEach((s) -> {
			pathMap.put(s, null);
		});;
	}

	private void initBuffer() {
		buffer = ByteBuffer.allocate(bufferSize());
	}

	@Override
	public int bufferSize() {
		return hosePrsFileDefinitionHolder.getSize(getRecordType().getSimpleName());
	}


	@Override
	public Class<BasketFileRecord> getRecordType() {
		return BasketFileRecord.class;
	}

	
}
