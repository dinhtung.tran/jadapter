package com.fss.jadapter.data.receiver.hose.udp.xml;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fss.jadapter.converter.Converter;
import com.fss.jadapter.converter.hose.udp.xml.model.HoseUdpXmlPackage;
import com.fss.jadapter.data.model.hose.udp.HoseUdpBroadcastPackage;
import com.fss.jadapter.data.receiver.AbstractConvertedDataReceiver;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HoseUdpXmlConvertReceiver implements Processor, Converter<HoseUdpBroadcastPackage, String>, InitializingBean {

	@Autowired
	private Converter<HoseUdpBroadcastPackage, HoseUdpXmlPackage> hoseUdpXmlPackageConverter;
	private ObjectMapper xmlMapper = new XmlMapper();
	private PrintWriter pw;

	@Override
	public String convert(HoseUdpBroadcastPackage input) {
		HoseUdpXmlPackage convertedData = hoseUdpXmlPackageConverter.convert(input);
		String out = "";
		if (convertedData == null) {
			log.warn("convertedData is null! input: {}", input);
		} else {
			try {
				out = xmlMapper.writeValueAsString(convertedData);
			} catch (JsonProcessingException e) {
				log.error("Error!input: " + input, e);
			}
		}
		return out;
	}
	
	@Override
	public void process(Exchange exchange) throws Exception {
		HoseUdpBroadcastPackage hoseUdpBroadcastPackage = exchange.getMessage().getBody(HoseUdpBroadcastPackage.class);
		String content = convert(hoseUdpBroadcastPackage);
		long time = System.currentTimeMillis();
		StringBuilder strB = new StringBuilder();
		strB
//		.append(time)
//		.append('\1')
		.append(content);
//		pw.println(strB.toString());
		exchange.getMessage().setBody(strB.toString());
	}


	@Override
	public void afterPropertiesSet() throws Exception {
		try {
			pw = new PrintWriter(new FileWriter("D:/HoseUdpXmlData.txt", false), true);
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
}
