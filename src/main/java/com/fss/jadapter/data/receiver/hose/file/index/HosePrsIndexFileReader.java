package com.fss.jadapter.data.receiver.hose.file.index;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fss.jadapter.constant.definition.hose.file.HosePrsFileDefinitionHolder;
import com.fss.jadapter.constant.definition.hose.file.HosePrsFilePackageFactory;
import com.fss.jadapter.constant.definition.hose.file.model.FileRecordStruct;
import com.fss.jadapter.data.model.hose.file.HosePrsFilePackage;
import com.fss.jadapter.data.model.hose.file.record.BasketFileRecord;
import com.fss.jadapter.data.model.hose.file.record.IndexFileRecord;
import com.fss.jadapter.data.receiver.hose.file.HosePrsFileReader;
import com.fss.jadapter.data.repo.cache.HosePrsFileRepository;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Component
public class HosePrsIndexFileReader implements Processor, HosePrsFileReader<IndexFileRecord>, InitializingBean{
	
	public static final String FILENAME_DATE_PATTERN = "yyyyMMdd";
	public static final DateTimeFormatter FILENAME_DATE_FORMATTER = DateTimeFormatter.ofPattern(FILENAME_DATE_PATTERN);
	
	@Autowired
	private HosePrsFileRepository hosePrsFileRepository;
	@Value("${hose.prs.file.index.data.dir}") 
	private String parentIndexFileDir;
	@Value("${hose.prs.file.index.list}") 
	private String indexListStr;
	
	@Setter
	@Getter
	private Path path = null;
	@Setter
	@Getter
	private String indexName;
	
	@Autowired
	private HosePrsFilePackageFactory hosePrsFilePackageFactory;
	@Autowired
	private HosePrsFileDefinitionHolder hosePrsFileDefinitionHolder;
	
	private Map<String, SeekableByteChannel> byteChannelMap = new HashMap<>();
	private Map<String, Path> pathMap = new HashMap<>();
	private Map<String, String> byteChannelStatusMap = new HashMap<>();
	private Map<String, Long> positionMap = new HashMap<>();

	private ByteBuffer buffer;
	private FileRecordStruct fileRecordStruct;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		try {
//			log.info("HosePrsIndexFileReader  processor!");
			Map<String, HosePrsFilePackage<IndexFileRecord>> pck = getPackages();
			exchange.getMessage().setBody(pck);
		} catch (Exception e) {
			log.error("HosePrsIndexFileReader  processor error!", e);
		}
	}

	@Override
	public int bufferSize() {
		return hosePrsFileDefinitionHolder.getSize(getRecordType().getSimpleName());
	}

	@Override
	public Class<IndexFileRecord> getRecordType() {
		return IndexFileRecord.class;
	}

	@Override
	public final void afterPropertiesSet() throws Exception {		
		log.info("thisClass: {} RecordType: {}, Path: {}", this.getClass(), getRecordType(), getPath());
		initBuffer();
		initStruct();
		initPathMap();
		initChannel();
	}

	private Map<String, HosePrsFilePackage<IndexFileRecord>> getPackages() {
		Map<String, HosePrsFilePackage<IndexFileRecord>> map = new HashMap<>();
		for (String k : byteChannelMap.keySet()) {
			doTaskOnChannel(k, (c) -> map.put(k, getPackage(k, c)));
		}
		return map;
	}

	private HosePrsFilePackage<IndexFileRecord> getPackage(String index, SeekableByteChannel byteChannel) {
		Stream.Builder<byte[]> builder = Stream.builder();
		try {
			long position = positionMap.get(index);
			byteChannel.position(position);
			while ((byteChannel.size() - byteChannel.position()) >= bufferSize()) {
				buffer.clear();
				byteChannel.read(buffer);
				buffer.flip();
				byte[] data = new byte[buffer.limit()];
				buffer.get(data);
				builder.add(data);
			}
			positionMap.put(index, byteChannel.position());
		} catch (IOException e) {
			log.error("getPackage error! ", e);
		}
		
		return hosePrsFilePackageFactory.product(index, builder.build(), getRecordType());
	}

	//	private void openChannel(String index) {
	//		log.info("openChannel:: index {}", index);
	//		Date now = new Date();
	//		String dataPathDate = hosePrsFileRepository.getDataPathDate();
	//		String systemDate = hosePrsFileRepository.DATA_PATH_DATE_FORMATTER.format(now);
	//		if (systemDate.equals(dataPathDate)) {
	//			String fileName = FILENAME_DATE_FORMATTER.format(now) + "_VN" + index  + ".DAT";
	//			Path path = Paths.get(parentIndexFileDir, fileName);
	//			SeekableByteChannel byteChannel = openChannel(path);
	//			byteChannelMap.put(index, byteChannel);
	//			log.info("openChannel: path: {}", path);
	//		}
	//	}
		
		private void doTaskOnChannel(String index, Consumer<SeekableByteChannel> consumer) {
			LocalDate today = LocalDate.now();
			
			String dataPathDate = hosePrsFileRepository.getDataPathDate();
			String systemDate = HosePrsFileRepository.DATA_PATH_DATE_FORMATTER.format(today);
			
			if (systemDate != null && systemDate.equals(dataPathDate)) {
				String fileName = today.format(FILENAME_DATE_FORMATTER) + "_VN" + index  + ".DAT";
				Path path = Paths.get(parentIndexFileDir, fileName);
				SeekableByteChannel byteChannel = null;
				try {
					if (path == null) {
						log.info("doTaskOnChannel fail! Path is null!");
					} else {
						byteChannel = FileChannel.open(path, StandardOpenOption.READ);
						if (byteChannel != null && byteChannel.isOpen()) {
							consumer.accept(byteChannel);
							byteChannel.close();
						}
						else
							log.error("doTaskOnChannel error!");
					}
				} catch (IOException e) {
					log.error("openChannel error! ", e);
				}
			} else {
				log.info("systemDate != dataPathDate: systemDate {} dataPathDate: {}", systemDate, dataPathDate);
				initChannel();
			}		
		}

	private void initChannel() {
		Stream.of(indexListStr.split(",")).forEach((s) -> {
			byteChannelMap.put(s, null);
			positionMap.put(s, 0L);
		});;
	}

	private void initPathMap() {
		Stream.of(indexListStr.split(",")).forEach((s) -> {
			pathMap.put(s, null);
		});;
	}

	private void initStruct() {
		fileRecordStruct = hosePrsFileDefinitionHolder.getStruct(getRecordType());
		if (fileRecordStruct == null)
			throw new NullPointerException("fileRecordStruct cannot be null!");
	}
	
	private void initBuffer() {
		buffer = ByteBuffer.allocate(bufferSize());
	}

//	private void openChannel(String index) {
//		log.info("openChannel:: index {}", index);
//		Date now = new Date();
//		String dataPathDate = hosePrsFileRepository.getDataPathDate();
//		String systemDate = hosePrsFileRepository.DATA_PATH_DATE_FORMATTER.format(now);
//		if (systemDate.equals(dataPathDate)) {
//			String fileName = FILENAME_DATE_FORMATTER.format(now) + "_VN" + index  + ".DAT";
//			Path path = Paths.get(parentIndexFileDir, fileName);
//			SeekableByteChannel byteChannel = openChannel(path);
//			byteChannelMap.put(index, byteChannel);
//			log.info("openChannel: path: {}", path);
//		}
//	}


	
}
