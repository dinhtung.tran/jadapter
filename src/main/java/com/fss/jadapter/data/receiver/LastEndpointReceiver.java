package com.fss.jadapter.data.receiver;

public abstract class LastEndpointReceiver<I> extends AbstractConvertedDataReceiver<I, I> {

	@Override
	public final I convert(I input) {
		consume(input);
		return input;
	}

	protected abstract void consume(I input);
}
