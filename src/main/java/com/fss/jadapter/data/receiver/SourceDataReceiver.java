package com.fss.jadapter.data.receiver;

public interface SourceDataReceiver {
	public String getOutDataEndpointUri();
}
