package com.fss.jadapter.data.receiver;

import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;

import com.fss.jadapter.camel.CamelManager;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractConvertedDataReceiver<I, O> implements ConvertedDataReceiver<I, O> {
	
	public final void process(Exchange exchange) {
		Object out = null;
		try {
			Object inObj = exchange.getMessage().getBody();
			if (inObj == null) {
				log.warn("inObj body is null! this class: {}", this.getClass());
			} else {
				try {
					I in = (I) inObj;
					out = convert(in);
				} catch (ClassCastException cce) {
					log.warn("inObj is not Instance of {}! inObj class is {}! this class: {}", getInClass(),
							inObj.getClass(), this.getClass());
				}
			}
		} catch (Exception e) {
			log.error("Error! exchange: {}", exchange);
			log.error("", e);
		}
		exchange.getMessage().setBody(out);
	}
	
}
