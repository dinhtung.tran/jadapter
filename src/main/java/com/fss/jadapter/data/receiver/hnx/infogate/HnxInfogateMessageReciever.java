package com.fss.jadapter.data.receiver.hnx.infogate;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fss.jadapter.converter.Converter;
import com.fss.jadapter.data.model.hnx.infogate.HnxInfogateMessage;

import lombok.extern.slf4j.Slf4j;
import quickfix.Message;

@Component
@Slf4j(topic = "HnxInfogateMessageReciever")
public class HnxInfogateMessageReciever implements Processor, InitializingBean {


	@Autowired
	private Converter<String, HnxInfogateMessage> hnxInfogateMessageConverter;
	@Autowired
	private Converter<String, Message> fixMessageConverter;
	
	private ObjectMapper mapper = new ObjectMapper();
	
	@Override
	public void process(Exchange exchange) throws Exception {
		String input = exchange.getMessage().getBody(String.class);
		log.info("Rcv:::{}", input);
		Object message = fixMessageConverter.convert(input);
//		Object message = hnxInfogateMessageConverter.convert(input);		
		if (message != null) {
//			log.debug("message class: {}", message.getClass());
			exchange.getMessage().setBody(message);
		} else {
			log.warn("Null message! Input: {}", input);
		}		
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		log.info("Start new log! Time: {}", LocalDateTime.now());
	}
}
