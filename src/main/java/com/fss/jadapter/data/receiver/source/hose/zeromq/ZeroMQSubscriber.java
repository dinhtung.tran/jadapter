package com.fss.jadapter.data.receiver.source.hose.zeromq;

import java.time.LocalDateTime;
import java.util.Queue;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ.Socket;

import com.fss.jadapter.camel.CamelManager;
import com.fss.jadapter.constant.StringValue;
import com.fss.jadapter.data.receiver.SourceDataReceiver;
import com.fss.jadapter.model.ActiveStatus;
import com.fss.utilities.array.ByteArrayU;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Generated;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Data(staticConstructor = "of")
@ToString(onlyExplicitlyIncluded = true, doNotUseGetters = true)
public class ZeroMQSubscriber implements InitializingBean, SourceDataReceiver {

	private static final SocketType SOCKET_TYPE = SocketType.SUB;
	
	private static final Logger PERSIST = LoggerFactory.getLogger("HosePrsBroadcaseSourceData");

	private static final int SOCKET_RECEIVE_TIME_OUT = 5000;

	protected static final long SLEEP_TIME = 100;
	
	@Value("${zero.mq.correction.length}")
	@Setter(AccessLevel.NONE)
	@ToString.Include
	private int correction;

	@Generated
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	@ToString.Include
	private int id;
	
	@Setter(AccessLevel.NONE)
//	@Getter(AccessLevel.NONE)
	@ToString.Include
	private volatile long messageCount1;
	@Setter(AccessLevel.NONE)
//	@Getter(AccessLevel.NONE)
	@ToString.Include
	private volatile long messageCount2;
	@Setter(AccessLevel.NONE)
//	@Getter(AccessLevel.NONE)
	@ToString.Include
	private volatile long messageCount3;

	@ToString.Include
	private String protocol;

	@ToString.Include
	private String host;

	@ToString.Include
	private int port;

	@ToString.Include
	private String topic;

	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	private Socket socket;
	
	@ToString.Include
	@Setter(AccessLevel.NONE)
	private ActiveStatus activeStatus = ActiveStatus.STOPED;
	
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	@Autowired
	private CamelManager camelManager;
	
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	private Queue<byte[]> dataQueue = new LinkedBlockingQueue<>();
	
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	private Runnable mainTask = new Runnable() {
		@Override
		public void run() {
			log.info("mainTask start! this: {}", ZeroMQSubscriber.this);
			while (activeStatus != ActiveStatus.DESTROYED) {
				if (activeStatus != ActiveStatus.STARTED) {
					try {
						Thread.sleep(SLEEP_TIME);
					} catch (InterruptedException e) {
						log.error("Error! this: {}", ZeroMQSubscriber.this);
						log.error("", e);
					}
				} else {
					byte[] data = socket.recv(0);
//					log.info("socket recv: {}", ByteArrayU.toString(data));
					enqueueData(data);
				}
			}
			log.info("mainTask stop! this: {}", ZeroMQSubscriber.this);
		}
	};
	@Setter(AccessLevel.NONE)
	@Getter(AccessLevel.NONE)
	private Runnable dequeueDataTask = new Runnable() {
		@Override
		public void run() {
			log.info("dequeueDataTask start! this: {}", ZeroMQSubscriber.this);
			while (activeStatus != ActiveStatus.DESTROYED) {
				if (!dataQueue.isEmpty()) {
					byte[] data = dataQueue.poll();
					camelManager.sendBodyTo(getOutDataEndpointUri(), data);
				} else {
					try {
						Thread.sleep(SLEEP_TIME);
					} catch (InterruptedException e) {
						log.error("Error! this: {}", ZeroMQSubscriber.this);
						log.error("", e);
					}
				}
			}
			log.info("dequeueDataTask stop! this: {}", ZeroMQSubscriber.this);
		}
	};

	private void init() {
		log.info("Creating ZeroMQSubscriber: {}", this);
		ZContext zContext = new ZContext();
		socket = zContext.createSocket(SOCKET_TYPE);
		String url = protocol + "://" + host + ":" + port;
		socket.connect(url);
		socket.subscribe(topic);
		socket.setReceiveTimeOut(SOCKET_RECEIVE_TIME_OUT);
		socket.setRcvHWM(Integer.MAX_VALUE);
		log.info("ZeroMQSubscriber has created: {}", url);
		log.info("ZeroMQSubscriber socket RCV_HWM: {}", socket.getRcvHWM());
		run();
	}

	private void run() {
		Executors.newSingleThreadExecutor().execute(mainTask);
//		Executors.newSingleThreadExecutor().execute(dequeueDataTask);
	}
	
	private void enqueueData(byte[] data) {
		messageCount1++;
		if (data == null || data.length == 0) {
			return;
		}
		log.info("enqueueData: {}", ByteArrayU.toString(data));
		messageCount2++;
		byte[] parsedData = ByteArrayU.cut(data, topic.length(), data.length);
		parsedData = ByteArrayU.cut(parsedData, correction, parsedData.length);
		log.info("parsedData: {}", ByteArrayU.toString(parsedData));
		PERSIST.info("Rcv:::{}", ByteArrayU.toString(parsedData));
		camelManager.sendBodyTo(getOutDataEndpointUri(), parsedData);

	}

	@PreDestroy
	public void destroy() {
		activeStatus = ActiveStatus.DESTROYED;
		String method = "destroy";
		log.info("invoking method: {}! this: {}", method, this);
		socket.close();
	}
	
	public void start() {
		String method = "start";
		if (activeStatus == ActiveStatus.STOPED) {
			activeStatus = ActiveStatus.STARTING;
			log.info("invoking method: {}! this: {}", method, this);		
//			if (consumer == null) {
//				log.warn("consumer is NULL! Do nothing when receive message! this: {}", method, this);		
//			}
			dataQueue.clear();
			socket.subscribe(topic);
			activeStatus = ActiveStatus.STARTED;
			log.info("Invoke done, method: {}! this: {}", method, this);
		} else {
			log.warn("Invoke failed, method: {}! this: {}", method, this);
		}
			
	}
	
	public void stop() {
		String method = "stop";
		if (activeStatus == ActiveStatus.STARTED || activeStatus == ActiveStatus.PAUSED) {
			activeStatus = ActiveStatus.STOPPING;
			log.info("invoking method: {}! this: {}", method, this);
			dataQueue.clear();
			socket.unsubscribe(topic);
			activeStatus = ActiveStatus.STOPED;
			log.info("Invoke done, method: {}! this: {}", method, this);
		} else {
			log.warn("Invoke failed, method: {}! this: {}", method, this);
		}
	}
	
	public void pause() {
		String method = "pause";
		if (activeStatus == ActiveStatus.STARTED) {
			log.info("invoking method: {}! this: {}", method, this);
			activeStatus = ActiveStatus.PAUSED;
			
			log.info("Invoke done, method: {}! this: {}", method, this);
		} else {
			log.warn("Invoke failed, method: {}! this: {}", method, this);
		}
	}
	
	public void resume() {
		String method = "resume";
		if (activeStatus == ActiveStatus.PAUSED) {
			log.info("invoking method: {}! this: {}", method, this);
			activeStatus = ActiveStatus.STARTED;

			log.info("Invoke done, method: {}! this: {}", method, this);
		} else {
			log.warn("Invoke failed, method: {}! this: {}", method, this);
		}			
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		destroy();
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		init();
		PERSIST.info("Start new log! Time: {}", LocalDateTime.now());
	}

	@Override
	public String getOutDataEndpointUri() {
		return StringValue.Hose.Udp.OUT_DATA_ENDPOINT_URI;
	}

}
