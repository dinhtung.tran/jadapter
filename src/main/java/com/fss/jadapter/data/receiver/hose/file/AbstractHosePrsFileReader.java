package com.fss.jadapter.data.receiver.hose.file;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.function.Consumer;
import java.util.stream.Stream;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import com.fss.jadapter.constant.definition.hose.file.HosePrsFileDefinitionHolder;
import com.fss.jadapter.constant.definition.hose.file.HosePrsFilePackageFactory;
import com.fss.jadapter.constant.definition.hose.file.model.FileRecordStruct;
import com.fss.jadapter.data.model.hose.file.HosePrsFilePackage;
import com.fss.jadapter.data.model.hose.file.HosePrsFileRecord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractHosePrsFileReader<T extends HosePrsFileRecord> implements Processor, HosePrsFileReader<T>, InitializingBean{
		
	@Autowired
	private HosePrsFilePackageFactory hosePrsFilePackageFactory;
	@Autowired
	private HosePrsFileDefinitionHolder hosePrsFileDefinitionHolder;
	
	private ByteBuffer buffer;
	private FileRecordStruct fileRecordStruct;
	
	@Override
	public void process(Exchange exchange) throws Exception {
		try {
			HosePrsFilePackage<T> pck = getPackage();
			exchange.getMessage().setBody(pck);
		} catch (Exception e) {
			log.error("", e);
		}
	}

	private HosePrsFilePackage<T> getPackage() throws IOException {
		Stream.Builder<byte[]> builder = Stream.builder();
		
		doTaskOnChannel((byteChannel) -> {
			long position = 0;
			try {
				byteChannel.position(position);
				while ((byteChannel.size() - position) >= bufferSize()) {
					buffer.clear();
					byteChannel.read(buffer);
					buffer.flip();
					byte[] data = new byte[buffer.limit()];
					buffer.get(data);
					builder.add(data);
					position = byteChannel.position();
				}
			} catch (IOException e) {
				log.error("doTaskOnChannel error!", e);
			}
		});
		
		
		return hosePrsFilePackageFactory.product("", builder.build(), getRecordType());
	}

	private void doTaskOnChannel(Consumer<SeekableByteChannel> consumer) {
		SeekableByteChannel byteChannel = null;
		try {
			if (getPath() == null) {
				log.info("doTaskOnChannel fail! Path is null!");
			} else {
				byteChannel = FileChannel.open(getPath(), StandardOpenOption.READ);
				if (byteChannel != null && byteChannel.isOpen()) {
					consumer.accept(byteChannel);
					byteChannel.close();
				}
				else
					log.error("doTaskOnChannel error!");
			}
		} catch (IOException e) {
			log.error("openChannel error! ", e);
		}
		
	}

	@Override
	public final void afterPropertiesSet() throws Exception {		
		init();
		log.info("thisClass: {} RecordType: {}, Path: {}", this.getClass(), getRecordType(), getPath());
		initBuffer();
		initStruct();
	}

	protected abstract void init();

	private void initStruct() {
		fileRecordStruct = hosePrsFileDefinitionHolder.getStruct(getRecordType());
		if (fileRecordStruct == null)
			throw new NullPointerException("fileRecordStruct cannot be null!");
	}

	private void initBuffer() {
		buffer = ByteBuffer.allocate(bufferSize());
	}
	
	@Override
	public int bufferSize() {
		return hosePrsFileDefinitionHolder.getSize(getRecordType().getSimpleName());
	}
}
