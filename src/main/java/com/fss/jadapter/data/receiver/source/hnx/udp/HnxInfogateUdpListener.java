package com.fss.jadapter.data.receiver.source.hnx.udp;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fss.jadapter.camel.CamelManager;
import com.fss.jadapter.constant.StringValue.Hnx;
import com.fss.jadapter.data.receiver.SourceDataReceiver;
import com.fss.jadapter.data.receiver.source.hose.zeromq.ZeroMQSubscriber;
import com.fss.jadapter.model.ActiveStatus;

import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@ToString(onlyExplicitlyIncluded = true)
public class HnxInfogateUdpListener implements InitializingBean, SourceDataReceiver, DisposableBean {
	
	private static final int ONE_MEGA_BYTE = 1024 * 1024;
	private static final int BUFFER_SIZE = 10 * ONE_MEGA_BYTE;
	private static final int SOCKET_RCV_BUF_SIZE = 200 * ONE_MEGA_BYTE;
	
	protected static final long SLEEP_TIME = 100;

	@Value("${hnx.broadcast.port}")
	private int port;
	@Value("${hnx.broadcast.protocol}")
	private String protocol = "udp";
	@Value("${hnx.broadcast.host}")
	private String host;
	
	@Autowired
	private CamelManager camelManager;
	
	private InetSocketAddress socketAddr;
	private DatagramChannel channel;
	private ByteBuffer buffer;
	@ToString.Include
	private long count = 0;
	
	private ActiveStatus activeStatus = ActiveStatus.STOPED;

	@Override
	public String getOutDataEndpointUri() {
		return Hnx.OUT_DATA_ENDPOINT_URI;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		init();
		activeStatus = ActiveStatus.STARTED;
	}
	
	private void init() throws IOException {
		channel = DatagramChannel.open();
		socketAddr = new InetSocketAddress(InetAddress.getByName(host), port);		
		channel.setOption(StandardSocketOptions.SO_BROADCAST, true);
		channel.setOption(StandardSocketOptions.SO_REUSEADDR, true);
		channel.setOption(StandardSocketOptions.SO_RCVBUF, SOCKET_RCV_BUF_SIZE);
		channel.configureBlocking(true);
				
		channel.bind(socketAddr);
		
		log.info("channel isBlocking mode: {}", channel.isBlocking());
		log.info("channel SO_RCVBUF: {}", channel.getOption(StandardSocketOptions.SO_RCVBUF));
		log.info("Hnx Broadcast Server Started: {}", socketAddr);
		
		buffer = ByteBuffer.allocate(BUFFER_SIZE);
		
		Executors.newSingleThreadExecutor().execute(mainTask);
		log.info("Executed mainTask!");
	}
	
	private Runnable mainTask = new Runnable () {
		@Override
		public void run() {
			while (activeStatus != ActiveStatus.DESTROYED) {
				if (activeStatus != ActiveStatus.STARTED) {
					try {
						Thread.sleep(SLEEP_TIME);
					} catch (InterruptedException e) {
						log.error("Error! this: {}", HnxInfogateUdpListener.this);
						log.error("", e);
					}
				} else {
					try {
						byte[] data = receiveData();
						if (data != null && data.length > 0)
							camelManager.sendBodyTo(getOutDataEndpointUri(), new String(data));
					} catch (Exception e) {
						log.error("", e);
					}
				}
			}
			log.info("mainTask has stopped!");
		}
	};

	private byte[] receiveData() throws IOException {
		if (channel.receive(buffer) == null) {
			return null;
		};
		buffer.flip();
		byte[] data = new byte[buffer.limit()];
		buffer.get(data);
		buffer.clear();
		count++;
		return data;
	}

	@Override
	public void destroy() throws Exception {
		buffer.clear();
		channel.close();
		activeStatus = ActiveStatus.DESTROYED;
	}
}
