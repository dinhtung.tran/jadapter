package com.fss.jadapter.data.receiver.hose.file;

import java.nio.file.Path;

import com.fss.jadapter.data.model.hose.file.HosePrsFileRecord;

public interface HosePrsFileReader<T extends HosePrsFileRecord> {
	int bufferSize();
	Path getPath();
	Class<T> getRecordType();
}
