package com.fss.jadapter.data.receiver;

import org.apache.camel.Processor;

import com.fss.jadapter.converter.Converter;

public interface ConvertedDataReceiver<I, O> extends Processor, Converter<I, O> {
	public Class<I> getInClass();
	public Class<O> getOutClass();
}
