package com.fss.jadapter.constant;

public class HoseSystemControlCode {
	public static final String ATO = "P";
	public static final String ATC = "A";
	public static final String OPEN = "O";
	public static final String INTERMISSION = "I";
	public static final String CROSS = "C";
	
	public static final String BEGIN_EOD_UPDATE = "G";
	public static final String END_EOD_UPDATE = "J";
	
	public static final String END_RUNOFF_PERIOR = "K";
	
}
