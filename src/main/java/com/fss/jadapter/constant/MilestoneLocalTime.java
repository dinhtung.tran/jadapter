package com.fss.jadapter.constant;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.function.Consumer;

public class MilestoneLocalTime {
	private static final String TIME_OF_DAY_PATTERN = "HH:mm:ss";
	
	public static final DateTimeFormatter TIME_OF_DAY_FORMATTER = DateTimeFormatter.ofPattern(TIME_OF_DAY_PATTERN);

	public static final LocalTime INIT_DAY_START = of("07:59:59");
	public static final LocalTime INIT_DAY_END = of("08:40:00");
	
	public static final LocalTime FDS_ATO = of("08:45:00");
	public static final LocalTime FDS_CNT_MORNING = of("09:00:00");
	public static final LocalTime FDS_LB = of("11:30:00");
	public static final LocalTime FDS_CNT_AFTERNOON = of("13:00:00");
	public static final LocalTime FDS_ATC = of("14:30:00");
	public static final LocalTime FDS_CLOSE_MARKET = of("14:45:00");
	
	public static final LocalTime HNX_CNT_MORNING = of("09:00:00");
	public static final LocalTime HNX_LB = of("11:30:00");
	public static final LocalTime HNX_CNT_AFTERNOON = of("13:00:00");
	public static final LocalTime HNX_ATC = of("14:30:00");
	public static final LocalTime HNX_PLO = of("14:45:00");
	public static final LocalTime HNX_CLOSE_MARKET = of("15:00:00");
	
	public static final LocalTime HOSE_CNT_MORNING = of("09:15:00");
	public static final LocalTime HOSE_LB = of("11:30:00");
	public static final LocalTime HOSE_CNT_AFTERNOON = of("13:00:00");
	public static final LocalTime HOSE_ATC = of("14:30:00");
	public static final LocalTime HOSE_CROSS_ONLY = of("14:45:00");
	public static final LocalTime HOSE_CLOSE_MARKET = of("15:00:00");

	public static final LocalTime of(String time) {
		return LocalTime.parse(time, TIME_OF_DAY_FORMATTER);
	}
	
	private static LocalTime currentTime = of("00:00:00");
	private static LocalDate currentDate = LocalDate.now();
	
	public static LocalTime getCurrentTime() {
		return currentTime;
	}
	
	public static LocalDate getCurrentDate() {
		return currentDate;
	}
	
	public static synchronized void setCurrentTime(LocalTime time) {
		currentTime = time;
	}
	
	public static synchronized void setCurrentDate(LocalDate date) {
		currentDate = date;
	}
	
	public static void doTaskEverySecond(LocalTime time, int interval, Runnable task) {
		if (time.getSecond() % interval == 0)
			task.run();
	}
	
}
