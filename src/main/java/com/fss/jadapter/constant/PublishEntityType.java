package com.fss.jadapter.constant;

public final class PublishEntityType {
	public static final String STOCK_INFOR = "StockInfor";
	public static final String MARKET_INFOR = "MarketInfor";
	public static final String TRANSLOG_INFOR = "TransLog";
	
	private static final char URL_SEPARATOR = '/';

	public static  String buildUrl(String prefix, String key) {
		StringBuffer buffer = new StringBuffer(32);
		buffer.append(prefix).append(URL_SEPARATOR).append(key);
		return buffer.toString();
	}

	public static final String getStockInforUrl(String symbol) {
		String prefix = STOCK_INFOR;
		return buildUrl(prefix, symbol);
		
	}

	public static  final String getMarketInforUrl(String indexId) {
		String prefix = MARKET_INFOR;
		return buildUrl(prefix, indexId);
	}

	public static  final String getTransLogUrl(String symbol) {
		String prefix = TRANSLOG_INFOR;
		return buildUrl(prefix, symbol);
	}
}
