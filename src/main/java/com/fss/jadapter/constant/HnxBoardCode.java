package com.fss.jadapter.constant;

import com.fss.jadapter.data.persistence.model.v2.market.IndexInfor;

import lombok.Data;

@Data(staticConstructor = "of")
public class HnxBoardCode {
	public static final String HNX_BOARD_01 = "LIS_BRD_01";
	public static final String UPCOM_BOARD_01 = "UPC_BRD_01";
	public static final String DER_BOARD_01 = "DER_BRD_01";
	public static final String BOND_BOARD = "LIS_BRD_BOND";
	
	public static final String getIndexIdByBoard(String boardCode) {
		if (boardCode == null || boardCode.isEmpty())
			return null;
		switch (boardCode.toUpperCase()) {
		case HNX_BOARD_01:
			return IndexInfor.HNX;
		case UPCOM_BOARD_01:
			return IndexInfor.UPCOM;
		case DER_BOARD_01:
		case BOND_BOARD:
		default:
			return "";
		}
	}
}
