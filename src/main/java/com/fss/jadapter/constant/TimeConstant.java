package com.fss.jadapter.constant;

public final class TimeConstant {
	public static final long ONE_MILISECOND = 1;
	public static final long ONE_HUNDRED_MILISECOND = 100;
	public static final long ONE_SECOND = 1000;
	public static final long ONE_MINUTE = ONE_SECOND * 60;
	public static final long ONE_HOUR = ONE_MINUTE * 60;
}
