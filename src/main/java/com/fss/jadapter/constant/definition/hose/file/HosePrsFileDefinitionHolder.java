package com.fss.jadapter.constant.definition.hose.file;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fss.jadapter.constant.definition.hose.file.model.FileRecordStruct;
import com.fss.jadapter.data.model.hose.file.HosePrsFileRecord;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class HosePrsFileDefinitionHolder implements InitializingBean {
	
	@Value("${jadapter.definition.hose.file.record.struct}")
	private String fileRecordStructResource;
	
	private ObjectMapper mapper = new ObjectMapper();
	
	private Map<String, FileRecordStruct> fileRecordStructMapper = new HashMap<>();
	private Map<String, Integer> fileRecordStructSizeMapper = new HashMap<>();			
	
	public int getSize(String recordType) {
		if (fileRecordStructSizeMapper.containsKey(recordType))
			return fileRecordStructSizeMapper.get(recordType);
		return 0;
	}
	
	public FileRecordStruct getStruct(Class<? extends HosePrsFileRecord> clazz) {
		return getStruct(clazz.getSimpleName());
	}
	
	public FileRecordStruct getStruct(String recordType) {
		return fileRecordStructMapper.get(recordType);
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		initFileRecordStructMapper();
		log.info("fileRecordStructMapper: {}", fileRecordStructMapper);
		log.info("fileRecordStructSizeMapper: {}", fileRecordStructSizeMapper);
	}

	private void initFileRecordStructMapper() throws IOException {
		log.info("fileRecordStructResource: {}", fileRecordStructResource);
		InputStream fileRecordStructResourceInputStream = new ClassPathResource(fileRecordStructResource).getInputStream();

		ObjectNode node = (ObjectNode) mapper.readTree(fileRecordStructResourceInputStream);
		
		log.info("fileRecordStruct: {}", node);
		
		ArrayNode arrayNode = (ArrayNode) node.get("messageTypes");
		arrayNode.forEach((e) -> {
			FileRecordStruct s = mapper.convertValue(e, FileRecordStruct.class);
			int[] totalSize = {0};
			s.getFields().forEach((f) -> {
				totalSize[0] = totalSize[0] + f.getSize();
			});
			
			Map<String, Object> exampleObject = new LinkedHashMap<>();
			s.getFields().forEach((f) -> {
				String fieldName = f.getName();
				Object value = getValue(f.getType());
				exampleObject.put(fieldName, value);
			});
			
//			try {
//				log.info(" msgtype: {} exampleObject: {}", s.getMessageType(), mapper.writeValueAsString(exampleObject));
//			} catch (JsonProcessingException e1) {
//				// TODO Auto-generated catch block
//				e1.printStackTrace();
//			}
			
			fileRecordStructMapper.put(s.getMessageType(), s);
			fileRecordStructSizeMapper.put(s.getMessageType(), Integer.valueOf(totalSize[0]));
		});
	}

	private Object getValue(String type) {
		if (HosePrsFileRecordFactory.INTERGER.equals(type)) {
			return 0;
		} else if (HosePrsFileRecordFactory.FLOAT.equals(type)) {
			return 0.0;
		} else if (HosePrsFileRecordFactory.STRING.equals(type)) {
			return "";
		} else {
			return "";
		}
	}
}
