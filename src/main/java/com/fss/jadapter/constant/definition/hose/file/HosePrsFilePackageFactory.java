package com.fss.jadapter.constant.definition.hose.file;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fss.jadapter.data.model.hose.file.HosePrsFilePackage;
import com.fss.jadapter.data.model.hose.file.HosePrsFileRecord;
import com.fss.utilities.array.ByteArrayU;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class HosePrsFilePackageFactory implements InitializingBean {
	
	@Autowired
	private HosePrsFileDefinitionHolder hosePrsFileDefinitionHolder;
	@Autowired
	private HosePrsFileRecordFactory hosePrsFileRecordFactory;
	
	public <T extends HosePrsFileRecord> HosePrsFilePackage<T> product(String key, Stream<byte[]> stream, Class<T> clazz) {
		List<T> records = new ArrayList<>();
		HosePrsFilePackage<T> pck = HosePrsFilePackage.of(records);
		
		stream.forEach((data) -> {
			T record = hosePrsFileRecordFactory.<T>product(data, clazz);
			if(record != null) {
				pck.addRecord(record);
			} else {
				log.warn("Record result is null! data: {} len: {} clazz: {}", ByteArrayU.toString(data), data.length, clazz.getSimpleName());
			}
		});
		
		return pck;
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		
	}
	
}
