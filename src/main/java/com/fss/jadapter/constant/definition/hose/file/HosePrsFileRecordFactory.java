package com.fss.jadapter.constant.definition.hose.file;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fss.jadapter.constant.definition.hose.file.model.FileRecordStruct;
import com.fss.jadapter.data.model.hose.file.HosePrsFileRecord;
import com.fss.utilities.array.ByteArrayU;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class HosePrsFileRecordFactory implements InitializingBean {

	public static final String INTERGER = "int";
	public static final String FLOAT = "float";
	public static final String STRING = "String";

	@Autowired
	private HosePrsFileDefinitionHolder hosePrsFileDefinitionHolder;
	private ObjectMapper mapper = new ObjectMapper();

	public <T extends HosePrsFileRecord> T product(byte[] data, Class<T> clazz) {
		Map<String, Object> map = new HashMap<>();
		try {
			FileRecordStruct struct = hosePrsFileDefinitionHolder.getStruct(clazz);
			ByteBuffer buffer = ByteBuffer.wrap(data);
			struct.getFields().forEach((f) -> {
				String fName = f.getName();
				int size = f.getSize();
				byte[] d = new byte[size];
				buffer.get(d);
				ByteBuffer mbuffer = ByteBuffer.wrap(d).order(ByteOrder.LITTLE_ENDIAN);
				String type = f.getType();
				Object value;

				if (INTERGER.equals(type)) {
					if (size == 2) {
						value = mbuffer.getShort();
					} else if (size == 4) {
						value = mbuffer.getInt();
					} else if (size == 8) {
						value = mbuffer.getLong();
					} else {
						value = new String(d);
						log.warn("Invalid case 1: value: {}", value);
					}
				} else if (FLOAT.equals(type)) {
					if (size == 4) {
						value = mbuffer.getFloat();
					} else if (size == 8) {
						value = mbuffer.getDouble();
					} else {
						value = new String(d);
						log.warn("Invalid case 2: value: {}", value);
					}
				} else if (STRING.equals(type)) {
					value = new String(d);
				} else {
					value = new String(d);
					log.warn("Invalid case 3: value: {}", value);
				}

				map.put(fName, value);

			});
//			log.info("Json: {}", mapper.writeValueAsString(map));
			T rs = mapper.convertValue(map, clazz);
			return rs;
		} catch (Exception e) {
//			log.error("Error! Data: {}", ByteArrayU.toString(data));
			log.error("", e);
			return null;
		}
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub

	}
}
