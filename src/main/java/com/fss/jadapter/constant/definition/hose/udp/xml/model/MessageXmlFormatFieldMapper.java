package com.fss.jadapter.constant.definition.hose.udp.xml.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@RequiredArgsConstructor(staticName = "of")
@ToString
@NoArgsConstructor
public class MessageXmlFormatFieldMapper {
	@NonNull
	private String sourceFieldName;
	@NonNull
	private String convertedFieldName;
}
