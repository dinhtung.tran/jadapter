package com.fss.jadapter.constant.definition.hose.udp.xml.model;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Getter
@ToString
@RequiredArgsConstructor(staticName = "of")
@NoArgsConstructor
public class MessageXmlFormatHolder {
	@NonNull
	private String messageType;
	@NonNull
	private List<MessageXmlFormatFieldMapper> fields;
	
}
