package com.fss.jadapter.constant.definition.hose.udp.model;

import java.util.List;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class BroadcastPackageStruct {
	private String name;
	private List<BroadcastMessageMessageField> struct;
}
