package com.fss.jadapter.constant.definition.hose.udp;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fss.jadapter.constant.definition.hose.udp.model.BroadcastMessageClass;
import com.fss.jadapter.constant.definition.hose.udp.model.BroadcastMessageType;
import com.fss.jadapter.constant.definition.hose.udp.model.BroadcastPackageStruct;
import com.fss.jadapter.constant.definition.hose.udp.xml.model.MessageXmlFormatHolder;
import com.fss.jadapter.constant.definition.hose.udp.xml.model.MessageXmlFormatFieldMapper;
import lombok.Getter;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class HoseUdpDefinitionHolder implements InitializingBean {
	
	@Value("${jadapter.definition.hose.udp.broadcast.package.struct}")
	private String broadcastPackageStructResource;
	@Value("${jadapter.definition.hose.udp.broadcast.message.types}")
	private String broadcastMessageTypesResource;
	@Value("${jadapter.definition.hose.udp.broadcast.message.xml.format}")
	private String broadcastMessageXmlFormatResource;
	
	private ObjectMapper mapper = new ObjectMapper();
	
	@Getter
	private BroadcastPackageStruct broadcastPackageStruct;
	private BroadcastMessageClass broadcastMessageClass;
	private Map<String, MessageXmlFormatHolder> broadcastMessageXmlFormatMapper = new HashMap<>();
	private Map<String, BroadcastMessageType> broadcastMessageTypeMapper = new HashMap<>();
	
	public BroadcastMessageType getBroadcastMessageType(String messageType) {
		return broadcastMessageTypeMapper.get(messageType);
	}
	
	public MessageXmlFormatHolder getMessageXmlFormatHolder(String messageType) {
		return broadcastMessageXmlFormatMapper.get(messageType);
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		initBroadcastMessageTypeMapper();
		initXmlTypeMapper();
	}

	private void initXmlTypeMapper() throws IOException {
		log.info("broadcastPackageStructResource: {}", broadcastMessageXmlFormatResource);
		InputStream broadcastPackageStructResourceInputStream = new ClassPathResource(broadcastMessageXmlFormatResource).getInputStream();
		TypeReference<List<MessageXmlFormatHolder>> typeRef = new TypeReference<List<MessageXmlFormatHolder>>() {};
		List<MessageXmlFormatHolder> list = mapper.readValue(broadcastPackageStructResourceInputStream, typeRef);
		
		for (MessageXmlFormatHolder e : list) {
			log.info("MessageXmlFormatHolder: {}", e);
			broadcastMessageXmlFormatMapper.put(e.getMessageType(), e);
		}
		
		
		
		List<MessageXmlFormatHolder> out = new ArrayList<>();
		broadcastMessageClass.getMessageTypes().forEach((e) -> {
			List<MessageXmlFormatFieldMapper> fieldList = new ArrayList<>();
			e.getFields().forEach((f) -> {
				fieldList.add(MessageXmlFormatFieldMapper.of(f.getName(), camelToSnake(f.getName())));
			});
			
			MessageXmlFormatHolder messageXmlFormatHolder = MessageXmlFormatHolder.of(e.getMessageType(), fieldList);
			out.add(messageXmlFormatHolder);
		});;
		log.info("Convert Done! \r\n{}", mapper.writeValueAsString(out));
	}

	private String camelToSnake(String str)   { 
  
        // Empty String 
        String result = ""; 
  
        // Append first character(in lower case) 
        // to result string 
        char c = str.charAt(0); 
        result = result + Character.toLowerCase(c); 
  
        // Tarverse the string from 
        // ist index to last index 
        for (int i = 1; i < str.length(); i++) { 
  
            char ch = str.charAt(i); 
  
            // Check if the character is upper case 
            // then append '_' and such character 
            // (in lower case) to result string 
            if (Character.isUpperCase(ch)) { 
                result = result + '_'; 
                result 
                    = result 
                      + Character.toLowerCase(ch); 
            } 
  
            // If the character is lower case then 
            // add such character into result string 
            else { 
                result = result + ch; 
            } 
        } 
  
        // return the result 
        return result; 
    } 

	private void initBroadcastMessageTypeMapper() throws IOException {
		log.info("broadcastPackageStructResource: {}", broadcastPackageStructResource);
		log.info("broadcastMessageTypesResource: {}", broadcastMessageTypesResource);
		InputStream broadcastPackageStructResourceInputStream = new ClassPathResource(broadcastPackageStructResource).getInputStream();
		InputStream broadcastMessageTypesResourceInputStream = new ClassPathResource(broadcastMessageTypesResource).getInputStream();

		broadcastPackageStruct = mapper.readValue(broadcastPackageStructResourceInputStream, BroadcastPackageStruct.class);
		broadcastMessageClass = mapper.readValue(broadcastMessageTypesResourceInputStream, BroadcastMessageClass.class);
		
		log.info("broadcastPackageStruct: {}", broadcastPackageStruct);
		log.info("broadcastMessageClass: {}", broadcastMessageClass);
		for (BroadcastMessageType t : broadcastMessageClass.getMessageTypes()) {
			broadcastMessageTypeMapper.put(t.getMessageType(), t);
		}
	}	
}
