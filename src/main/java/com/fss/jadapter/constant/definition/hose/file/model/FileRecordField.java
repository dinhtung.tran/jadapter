package com.fss.jadapter.constant.definition.hose.file.model;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class FileRecordField {
	private String name;
	private int size;
	private String type;
	private String messageClass;
	private int separatorUnit;	
}
