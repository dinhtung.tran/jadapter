package com.fss.jadapter.constant.definition.hose.file.model;

import java.util.List;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class FileRecordStruct {
	private String messageType;
	private List<FileRecordField> fields;
}
