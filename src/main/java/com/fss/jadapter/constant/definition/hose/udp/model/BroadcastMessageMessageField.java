package com.fss.jadapter.constant.definition.hose.udp.model;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class BroadcastMessageMessageField {
	private String name;
	private int size;
	private String type;
	private String messageClass;
	private int separatorUnit;	
}
