package com.fss.jadapter.constant.definition.hose.udp;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fss.jadapter.constant.StringValue;
import com.fss.jadapter.constant.StringValue.DataType;
import com.fss.jadapter.constant.definition.hose.udp.model.BroadcastMessageMessageField;
import com.fss.jadapter.constant.definition.hose.udp.model.BroadcastMessageType;
import com.fss.jadapter.data.model.hose.udp.HoseUdpBroadcastMessage;
import com.fss.utilities.array.ByteArrayU;
import com.fss.utilities.converter.Mod96;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class HoseUdpBroadcastMessageFactory implements InitializingBean {
	@Autowired
	private HoseUdpDefinitionHolder hoseUdpDefinitionHolder;
	private ObjectMapper mapper = new ObjectMapper();

	@Override
	public void afterPropertiesSet() throws Exception {
	}

	public HoseUdpBroadcastMessage product(byte[] data) {
		try {
			HoseUdpBroadcastMessage hoseUdpBroadcastMessage = null;
			ByteBuffer buffer = ByteBuffer.wrap(data);
			Map<String, Object> keyValueRepo = new HashMap<>();
			String messageType = new String(ByteArrayU.cut(data, 0, StringValue.Hose.MSG_TYPE_LENGTH));
			BroadcastMessageType broadcastMessageType = hoseUdpDefinitionHolder.getBroadcastMessageType(messageType);
			if (broadcastMessageType == null) {
				return null;
			}
			for (BroadcastMessageMessageField messageField : broadcastMessageType.getFields()) {
				String name = messageField.getName();
				String type = messageField.getType();
				int size = messageField.getSize();
				Object value;
				if (size > 0) {
					byte[] mData = new byte[size];
					buffer.get(mData);
					if (DataType.MOD96.equals(type)) {
						value = Mod96.decodeMod96(mData);
					} else if (DataType.STRING.equalsIgnoreCase(type)) {
						value = (new String (mData)).trim();
					} else {
						value = mData;
					}
				} else {
					value = null;
				}
				keyValueRepo.put(name, value);
			}
			hoseUdpBroadcastMessage =  mapper.convertValue(keyValueRepo, HoseUdpBroadcastMessage.class);
			return hoseUdpBroadcastMessage;
		} catch (Exception e) {
			log.error("Error! data: {}", ByteArrayU.toString(data));
			log.error("", e);
			return null;
		}
	}
}
