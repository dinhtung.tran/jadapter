package com.fss.jadapter.constant.definition.hose.udp.model;

import java.util.List;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class BroadcastMessageType {
	private String messageType;
	private List<BroadcastMessageMessageField> fields;
}
