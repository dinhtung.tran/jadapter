package com.fss.jadapter.constant.definition.hose.udp;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fss.jadapter.constant.StringValue.DataType;
import com.fss.jadapter.constant.definition.hose.udp.model.BroadcastMessageMessageField;
import com.fss.jadapter.constant.definition.hose.udp.model.BroadcastPackageStruct;
import com.fss.jadapter.data.model.hose.udp.HoseUdpBroadcastMessage;
import com.fss.jadapter.data.model.hose.udp.HoseUdpBroadcastPackage;
import com.fss.utilities.array.ByteArrayU;
import com.fss.utilities.converter.Mod96;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class HoseUdpBroadcastPackageFactory implements InitializingBean{
	
	@Autowired
	private HoseUdpDefinitionHolder hoseUdpDefinitionHolder;
	@Autowired
	private HoseUdpBroadcastMessageFactory hoseUdpBroadcastMessageFactory;
	private BroadcastPackageStruct broadcastPackageStruct;
	
	private ObjectMapper mapper = new ObjectMapper();
	
	public HoseUdpBroadcastPackage product(byte[] data) {
		try {
			HoseUdpBroadcastPackage hoseUdpBroadcastPackage;
			ByteBuffer buffer = ByteBuffer.wrap(data);
			Map<String, Object> keyValueRepo = new HashMap<>();
			List<HoseUdpBroadcastMessage> collBroadcastMessage = new ArrayList<>();
			for (BroadcastMessageMessageField messageField : broadcastPackageStruct.getStruct()) {
				int size = messageField.getSize();
				String name = messageField.getName();
				String type = messageField.getType();
				Object value = null;
				if (size > 0) {
					byte[] mData = new byte[size];
					buffer.get(mData);
					if (DataType.MOD96.equals(type)) {
						value = Mod96.decodeMod96(mData);
					} else if (DataType.STRING.equalsIgnoreCase(type)) {
						value = new String (mData);
					} else {
						value = mData;
					}
				} else {
					if (DataType.ARRAY.equals(type)) {
						byte sepUnit = (byte) messageField.getSeparatorUnit();
						byte[] mData = new byte[buffer.remaining()];
						buffer.get(mData);
						Collection<byte[]> collData = ByteArrayU.split(mData, sepUnit);
						collData.stream().filter((d) -> {
							if (d.length > 0)
								return true;
							else {
								log.warn("collData len <= 0! Skip!");
								return false;
							}
								
						}).forEach((d) -> {
							HoseUdpBroadcastMessage hoseUdpBroadcastMessage = hoseUdpBroadcastMessageFactory.product(d);
							if (hoseUdpBroadcastMessage != null)
								collBroadcastMessage.add(hoseUdpBroadcastMessage);
							else {
								log.warn("hoseUdpBroadcastMessage == null! Skip!");
								log.warn("Splited Data: {}", ByteArrayU.toString(d));
							}
						});
					} else {
						value = null;
						log.warn("value == null!!");
					}
				} 
				keyValueRepo.put(name, value);
			}
			hoseUdpBroadcastPackage = mapper.convertValue(keyValueRepo, HoseUdpBroadcastPackage.class);
			hoseUdpBroadcastPackage.setPackedContent(collBroadcastMessage);
			return hoseUdpBroadcastPackage;
		} catch (Exception e) {
			log.error("Error! data: {}", ByteArrayU.toString(data));
			log.error("", e);
			return null;
		}
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		broadcastPackageStruct = hoseUdpDefinitionHolder.getBroadcastPackageStruct();
	}
}
