package com.fss.jadapter.constant;

public class CamelRouteName {
	public static final String HOSE_UDP_ZEROMQ_TO_XML = "hose_udp_zeromq_to_xml";
	public static final String HOSE_UDP_ZEROMQ_LISTENER = "hose_udp_zeromq_listener";
	
	public static final String HNX_INFOGATE_FIX = "hnx_infogate_fix";
}
