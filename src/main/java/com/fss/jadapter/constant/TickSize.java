package com.fss.jadapter.constant;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public enum TickSize {
	HOSE(
			new long[] {0, 9999, 10},
			new long[] {10000, 49999, 50},
			new long[] {50000, Long.MAX_VALUE, 100}
		), 
	HNX(
			new long[] {0, Long.MAX_VALUE, 100}
			), 
	
	UPCOM(
			new long[] {0, Long.MAX_VALUE, 100}
			),
	FDS
	;
	
	TickSize(long[]...tick) {
		listTick = new ArrayList<>();
		Stream.of(tick).forEach((arr) -> {
			listTick.add(new Tick(arr));
		});
	}
	
	private final List<Tick> listTick;
	
	public int getTick(int price) {
		for (Tick e : listTick) {
			if (price >= e.from && price <= e.to) {
				return (int) e.tick;
			}
		}
		return (int) listTick.get(listTick.size() - 1).tick;
	}
	
	private class Tick {
		private Tick(long[] arr) {
			from = arr[0];
			to = arr[1];
			tick = arr[2];
		}
		
		private long from;
		private long to;
		private long tick;
	}
}
