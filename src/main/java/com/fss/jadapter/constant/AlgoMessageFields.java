package com.fss.jadapter.constant;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public final class AlgoMessageFields {
	public static final String TIME = "0";
	public static final String BUYPRICE3 = "1";
	public static final String BUYAMOUNT3 = "2";
	public static final String BUYPRICE2 = "3";
	public static final String BUYAMOUNT2 = "4";
	public static final String BUYPRICE1 = "5";
	public static final String BUYAMOUNT1 = "6";
	public static final String DIFF = "7";
	public static final String FINISHPRICE = "8";
	public static final String FINISHAMOUNT = "9";
	public static final String TOTALSHARE = "10";
	public static final String SELLPRICE1 = "11";
	public static final String SELLAMOUNT1 = "12";
	public static final String SELLPRICE2 = "13";
	public static final String SELLAMOUNT2 = "14";
	public static final String SELLPRICE3 = "15";
	public static final String SELLAMOUNT3 = "16";
	public static final String OPENPRICE = "17";
	public static final String HIGHEST = "18";
	public static final String LOWEST = "19";
	public static final String FGBUYQUANTITY = "20";
	public static final String FGSELLQUANTITY = "21";
	public static final String FILTER_1 = "22";
	public static final String FILTER_2 = "23";
	public static final String FILTER_3 = "24";
	public static final String FILTER_4 = "25";
	public static final String REMAINROOM = "26";
	public static final String TOTALROOM = "27";
	public static final String PRICE1 = "28";
	public static final String AMOUNT1 = "29";
	public static final String PRICE2 = "30";
	public static final String AMOUNT2 = "31";
	public static final String FLOORID = "32";
	public static final String TOTALVALUE = "33";
	public static final String TOTALBIDQUANTITY = "34";
	public static final String TOTALOFFERQUANTITY = "35";
	public static final String BIDCOUNT = "36";
	public static final String OFFERCOUNT = "37";
	public static final String AVERAGEVOLPERCENT = "38";
	public static final String DIFFRATE = "39";
	public static final String REMAINROOMPCT = "40";
	public static final String OPENINTEREST = "41";
	public static final String LASTTRADINGDATE = "42";
	public static final String DIFFVN30 = "43";
	public static final String AVERAGEPRICE = "44";
	public static final String BASIS = "45";
	public static final String BUYPRICE4 = "46";
	public static final String BUYAMOUNT4 = "47";
	public static final String BUYPRICE5 = "48";
	public static final String BUYAMOUNT5 = "49";
	public static final String BUYPRICE6 = "50";
	public static final String BUYAMOUNT6 = "51";
	public static final String BUYPRICE7 = "52";
	public static final String BUYAMOUNT7 = "53";
	public static final String BUYPRICE8 = "54";
	public static final String BUYAMOUNT8 = "55";
	public static final String BUYPRICE9 = "56";
	public static final String BUYAMOUNT9 = "57";
	public static final String BUYPRICE10 = "58";
	public static final String BUYAMOUNT10 = "59";
	public static final String SELLPRICE4 = "60";
	public static final String SELLAMOUNT4 = "61";
	public static final String SELLPRICE5 = "62";
	public static final String SELLAMOUNT5 = "63";
	public static final String SELLPRICE6 = "64";
	public static final String SELLAMOUNT6 = "65";
	public static final String SELLPRICE7 = "66";
	public static final String SELLAMOUNT7 = "67";
	public static final String SELLPRICE8 = "68";
	public static final String SELLAMOUNT8 = "69";
	public static final String SELLPRICE9 = "70";
	public static final String SELLAMOUNT9 = "71";
	public static final String SELLPRICE10 = "72";
	public static final String SELLAMOUNT10 = "73";
	public static final String BALANCEPRICE = "74";
	
	private static final Map<String, BigDecimal> DIVISION_MAPPER = new HashMap<>();
	
	public static final String[] STOCKINFOR_ALL_FIELDS = { 
			BUYPRICE3, BUYAMOUNT3, BUYPRICE2, BUYAMOUNT2, BUYPRICE1,
			BUYAMOUNT1, DIFF, FINISHPRICE, FINISHAMOUNT, TOTALSHARE, SELLPRICE1, SELLAMOUNT1, SELLPRICE2, SELLAMOUNT2,
			SELLPRICE3, SELLAMOUNT3, OPENPRICE, HIGHEST, LOWEST, FGBUYQUANTITY, FGSELLQUANTITY, FILTER_1, FILTER_2,
			FILTER_3, FILTER_4, REMAINROOM, TOTALROOM, PRICE1, AMOUNT1, PRICE2, AMOUNT2, FLOORID, TOTALVALUE,
			TOTALBIDQUANTITY, TOTALOFFERQUANTITY, BIDCOUNT, OFFERCOUNT, AVERAGEVOLPERCENT, DIFFRATE, REMAINROOMPCT,
			OPENINTEREST, LASTTRADINGDATE, DIFFVN30, AVERAGEPRICE, BASIS, BUYPRICE4, BUYAMOUNT4, BUYPRICE5, BUYAMOUNT5,
			BUYPRICE6, BUYAMOUNT6, BUYPRICE7, BUYAMOUNT7, BUYPRICE8, BUYAMOUNT8, BUYPRICE9, BUYAMOUNT9, BUYPRICE10,
			BUYAMOUNT10, SELLPRICE4, SELLAMOUNT4, SELLPRICE5, SELLAMOUNT5, SELLPRICE6, SELLAMOUNT6, SELLPRICE7,
			SELLAMOUNT7, SELLPRICE8, SELLAMOUNT8, SELLPRICE9, SELLAMOUNT9, SELLPRICE10, SELLAMOUNT10, BALANCEPRICE
	};
	
	public static final String[] TOP_10_SELL_PRICE_FIELDS = {
			"", SELLPRICE1, SELLPRICE2, SELLPRICE3, SELLPRICE4, SELLPRICE5, SELLPRICE6, SELLPRICE7, SELLPRICE8, SELLPRICE9, SELLPRICE10
	};
	
	public static final String[] TOP_10_BUY_PRICE_FIELDS = {
			"", BUYPRICE1, BUYPRICE2, BUYPRICE3, BUYPRICE4, BUYPRICE5, BUYPRICE6, BUYPRICE7, BUYPRICE8, BUYPRICE9, BUYPRICE10
	};
	
	public static final String[] TOP_10_SELL_VOLUME_FIELDS = {
			"", SELLAMOUNT1, SELLAMOUNT2, SELLAMOUNT3, SELLAMOUNT4, SELLAMOUNT5, SELLAMOUNT6, SELLAMOUNT7, SELLAMOUNT8, SELLAMOUNT9, SELLAMOUNT10
	};
	
	public static final String[] TOP_10_BUY_VOLUME_FIELDS = {
			"", BUYAMOUNT1, BUYAMOUNT2, BUYAMOUNT3, BUYAMOUNT4, BUYAMOUNT5, BUYAMOUNT6, BUYAMOUNT7, BUYAMOUNT8, BUYAMOUNT9, BUYAMOUNT10
	};
	
	
	
	static {
		Stream.Builder<String> thousandDivision = Stream.builder();
		Stream.Builder<String> tenDivision = Stream.builder();
		Stream.Builder<String> millionDivision = Stream.builder();
		
		//Division 1,000
		Stream.of(TOP_10_SELL_PRICE_FIELDS).forEach((s) -> {
			if (s == null || s.isEmpty())
				return;
			thousandDivision.add(s);
		});
		Stream.of(TOP_10_BUY_PRICE_FIELDS).forEach((s) -> {
			if (s == null || s.isEmpty())
				return;
			thousandDivision.add(s);
		});
		
		thousandDivision.add(DIFF);
		thousandDivision.add(FINISHPRICE);
		thousandDivision.add(OPENPRICE);
		thousandDivision.add(HIGHEST);
		thousandDivision.add(LOWEST);
		thousandDivision.add(PRICE1);
		thousandDivision.add(PRICE2);
		thousandDivision.add(AVERAGEPRICE);
		thousandDivision.add(BASIS);
		thousandDivision.add(BALANCEPRICE);
		
		//Division 10
		Stream.of(TOP_10_SELL_VOLUME_FIELDS).forEach((s) -> {
			if (s == null || s.isEmpty())
				return;
			tenDivision.add(s);
		});
		
		Stream.of(TOP_10_BUY_VOLUME_FIELDS).forEach((s) -> {
			if (s == null || s.isEmpty())
				return;
			tenDivision.add(s);
		});
		
		tenDivision.add(FINISHAMOUNT);
		tenDivision.add(TOTALSHARE);
		tenDivision.add(FGBUYQUANTITY);
		tenDivision.add(FGSELLQUANTITY);
//		tenDivision.add(REMAINROOM);
//		tenDivision.add(TOTALROOM);
		tenDivision.add(TOTALBIDQUANTITY);
		tenDivision.add(TOTALOFFERQUANTITY);
		
		//Division 1,000,000
		millionDivision.add(TOTALVALUE);
		
		//Process
		thousandDivision.build().forEach((s) -> DIVISION_MAPPER.put(s, BigDecimalConstant.THOUSAND));
		tenDivision.build().forEach((s) -> DIVISION_MAPPER.put(s, BigDecimalConstant.TEN));
		millionDivision.build().forEach((s) -> DIVISION_MAPPER.put(s, BigDecimalConstant.MILLION));
		log.info("Init DIVISION_MAPPER: {}", DIVISION_MAPPER);
	}
	
	public static final BigDecimal getDivision(String field) {
		BigDecimal d = null;
		if (field == null || field.isEmpty() || (d = DIVISION_MAPPER.get(field)) == null) 
			return BigDecimalConstant.ONE;
		else
			return DIVISION_MAPPER.get(field);
	}

	@Component
	public static final class Index {
		public static final String INDEX = "2";
		public static final String DIFF = "3";
		public static final String DIFFRATE = "4";
		public static final String TOTAL = "5";
		public static final String TOTALSHARE = "6";
		public static final String TOTALTRADE = "7";
		public static final String ADVANCES = "8";
		public static final String DECLINES = "9";
		public static final String NOCHANGE = "10";
		public static final String CEILING = "11";
		public static final String FLOOR = "12";
		public static final String NOTRADE = "13";
		public static final String PTTOTAL = "14";
		public static final String PTTOTALSHARE = "15";
		public static final String PTTOTALTRADE = "16";
		
		public static final String[] MARKETINFOR_ALL_FIELDS = { 
			INDEX, DIFF, DIFFRATE, TOTAL, TOTALSHARE, TOTALTRADE, ADVANCES, DECLINES, NOCHANGE, CEILING, FLOOR, NOTRADE, PTTOTAL, PTTOTALSHARE, PTTOTALTRADE
		};
		
		private static final Map<String, BigDecimal> DIVISION_MAPPER = new HashMap<>();
		
		static {
			Stream.Builder<String> thousandDivision = Stream.builder();
			Stream.Builder<String> tenDivision = Stream.builder();
			Stream.Builder<String> millionDivision = Stream.builder();
			Stream.Builder<String> billionDivision = Stream.builder();
						
			billionDivision.add(TOTAL);
			billionDivision.add(PTTOTAL);
			
			//Process
			thousandDivision.build().forEach((s) -> DIVISION_MAPPER.put(s, BigDecimalConstant.THOUSAND));
			tenDivision.build().forEach((s) -> DIVISION_MAPPER.put(s, BigDecimalConstant.TEN));
			millionDivision.build().forEach((s) -> DIVISION_MAPPER.put(s, BigDecimalConstant.MILLION));
			billionDivision.build().forEach((s) -> DIVISION_MAPPER.put(s, BigDecimalConstant.BILLION));
			log.info("Init Index DIVISION_MAPPER: {}", DIVISION_MAPPER);
		}
		
		public static final BigDecimal getDivision(String field) {
			BigDecimal d = null;
			if (field == null || field.isEmpty() || (d = DIVISION_MAPPER.get(field)) == null) 
				return BigDecimalConstant.ONE;
			else
				return DIVISION_MAPPER.get(field);
		}

	}
	
	
}
