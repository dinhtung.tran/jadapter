package com.fss.jadapter.constant;

public class StringValue {
	public static final class MessageClass {
		public static final String AUTO_T = "autot";
		public static final String BROADCAST = "broadcast";
	}
	
	public static class Hose {
//		public static class SystemControlCode {
//			public static final String ATO = "P";
//			public static final String ATC = "A";
//			public static final String OPEN = "O";
//			public static final String INTERMISSION = "I";
//			public static final String CROSS = "C";
//			
//			public static final String BEGIN_EOD_UPDATE = "G";
//			public static final String END_EOD_UPDATE = "J";
//			
//			public static final String END_RUNOFF_PERIOR = "K";	
//			
//		}
		
		public static class Udp {
			public static final String OUT_DATA_ENDPOINT_URI = "seda:hose.udp.source.out";
		}
		public static final int MSG_TYPE_LENGTH = 2;
		
				
	}
	
	public static class Hnx {
		public static final String OUT_DATA_ENDPOINT_URI = "seda:hnx.source.out";
	}
	
	
	public static class DataType {
		public static final String STRING = "String";
		public static final String MOD96 = "mod96";
		public static final String ARRAY = "array";
	}
	
}
