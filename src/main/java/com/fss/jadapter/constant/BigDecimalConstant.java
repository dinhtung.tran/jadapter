package com.fss.jadapter.constant;

import java.math.BigDecimal;

public final class BigDecimalConstant {
	public static final BigDecimal ONE = BigDecimal.ONE;
	public static final BigDecimal TEN = BigDecimal.valueOf(10L);
	public static final BigDecimal HUNDRED = BigDecimal.valueOf(100L);
	public static final BigDecimal THOUSAND = BigDecimal.valueOf(1000L);
	public static final BigDecimal MILLION = BigDecimal.valueOf(1000000L);
	public static final BigDecimal BILLION = BigDecimal.valueOf(1000000000L);
}
