package com.fss.jadapter.constant;

import java.lang.reflect.Field;
import java.math.BigDecimal;

import com.fss.jadapter.data.persistence.model.v2.Information;

public class ApplicationType {
	
	public static final Class<Long> LONG = Long.TYPE;
	public static final Class<Integer> INTERGER = Integer.TYPE;
	public static final Class<Boolean> BOOL = Boolean.TYPE;
	public static final Class<Double> DOUBLE = Double.TYPE;
	public static final Class<Float> FLOAT = Float.TYPE;
	public static final Class<String> STRING = String.class;
	public static final Class<BigDecimal> DECIMAL = BigDecimal.class;	
	
	public static final Class<Information> INFORMATION = Information.class;
	
	public static boolean isPrimitiveType(Field field) {
		Class clazz = field.getType();
		for (Class primitiveClazz : PRIMITIVE_TYPE) {
			if (clazz.isAssignableFrom(primitiveClazz) && primitiveClazz.isAssignableFrom(clazz)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isInformationType(Field field) {
		Class clazz = field.getType();
		return INFORMATION.isAssignableFrom(clazz);
	}
	
	private static final Class[] PRIMITIVE_TYPE = new Class[] {
			LONG, 
			INTERGER, 
			BOOL, 
			DOUBLE,
			FLOAT, 
			STRING, 
			DECIMAL			
	};
}
