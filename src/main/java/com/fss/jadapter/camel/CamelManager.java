package com.fss.jadapter.camel;

import java.util.Map.Entry;
import java.util.UUID;

import org.apache.camel.CamelContext;
import org.apache.camel.Endpoint;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.ValueHolder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.activemq.ActiveMQComponent;
import org.apache.camel.component.seda.SedaEndpoint;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class CamelManager implements InitializingBean{
	
	@Autowired
	private CamelContext camelContext;	
	private ProducerTemplate producerTemplate;
	
//	private static final List<RouteBuilder> BUILDER_REPO = new ArrayList<>();
	
		
	public void addNewRoute(RouteBuilder builder) throws Exception {
		camelContext.addRoutes(builder);
		log.info("addNewRoute: {}", builder);
	}
	
	public SedaEndpoint getSedaEndpoint(String name) {
		SedaEndpoint sedaEndpoint = new SedaEndpoint();
		sedaEndpoint.setEndpointUriIfNotSpecified(name);
		return sedaEndpoint;
	}

	public void sendBodyTo(String endpointUri, Object body) {
		producerTemplate.sendBody(endpointUri, body);
	}
	
//	public RouteBuilder buildRoute(String routeName, RouteBuilder builder) throws Exception {
//		camelContext.addRoutes(builder);
//		return builder;
//	}
	
	@Bean("signatureEndpointUri")
	@Scope("prototype")
	public String getSignatureEndpointUri() {
		String randomStr = UUID.randomUUID().toString();
		for (Entry<? extends ValueHolder<String>, Endpoint> entry : camelContext.getEndpointRegistry().entrySet()) {
			ValueHolder<String> k = entry.getKey();
			Endpoint v = entry.getValue();
			if (String.valueOf(k == null ? k : k.get()).contains(randomStr) || String.valueOf(v == null ? v : v.getEndpointBaseUri()).contains(randomStr)) {
				return getSignatureEndpointUri();
			} else {
				
			}
		};
		String uri = "seda:" + randomStr;
		return uri;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		camelContext.getEndpointRegistry();
		ActiveMQComponent activemqComp = new ActiveMQComponent();
		activemqComp.setBrokerURL("tcp://192.168.1.30:61616");
		activemqComp.setClientId("JAdapter");
		camelContext.addComponent("activemq", activemqComp);
		producerTemplate = camelContext.createProducerTemplate(Integer.MAX_VALUE);
	}
}
