package com.fss.jadapter.operation.entity;

import lombok.Data;

@Data(staticConstructor = "of")
public class ApplicationOperatorResponse  {
	private final String cmdCode;
	private String responseMessage;	
}
	