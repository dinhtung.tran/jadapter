package com.fss.jadapter.operation.handler;

import java.util.concurrent.Executors;

import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;
import com.fss.jadapter.operation.entity.ApplicationOperatorResponse;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ShutdownApplication {
	
	private static final long SHUTDOWN_AFTER_MILIS = 3000; 
	
	public ApplicationOperatorResponse command(String code, String...args) {
		ApplicationOperatorResponse res = doShutdown();
		return res;
	}

	private ApplicationOperatorResponse doShutdown() {
		log.info("doShutdown: prepared...");
		String notify = String.format("Application shutdown in %s second(s)!", SHUTDOWN_AFTER_MILIS);
		ApplicationOperatorResponse res = ApplicationOperatorResponse.of("shutdown");
		Executors.newSingleThreadExecutor().execute(() -> {
			try {
				log.info("doShutdown: notify: {}", notify);
				Thread.sleep(SHUTDOWN_AFTER_MILIS);
				log.info("doShutdown: System.exit: status {}", 0);
				System.exit(0);
			} catch (InterruptedException e) {
				log.error("", e);
			}
		});
		res.setResponseMessage(notify);
		return res;
	}
	
	@PreDestroy
	public void destroyBean() {
//		log.info("djt me bi xoa roi");
	}
}

