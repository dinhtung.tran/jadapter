package com.fss.jadapter.publisher;

public interface DataPublisher {
	public void send(String message);
	public void send(byte[] message);
}
