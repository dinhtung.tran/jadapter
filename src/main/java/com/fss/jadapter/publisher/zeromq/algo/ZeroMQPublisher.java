package com.fss.jadapter.publisher.zeromq.algo;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.zeromq.SocketType;
import org.zeromq.ZContext;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Socket;

import com.fss.jadapter.publisher.DataPublisher;

import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "ZeroMQPublisher")
@Component
public class ZeroMQPublisher implements InitializingBean, DisposableBean, DataPublisher {
	
	private static final SocketType SOCKET_TYPE = SocketType.PUB;
	
	private Socket socket;
	
	@Value("${zero.mq.pub.port}")
	private String port;
	@Value("${zero.mq.pub.protocol}")
	private String protocol;
	@Value("${zero.mq.pub.host}")
	private String host;
	@Value("${zero.mq.pub.topic}")
	private String topic;
	
	private ZContext zContext;
	
	private void init() {		
		log.info("Creating ZeroMQPublisher: {}", this);
		zContext = new ZContext();
		socket = zContext.createSocket(SOCKET_TYPE);
		String url = protocol + "://" + host + ":" + port;
		socket.bind(url);
		socket.setSndHWM(Integer.MAX_VALUE);
		log.info("ZeroMQPublisher has created: {}", url);
		log.info("ZeroMQPublisher socket SND_HWM: {}", socket.getSndHWM());
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		init();		
	}

	public void send(byte[] data) {
		send(new String(data, ZMQ.CHARSET));
	}

	public void send(String data) {
		send(topic, data);		
	}

	public synchronized void send(String topic, String data) {
		if (topic == null || topic.trim().isEmpty())
			send(data);
		else {
			String payload = topic + data;
			
			boolean rs = socket.send(payload, 0);
			
			if (log.isDebugEnabled())
				log.debug("Snt: {}", payload);
			
			if (!rs) {
				log.warn("payload doesn't send: payload: {}", payload);
			} 
		}		
	}

	public void send(String topic, byte[] data) {
		send(topic, new String(data, ZMQ.CHARSET));
	}
	
	@Override
	public void destroy() throws Exception {		
		socket.close();
		zContext.close();
	}
}
