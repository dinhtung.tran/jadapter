package com.fss.jadapter.api.algo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fss.jadapter.api.model.ResponseEntity;
import com.fss.jadapter.constant.PublishEntityType;
import com.fss.jadapter.data.process.persistence.minor.AlgoSubscribeMessageProcessor;
import com.fss.jadapter.data.repo.cache.CachedMemoryRepository;
import com.fss.jadapter.model.MultiMessagePublishObject;
import com.fss.jadapter.model.PublishObject;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@RestController
@CrossOrigin
@RequestMapping("/algo")
@Slf4j
public class AlgoSnapshotApiGateway {
	
	@Autowired
	private CachedMemoryRepository cachedMemoryRepository;
	@Autowired
	private AlgoSubscribeMessageProcessor algoSubscribeMessageProcessor;
	
	@GetMapping
	public HelloWorld helloWorld() {
		HelloWorld entity = new HelloWorld();
		entity.setDialog("Hello world");
		entity.setToday(new Date().toString());
		return entity;
	}
	
	@GetMapping("/snapshot/{entityType}")
	public ResponseEntity snapshot(@RequestParam MultiValueMap<String, String> requestParams, @PathVariable("entityType") String entityType) {
		ResponseEntity res;
		try {
			MultiValueMap<String, String> params = requestParams;
			if (entityType == null || entityType.isEmpty()) {
				res = ResponseEntity.error(-1, "Invalid url: snapshot/" + entityType);
			} else if (PublishEntityType.STOCK_INFOR.equalsIgnoreCase(entityType)) {
				res = getSnapshotStockInfor(params);
			} else if (PublishEntityType.MARKET_INFOR.equalsIgnoreCase(entityType)) {
				res = getSnapshotMarketInfor(params);
			} else if (PublishEntityType.TRANSLOG_INFOR.equalsIgnoreCase(entityType)) {
				res = getSnapshotTransLog(params);
			} else {
				res = ResponseEntity.error(-1, "Invalid url.: snapshot/" + entityType);
			}
		} catch (Exception e) {
			log.error("Error! ", e);
			res = ResponseEntity.error(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
			return res;
		}
		return res;
	}
	
	private ResponseEntity getSnapshotTransLog(MultiValueMap<String, String> params) {
		List<String> keys = params.get("keys");
		List<String> args = params.get("opts");
		
		List<MultiMessagePublishObject> resObject = new ArrayList<>();
				
		keys.forEach((key) -> {
			Stream.of(key.split(",")).forEach((s) -> {
				MultiMessagePublishObject transLogList = cachedMemoryRepository.getTransLogPublishMessage(s);
				resObject.add(transLogList);
//				algoSubscribeMessageProcessor.processChangedValuesCollection(
//						cachedMemoryRepository.getPublishTransLog(s).collect(Collectors.toList())
//						).forEach((o) -> {
//							transLogList.setUrl(o.getUrl());
//							transLogList.addMessage(o.getMsg());
//						});
			});
		});
				
		return ResponseEntity.ok(resObject);
	}

	private ResponseEntity getSnapshotMarketInfor(MultiValueMap<String, String> params) {
		List<String> keys = params.get("keys");
		List<String> args = params.get("opts");
		
		List<PublishObject> resObject = new ArrayList<>();
		
		keys.forEach((key) -> {
			Stream.of(key.split(",")).forEach((s) -> {
				cachedMemoryRepository.getPublishMarketInfor(s).forEach((cv) -> {
					algoSubscribeMessageProcessor.processChangedValuesCollectionForSnapshot(Collections.singletonList(cv)).forEach(resObject::add);
				});
			});
		});
				
		return ResponseEntity.ok(resObject);
	}

	private ResponseEntity getSnapshotStockInfor(MultiValueMap<String, String> params) {
		List<String> keys = params.get("keys");
		List<String> args = params.get("opts");
		
		List<PublishObject> resObject = new ArrayList<>();
		
		keys.forEach((key) -> {
			Stream.of(key.split(",")).forEach((s) -> {
				cachedMemoryRepository.getPublishStockInfor(s).forEach((cv) -> {
					algoSubscribeMessageProcessor.processChangedValuesCollectionForSnapshot(Collections.singletonList(cv)).forEach(resObject::add);
				});
			});
		});
				
		return ResponseEntity.ok(resObject);
	}

	@Data
	static class HelloWorld {
		private String dialog;
		private String today;
	}
	
}
