package com.fss.jadapter.api.operation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fss.jadapter.api.model.ResponseEntity;
import com.fss.jadapter.operation.handler.ShutdownApplication;

@RestController
@CrossOrigin
@RequestMapping("/app")
public class ApplicationOperation {
	
	@Autowired
	private ShutdownApplication shutdownApplication;
	
	@PostMapping("/shutdown")
	private ResponseEntity shutdown() {		
		return ResponseEntity.ok(shutdownApplication.command("shutdown"));
	} 
	
}
