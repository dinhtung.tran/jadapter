package com.fss.jadapter.api.model;

public class ResponseEntity {
	private int ec;
	private String em;
	private Object d;
	
	private ResponseEntity(int ec, String em, Object d) {
		super();
		this.ec = ec;
		this.em = em;
		this.d = d;
	}
	
	public int getEc() {
		return ec;
	}
	public String getEm() {
		return em;
	}
	public Object getD() {
		return d;
	}
	
	public static ResponseEntity ok() {
		return new ResponseEntity(0, null, null);
	}
	
	public static ResponseEntity ok(Object data) {
		ResponseEntity entity = ok();
		entity.d = data;
		return entity;
	}
	
	public static ResponseEntity error(int code) {
		return new ResponseEntity(code, null, null);
	}
	
	public static ResponseEntity error(int code, String message) {
		return new ResponseEntity(code, message, null);
	}
	
	public static class CommonResponse {
		private static ResponseEntity INVALID_URL;
	}
}
