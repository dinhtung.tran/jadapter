package com.fss.jadapter.config;

import java.io.IOException;
import java.io.InputStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import com.fss.jadapter.data.receiver.source.hose.zeromq.ZeroMQSubscriber;
import com.fss.jadapter.data.repo.cache.CachedMemoryRepository;

import quickfix.ConfigError;
import quickfix.DataDictionary;
import quickfix.fixig.MessageFactory;

@Configuration
public class ApplicationInitialConfig {
	
	@Autowired
	private ZeroMQSubscriber zeroMQSubscriber;

	@Bean
	public MessageFactory messageFactory() {
		return new MessageFactory();
	}

	@Bean
	public DataDictionary dataDictionary(
			@Value("${jadapter.definition.hnx.infogate.fix}") String hnxInfogateFixMessageDefineResource)
			throws IOException, ConfigError {
		InputStream is = new ClassPathResource(hnxInfogateFixMessageDefineResource).getInputStream();
		DataDictionary dataDictionary = new DataDictionary(is);
		return dataDictionary;

	}

	@Bean
	public ApplicationRunner initMarketInfor(CachedMemoryRepository cachedMemoryRepository) {
		return (args) -> {
			zeroMQSubscriber.start();
		};
	}
}
