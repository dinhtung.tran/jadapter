package com.fss.jadapter.config.redis;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.fss.jedis")
public class RedisConfig {
	
}
