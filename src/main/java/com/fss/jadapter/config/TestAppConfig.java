package com.fss.jadapter.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fss.jadapter.data.model.hose.udp.HoseUdpBroadcastMessage;
import com.fss.jadapter.data.model.hose.udp.HoseUdpBroadcastPackage;
import com.fss.jadapter.data.model.hose.udp.message.IU;
import com.fss.jadapter.data.model.hose.udp.message.SS;
import com.fss.jadapter.data.model.hose.udp.message.SU;
import com.fss.jadapter.data.model.hose.udp.message.TS;
import com.fss.jadapter.data.process.system.counter.Counter;
import com.fss.jadapter.data.receiver.ConvertedDataReceiver;
import com.fss.jadapter.data.receiver.LastEndpointReceiver;
import com.fss.jadapter.data.receiver.source.hnx.udp.HnxInfogateUdpListener;
import com.fss.jadapter.data.receiver.source.hose.zeromq.ZeroMQSubscriber;
import com.fss.jadapter.manager.AdapterConfigurationManager;
import com.fss.jadapter.manager.model.AdapterSource;
import com.fss.jadapter.manager.model.AdapterType;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class TestAppConfig {
	
	static Map<String, Integer> countMap = new HashMap<>();
	
	long lostMessage = 0;
	int lastSeqNum = -1;
	int firstSeqNum = -1;
	int sum = 0;
	
	@Autowired
	SourceRecieverConfig sourceRecieverConfig;
	
	@Bean
	public ConvertedDataReceiver countProcessor() {
		LastEndpointReceiver<HoseUdpBroadcastPackage> lastEndpointReceiver = new LastEndpointReceiver<HoseUdpBroadcastPackage>() {

			@Override
			public Class<HoseUdpBroadcastPackage> getOutClass() {
				return HoseUdpBroadcastPackage.class;
			}

			@Override
			public Class<HoseUdpBroadcastPackage> getInClass() {
				return HoseUdpBroadcastPackage.class;
			}

			@Override
			protected void consume(HoseUdpBroadcastPackage i) {
//				log.info("pck size: {}", i.getPackedContent().size());
				
				if (lastSeqNum > 0) {
					if (i.getSeqNum() - lastSeqNum > 1) {
						lostMessage += (i.getSeqNum() - lastSeqNum - 1);
						log.warn("lostMessage: {} firstSeqNum: {} ! lastSeqNum {} current {}", lostMessage, firstSeqNum, lastSeqNum, i.getSeqNum());
					} else if (i.getSeqNum() - lastSeqNum < 1){
						log.warn("smt wrong!!!!!!!!!!");
					} else {
						
					}
				} else {
					firstSeqNum = i.getSeqNum();
				}
				lastSeqNum = i.getSeqNum();
				
				for (HoseUdpBroadcastMessage hoseUdpBroadcastMessage : i.getPackedContent()) {
					String msgType = hoseUdpBroadcastMessage.getMessageType();
					
					if (countMap.containsKey(msgType)) {
						countMap.put(msgType, countMap.get(msgType) + 1);
					} else {
						countMap.put(msgType, 1);
					}
					if (hoseUdpBroadcastMessage instanceof TS) {
						log.info("TS message: {}", hoseUdpBroadcastMessage);
					}
//					if (hoseUdpBroadcastMessage instanceof SU) {
//						log.info("SU message: {}", hoseUdpBroadcastMessage);
//					}
				}
				
				
			}
		
		};
		return lastEndpointReceiver;
	}
	
	@Bean
	public ApplicationRunner test(Counter counter) {
		return (a) -> {			
			Executors.newSingleThreadExecutor().execute(() -> {
				while (true) {
					try {
						log.info("countResult: {}", counter.countResult());
						Thread.sleep(30000);
					} catch (Exception e) {
						log.error("", e);
					}
				}
			});
		};
	}
}
