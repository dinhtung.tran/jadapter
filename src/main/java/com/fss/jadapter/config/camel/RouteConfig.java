package com.fss.jadapter.config.camel;

import java.math.BigDecimal;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.fss.jadapter.camel.CamelManager;
import com.fss.jadapter.config.TestAppConfig;
import com.fss.jadapter.constant.CamelRouteName;
import com.fss.jadapter.constant.TimeConstant;
import com.fss.jadapter.data.process.TimerEndpointImpl;
import com.fss.jadapter.data.process.hose.file.basket.HosePrsBasketFileProcessor;
import com.fss.jadapter.data.process.hose.file.common.HosePrsDataPathProcessor;
import com.fss.jadapter.data.process.hose.file.index.HosePrsIndexFileProcessor;
import com.fss.jadapter.data.process.persistence.CachedMemoryProcessor;
import com.fss.jadapter.data.process.persistence.RedisPersistenceProcessor;
import com.fss.jadapter.data.process.persistence.RedisSummaryInforProcessor;
import com.fss.jadapter.data.process.persistence.minor.AlgoSubscribeMessageProcessor;
import com.fss.jadapter.data.process.persistence.minor.AlgoTransformMessageProcessor;
import com.fss.jadapter.data.process.system.SystemSignalProcessor;
import com.fss.jadapter.data.process.system.counter.CounterProcessor;
import com.fss.jadapter.data.receiver.SourceDataReceiver;
import com.fss.jadapter.data.receiver.hnx.infogate.HnxInfogateMessageReciever;
import com.fss.jadapter.data.receiver.hose.file.basket.HosePrsBasketFileReader;
import com.fss.jadapter.data.receiver.hose.file.common.HosePrsDataPathReader;
import com.fss.jadapter.data.receiver.hose.file.common.HosePrsSecurityReader;
import com.fss.jadapter.data.receiver.hose.file.index.HosePrsIndexFileReader;
import com.fss.jadapter.data.receiver.hose.udp.HoseUdpBroadcastPackageReceiver;
import com.fss.jadapter.data.receiver.hose.udp.xml.HoseUdpXmlConvertReceiver;
import com.fss.jadapter.manager.AdapterConfigurationManager;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class RouteConfig {
	@Autowired
	private TestAppConfig testAppConfig;
	@Autowired
	private CamelManager camelManager;
	@Autowired
	private AdapterConfigurationManager adapterConfigurationManager;
	@Autowired
	private HoseUdpBroadcastPackageReceiver hoseUdpBroadcastPackageReceiver;
	@Autowired
	private CachedMemoryProcessor cachedMemoryProcessor;
	@Autowired
	private AlgoTransformMessageProcessor algoTransformMessageProcessor;
	@Autowired
	private AlgoSubscribeMessageProcessor algoSubscribeMessageProcessor;
	@Autowired
	private HnxInfogateMessageReciever hnxInfogateMessageReciever;
	
	@Autowired
	private SourceDataReceiver zeroMQSubscriber;
	@Autowired
	private SourceDataReceiver hnxInfogateUdpListener;
	@Autowired
	private CounterProcessor counterProcessor;
	@Autowired
	private HosePrsDataPathReader hosePrsDataPathReader;
	@Autowired
	private HosePrsDataPathProcessor hosePrsDataPathProcessor;
	@Autowired
	private HosePrsIndexFileReader hosePrsIndexFileReader;
	@Autowired
	private HosePrsBasketFileReader hosePrsBasketFileReader;
	@Autowired
	private HosePrsSecurityReader hosePrsSecurityReader;
	@Autowired
	private HosePrsIndexFileProcessor hosePrsIndexFileProcessor;
	@Autowired
	private HosePrsBasketFileProcessor hosePrsBasketFileProcessor;
	@Autowired
	private RedisSummaryInforProcessor redisSummaryInforProcessor;
	@Autowired
	private RedisPersistenceProcessor redisPersistenceProcessor;
	@Autowired
	private SystemSignalProcessor systemSignalProcessor;
	
	@Bean
	public boolean buildRoute() throws Exception {		
		 camelManager.addNewRoute(new RouteBuilder() {
			@Override
			public void configure() throws Exception {
				from(zeroMQSubscriber.getOutDataEndpointUri()).routeId(CamelRouteName.HOSE_UDP_ZEROMQ_LISTENER)
				.process(hoseUdpBroadcastPackageReceiver)
				.process(counterProcessor)
				.process(cachedMemoryProcessor)
				.process(redisSummaryInforProcessor)
				.process(algoSubscribeMessageProcessor)
				
//				.process(hoseUdpXmlConvertReceiver())
//				.to("activemq:topic:prs.hose.227")
				;
			}
		});
		 
		camelManager.addNewRoute(new RouteBuilder() {
			@Override
			public void configure() throws Exception {
				from(hnxInfogateUdpListener.getOutDataEndpointUri()).routeId(CamelRouteName.HNX_INFOGATE_FIX)
				.process(hnxInfogateMessageReciever)
				.process(counterProcessor)
				.process(cachedMemoryProcessor)
				.process(redisSummaryInforProcessor)
				.process(algoSubscribeMessageProcessor)
				;

			}
		});
			 
		return true;
	}
	
	@Bean
	public boolean hoseIndexFileReaderRoute(@Value("${hose.prs.file.index.interval}") int hoseIndexFileInterval) throws Exception {
		camelManager.addNewRoute(new RouteBuilder() {
			@Override
			public void configure() throws Exception {
				TimerEndpointImpl hosePrsFileTimer = newTimerEndpoint();
				hosePrsFileTimer.setName("hoseIndexFileReaderRoute");
				BigDecimal interval = BigDecimal.valueOf(TimeConstant.ONE_SECOND * hoseIndexFileInterval);
				hosePrsFileTimer.setPeriodInMilis(interval.longValue());
				from(hosePrsFileTimer.getTimerEndpoint()).routeId("hoseIndexFileReaderRouteId")
				.process(hosePrsIndexFileReader)
				.process(hosePrsIndexFileProcessor)
				.process(redisSummaryInforProcessor)
				.process(algoSubscribeMessageProcessor)
				;
			}
		});

		return false;
	}
	
	@Bean
	public boolean hoseBasketFileReaderRoute() throws Exception {
		camelManager.addNewRoute(new RouteBuilder() {
			@Override
			public void configure() throws Exception {
				TimerEndpointImpl hoseBasketFileFileTimer = newTimerEndpoint();
				hoseBasketFileFileTimer.setName("hoseBasketFileReaderRoute");
				hoseBasketFileFileTimer.setPeriodInMilis(TimeConstant.ONE_MINUTE * 15);
				from(hoseBasketFileFileTimer.getTimerEndpoint()).routeId("hoseBasketFileReaderRoute")
				.process(hosePrsBasketFileReader)
				.process(hosePrsBasketFileProcessor)
				;
			}
		});

		return false;
	}
	
//	@Bean
	public boolean hoseSecurityFileReaderRoute() throws Exception {
		camelManager.addNewRoute(new RouteBuilder() {
			@Override
			public void configure() throws Exception {
				TimerEndpointImpl hosePrsFileTimer = newTimerEndpoint();
				hosePrsFileTimer.setName("hoseSecurityFileReaderRoute");
				hosePrsFileTimer.setPeriodInMilis(TimeConstant.ONE_MINUTE * 1);
				from(hosePrsFileTimer.getTimerEndpoint()).routeId("hoseSecurityFileReaderRoute")
				.process(hosePrsSecurityReader)
				.process(hosePrsDataPathProcessor)
				;
			}
		});

		return false;
	}
	
	@Bean
	public boolean dataPathReaderRoute() throws Exception {
		camelManager.addNewRoute(new RouteBuilder() {
			@Override
			public void configure() throws Exception {
				TimerEndpointImpl hosePrsFileTimer = newTimerEndpoint();
				hosePrsFileTimer.setName("dataPathReaderRoute");
				hosePrsFileTimer.setPeriodInMilis(TimeConstant.ONE_MINUTE/1);
				from(hosePrsFileTimer.getTimerEndpoint()).routeId("dataPathReaderRouteId")
				.process(hosePrsDataPathReader)
				.process(hosePrsDataPathProcessor)
				;
			}
		});
		return false;
	}
	
	@Bean
	public boolean redisPersistenRoute() throws Exception {
		camelManager.addNewRoute(new RouteBuilder() {
			@Override
			public void configure() throws Exception {
				TimerEndpointImpl redisPersistenceTimer = newTimerEndpoint();
				redisPersistenceTimer.setName("redisPersistenceTimer");
				redisPersistenceTimer.setPeriodInMilis(TimeConstant.ONE_SECOND / 10);
				from(redisPersistenceTimer.getTimerEndpoint()).routeId("redisPersistenRoute")
				.process(redisPersistenceProcessor)
				;
			}
		});
		return false;
	}
	
	@Bean
	public boolean systemSignalRoute() throws Exception {
		camelManager.addNewRoute(new RouteBuilder() {
			@Override
			public void configure() throws Exception {
				TimerEndpointImpl systemSignalTimer = newTimerEndpoint();
				systemSignalTimer.setName("systemSignalTimer");
				systemSignalTimer.setPeriodInMilis(TimeConstant.ONE_SECOND * 1);
				from(systemSignalTimer.getTimerEndpoint()).routeId("systemSignalRoute")
				.process(systemSignalProcessor)
				.process(redisSummaryInforProcessor)
				.process(algoSubscribeMessageProcessor)
				;
			}
		});
		return false;
	}
	
	@Scope("prototype")
	@Bean
	public static TimerEndpointImpl newTimerEndpoint() {
		TimerEndpointImpl hosePrsFileTimer = new TimerEndpointImpl();
		return hosePrsFileTimer;
	}

//	@Bean
	public HoseUdpXmlConvertReceiver hoseUdpXmlConvertReceiver() {
		return new HoseUdpXmlConvertReceiver();		
	}


}
