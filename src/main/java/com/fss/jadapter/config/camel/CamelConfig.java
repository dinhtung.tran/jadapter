package com.fss.jadapter.config.camel;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.component.seda.SedaComponent;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.impl.engine.DefaultShutdownStrategy;
import org.apache.camel.spi.ShutdownStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fss.jadapter.camel.CamelManager;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class CamelConfig {
	
	@Autowired
	private CamelManager camelManager;
		
	@Bean
	public CamelContext camelContext() {
		CamelContext camelContext = new DefaultCamelContext();
		SedaComponent sedaComp = new SedaComponent();
		sedaComp.setQueueSize(Integer.MAX_VALUE);
		camelContext.addComponent("seda", sedaComp);
		camelContext.getGlobalOptions().put(Exchange.MAXIMUM_CACHE_POOL_SIZE, String.valueOf(Integer.MAX_VALUE));
				
		camelContext.start();
		return camelContext;
	}
	
	@Bean 
	public ShutdownStrategy shutdownStrategy() {
		ShutdownStrategy shutdownStrategy = new DefaultShutdownStrategy(camelContext());
		shutdownStrategy.setTimeout(60);
		return shutdownStrategy;
	}	
	
	@Bean
//	@Order()
	public ApplicationRunner startContext() {
		return (args) -> {
			
		};
	}
}
