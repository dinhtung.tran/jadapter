package com.fss.jadapter.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fss.jadapter.data.receiver.source.hose.zeromq.ZeroMQSubscriber;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class SourceRecieverConfig {
	
	@Autowired
	ApplicationContext applicationContext;
	@Value("${zero.mq.host}")
	String host;
	@Value("${zero.mq.port}")
	int port;
	@Value("${zero.mq.topic}")
	String topic;
	@Value("${zero.mq.protocol}")
	String protocol;
	
	@Bean
	public ZeroMQSubscriber zeroMQSubscriber() {
			ZeroMQSubscriber zeroMQSubscriber = ZeroMQSubscriber.of();
			zeroMQSubscriber.setHost(host);
			zeroMQSubscriber.setPort(port);
			zeroMQSubscriber.setProtocol(protocol);
			zeroMQSubscriber.setTopic(topic);
			return zeroMQSubscriber;
	}	
}
