package com.fss.jadapter.converter.hose.udp.xml.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;


@JacksonXmlRootElement(localName = "hose-message")
@RequiredArgsConstructor(staticName = "of")
public class HoseUdpXmlMessage {
	@NonNull
	@Getter
	private String group;
	@NonNull
	@Getter
	private String type;
	@JacksonXmlProperty(localName = "item")
	@JacksonXmlElementWrapper(localName = "items")
	@Getter
	private final List<HoseUdpXmlItem> items = new ArrayList<>();
}
