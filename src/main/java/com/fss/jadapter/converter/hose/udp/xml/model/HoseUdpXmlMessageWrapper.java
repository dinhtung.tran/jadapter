package com.fss.jadapter.converter.hose.udp.xml.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "hose-messages")
public class HoseUdpXmlMessageWrapper implements HoseUdpXmlPackage {

	@JacksonXmlProperty(localName = "hose-message")
	@JacksonXmlElementWrapper(useWrapping = false)
	private List<HoseUdpXmlMessage> hoseMessageList = new ArrayList<>();
	
	@Override
	public void addMessage(HoseUdpXmlMessage hoseMessage) {
		hoseMessageList.add(hoseMessage);
	}

	@Override
	@JsonIgnore
	public Collection<HoseUdpXmlMessage> getMessages() {
		return Collections.unmodifiableCollection(hoseMessageList);
	}
}
