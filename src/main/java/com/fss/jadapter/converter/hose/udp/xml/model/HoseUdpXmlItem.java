package com.fss.jadapter.converter.hose.udp.xml.model;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(staticName = "of")
@JacksonXmlRootElement(localName = "item")
public class HoseUdpXmlItem {
	@JacksonXmlProperty(isAttribute = true, localName = "name")
	private final String name;
	@JacksonXmlText
	private final String value;
	
//	public static final HoseUdpXmlItem newItem(String name, String value) {
//		HoseUdpXmlItem item = new HoseUdpXmlItem();
//		item.name = name;
//		item.value = value;
//		return item;
//	}
}
