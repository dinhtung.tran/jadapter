package com.fss.jadapter.converter.hose.udp.xml.model;

import java.util.Collection;

public interface HoseUdpXmlPackage {
	
	public static HoseUdpXmlPackage newInstance() {
		return new HoseUdpXmlMessageWrapper();
	}
	
	public void addMessage(HoseUdpXmlMessage hoseMessage);
	public Collection<HoseUdpXmlMessage> getMessages();
}
