package com.fss.jadapter.converter.hose.udp.xml;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fss.jadapter.constant.definition.hose.udp.HoseUdpDefinitionHolder;
import com.fss.jadapter.constant.definition.hose.udp.xml.model.MessageXmlFormatFieldMapper;
import com.fss.jadapter.constant.definition.hose.udp.xml.model.MessageXmlFormatHolder;
import com.fss.jadapter.converter.Converter;
import com.fss.jadapter.converter.hose.udp.xml.model.HoseUdpXmlItem;
import com.fss.jadapter.converter.hose.udp.xml.model.HoseUdpXmlMessage;
import com.fss.jadapter.data.model.hose.udp.HoseUdpBroadcastMessage;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class HoseUdpXmlMessageConverter implements InitializingBean, Converter<HoseUdpBroadcastMessage, HoseUdpXmlMessage>{
	
	private static final String HOSE_UDP_XML_GROUP = "PRS";
	
	@Autowired
	private HoseUdpDefinitionHolder hoseUdpDefinitionHolder;
	private Map<String, FieldMap> fieldMap = new HashMap<>();

	@Override
	public HoseUdpXmlMessage convert(HoseUdpBroadcastMessage input) {
		String type = input.getClass().getSimpleName();
		HoseUdpXmlMessage hoseUdpXmlMessage = HoseUdpXmlMessage.of(HOSE_UDP_XML_GROUP, type);
		fillData(input, hoseUdpXmlMessage);
		
		return hoseUdpXmlMessage;
	}

	private void fillData(HoseUdpBroadcastMessage input, HoseUdpXmlMessage hoseUdpXmlMessage) {
		List<HoseUdpXmlItem> itemList = hoseUdpXmlMessage.getItems();
		MessageXmlFormatHolder messageXmlFormatHolder = hoseUdpDefinitionHolder.getMessageXmlFormatHolder(hoseUdpXmlMessage.getType());
//		log.info("HoseUdpBroadcastMessage: {}", input);
		for (MessageXmlFormatFieldMapper messageXmlFormatFieldMapper : messageXmlFormatHolder.getFields()) {
			String attributeName = messageXmlFormatFieldMapper.getConvertedFieldName();
			String attributeValue = getValue(messageXmlFormatFieldMapper.getSourceFieldName(), input);
			if (attributeValue != null)
				itemList.add(HoseUdpXmlItem.of(attributeName, attributeValue));
		};
	}

	private String getValue(String sourceFieldName, HoseUdpBroadcastMessage input) {
		Field field = getFieldMap(input.getClass()).getField(sourceFieldName);
		String rs = null;
		if (field != null)
			try {
				field.setAccessible(true);
				rs = String.valueOf(field.get(input));
			} catch (IllegalArgumentException | IllegalAccessException e) {
				log.error("sourceFieldName: {} | input: {}", sourceFieldName, input);
				log.error("", e);
			}
//		log.info("sourceFieldName: {} | rs: {}", sourceFieldName, rs);
		return rs;
	}
	
	private FieldMap getFieldMap(Class<? extends HoseUdpBroadcastMessage> clazz) {
		String messageType = clazz.getSimpleName();
		if (!fieldMap.containsKey(messageType)) {
			FieldMap fieldMapInstance = FieldMap.of(clazz);
			fieldMap.put(messageType, fieldMapInstance);
		}
		return fieldMap.get(messageType);
	}


	@Override
	public void afterPropertiesSet() throws Exception {
		
	}
	
	@RequiredArgsConstructor(staticName = "of")
	private static class FieldMap {
		private Map<String, Field> repo;
		@NonNull
		private Class<?> clazz;
		
		public Field getField(String fieldName) {
			if (repo == null) {
				init();
			}
			return repo.get(fieldName);
		}

		private void init() {
			repo = new HashMap<>();
			for (Field field : clazz.getDeclaredFields()) {
				repo.put(field.getName(), field);
			}
			log.info("FieldMap.init() done! repo: {}", repo);
		}		
	}

}
