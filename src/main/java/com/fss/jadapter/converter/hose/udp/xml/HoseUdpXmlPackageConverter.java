package com.fss.jadapter.converter.hose.udp.xml;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fss.jadapter.converter.Converter;
import com.fss.jadapter.converter.hose.udp.xml.model.HoseUdpXmlMessage;
import com.fss.jadapter.converter.hose.udp.xml.model.HoseUdpXmlPackage;
import com.fss.jadapter.data.model.hose.udp.HoseUdpBroadcastMessage;
import com.fss.jadapter.data.model.hose.udp.HoseUdpBroadcastPackage;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class HoseUdpXmlPackageConverter implements InitializingBean, Converter<HoseUdpBroadcastPackage, HoseUdpXmlPackage>{	
	
	@Autowired
	@Qualifier("hoseUdpXmlMessageConverter")
	Converter<HoseUdpBroadcastMessage, HoseUdpXmlMessage> hoseUdpXmlMessageConverter;
	
	@Override
	public HoseUdpXmlPackage convert(HoseUdpBroadcastPackage input) {
		HoseUdpXmlPackage output = HoseUdpXmlPackage.newInstance();
		for (HoseUdpBroadcastMessage hoseUdpBroadcastMessage : input.getPackedContent()) {
			HoseUdpXmlMessage hoseUdpXmlMessage = hoseUdpXmlMessageConverter.convert(hoseUdpBroadcastMessage);
			if (hoseUdpXmlMessage != null) {
				output.addMessage(hoseUdpXmlMessage);
			} else {
				log.warn("hoseUdpXmlMessage is null! hoseUdpBroadcastMessage: {}", hoseUdpBroadcastMessage);
			}
		}
		return output;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		
	}


}
