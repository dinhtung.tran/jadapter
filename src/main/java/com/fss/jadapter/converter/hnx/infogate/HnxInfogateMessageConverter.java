package com.fss.jadapter.converter.hnx.infogate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fss.jadapter.converter.Converter;
import com.fss.jadapter.data.model.hnx.infogate.HnxInfogateMessage;

import lombok.extern.slf4j.Slf4j;
import quickfix.DataDictionary;
import quickfix.FieldNotFound;
import quickfix.Message;

@Component
@Slf4j
public class HnxInfogateMessageConverter implements InitializingBean, Converter<String, HnxInfogateMessage> {
	@Autowired
	private Converter<String, Message> fixMessageConverter;
	@Autowired
	private DataDictionary dataDictionary;
	
	private ObjectMapper mapper = new ObjectMapper();
	
	@Override
	public HnxInfogateMessage convert(String input) {
		Map map = convertToMap(input);
		HnxInfogateMessage message; 
		try {
			message = mapper.convertValue(map, HnxInfogateMessage.class);
		} catch (IllegalArgumentException e) {
			try {
				log.warn("Unsupported msgType, data: {}", mapper.writeValueAsString(map));
				log.warn("e: ", e);
			} catch (JsonProcessingException e1) {
				log.warn("", e);
			}
			message = null;
		}
		return message;
	}

	private Map<String, Object> convertToMap(String input) {
		Message message = fixMessageConverter.convert(input);
		
		Map<String, Object> map = new HashMap<>();
		
		message.getHeader().iterator().forEachRemaining(field -> {
			String fieldName = getFieldName(field.getField());
			if (fieldName != null && !fieldName.isEmpty()) {
				map.put(fieldName, field.getObject());
			} else {
				log.warn("field tag is not declared! tag: {}", field.getField());
			}
		});
		
		message.iterator().forEachRemaining(field -> {
			String fieldName = getFieldName(field.getField());
			if (fieldName != null && !fieldName.isEmpty()) {
				map.put(fieldName, field.getObject());
			} else {
				log.warn("field tag is not declared! tag: {}", field.getField());
			}
		});
		
		final Message oM = message;
		message.groupKeyIterator().forEachRemaining(field -> {
			List<Map> list = new ArrayList<>();
			try {
				for (int i = 1; i <= oM.getInt(field); i++) {
					Map<String, Object> submap = new HashMap<>();
					oM.getGroup(i, field).iterator().forEachRemaining(f -> {
						String fieldName = getFieldName(f.getField());
						if (fieldName != null && !fieldName.isEmpty()) {
							submap.put(fieldName, f.getObject());
						} else {
							log.warn("field tag is not declared! tag: {}", f.getField());
						}
					});
					list.add(submap);
				}
			} catch (FieldNotFound e) {
				log.error("input: " + input, e);
			}
			String fieldName = getFieldName(field);
			if (fieldName != null && !fieldName.isEmpty()) {
				map.put(fieldName, list);
			} else {
				log.warn("field tag is not declared! tag: {}", field);
			}
		});
		
		return map;
	}

	private String getFieldName(int field) {
		return dataDictionary.getFieldName(field);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		
	}
	
	

}
