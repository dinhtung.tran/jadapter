package com.fss.jadapter.converter.hnx.infogate;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fss.jadapter.converter.Converter;

import lombok.extern.slf4j.Slf4j;
import quickfix.DataDictionary;
import quickfix.Message;
import quickfix.MessageUtils;
import quickfix.fixig.MessageFactory;

@Slf4j
@Component
public class FixMessageConverter implements InitializingBean, Converter<String, Message> {
		
	@Autowired
	private MessageFactory messageFactory;
	
	@Autowired
	private DataDictionary dataDictionary;

	@Override
	public Message convert(String input) {
		return getFixMessageObject(input);
	}
	
	private Message getFixMessageObject(String strMsg) {
		Message message = null;
		try {
			strMsg += checkSumField(strMsg);
			message = MessageUtils.parse(messageFactory, dataDictionary, strMsg);
		} catch (Exception e) {
			log.error("strMsg: " + strMsg, e);
		}
		return message;
	}

	private String checkSumField(String strMsg) {
		int sum = MessageUtils.checksum(strMsg);
		return "10=" + sum + "\1";
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		
	}

}
