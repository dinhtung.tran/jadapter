package com.fss.jadapter.converter;

public interface Converter<I, O> {
	O convert(I input);
}
