package com.fss.jadapter.manager;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fss.jadapter.camel.CamelManager;
import com.fss.jadapter.manager.model.AdapterSource;
import com.fss.jadapter.manager.model.AdapterType;

@Component
public class AdapterConfigurationManager implements InitializingBean {
	
	@Autowired
	private CamelManager camelManager;
	
	private static final Map<AdapterType, AdapterSource> ADAPTER_SOURCE_MAPPER = new HashMap<>(); 
	
	public AdapterSource getAdapterSource(AdapterType adapterType) {
		return ADAPTER_SOURCE_MAPPER.get(adapterType);
	}
	
	public void setAdapterSource(AdapterType adapterType, AdapterSource adapterSource) {
		ADAPTER_SOURCE_MAPPER.put(adapterType, adapterSource);
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		if (ADAPTER_SOURCE_MAPPER.isEmpty()) {
			ADAPTER_SOURCE_MAPPER.put(AdapterType.HNX, null);
			ADAPTER_SOURCE_MAPPER.put(AdapterType.HOSE, null);
		}
	}
	
}
