package com.fss.jadapter.manager.model;

import com.fss.jadapter.constant.StringValue;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum AdapterSource {
	HOSE_UDP_ZERO_MQ(AdapterType.HOSE, StringValue.Hose.Udp.OUT_DATA_ENDPOINT_URI),
	HOSE_UDP_GATEWAY(AdapterType.HOSE, ""),
	HOSE_FILE(AdapterType.HOSE, ""),
	HNX_INFOGATE_FILE(AdapterType.HNX, StringValue.Hnx.OUT_DATA_ENDPOINT_URI),
	HNX_INFOGATE_DB(AdapterType.HNX, ""),
//	HNX_INFOGATE_TCP(AdapterType.HNX, StringValue.Hnx.OUT_DATA_ENDPOINT_URI),
	;
	@NonNull
	private AdapterType adapter;
	@NonNull
	private String dataEndpointUri;
}
