package com.fss.jadapter.model;

public enum ActiveStatus {
	STARTING,
	STARTED,
	STOPPING,
	STOPED,
	PAUSED,
	DESTROYED;
}
