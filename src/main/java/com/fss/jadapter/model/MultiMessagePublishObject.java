package com.fss.jadapter.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.experimental.NonFinal;

@Getter
@RequiredArgsConstructor(staticName = "of")
public class MultiMessagePublishObject {
	
	@NonNull
	private String url;
	private List<String> msg = new ArrayList<>();
	
	public void addMessage(String message) {
		msg.add(message);
	}
}
