package com.fss.jadapter.model;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;


@Getter
@Builder
public class PublishObject {
	private String url;
	private String msg;
	private String ref;
}
