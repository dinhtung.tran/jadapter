package com.fss.jadapter.dao.algo;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Stream;

import javax.sql.DataSource;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fss.jadapter.constant.BigDecimalConstant;
import com.fss.jadapter.constant.TimeConstant;
import com.fss.jadapter.data.persistence.model.v2.security.StaticInfor;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class StockBoardDataAccess implements InitializingBean {
	@Value("${jadpater.algo.datasource.sqlserver.url}")
	private String url;
	@Value("${jadpater.algo.datasource.sqlserver.user}")
	private String user;
	@Value("${jadpater.algo.datasource.sqlserver.pwd}")
	private String pwd;
	@Value("${jadpater.algo.datasource.sqlserver.use}")
	private boolean isUse;
	@Value("${jadpater.algo.datasource.sqlserver.pool.size}")
	private int poolSize;
	private HikariDataSource dataSource;
	
	private static final Map<String, StaticInfor> CACHED_STATIC_INFOR = new HashMap<>();
	
	public void getPriorIndex(String indexName, Consumer<BigDecimal> callback) {
		if (!isUse) {
			log.info("getPriorIndex: isUse: {}", isUse);
			return;
		}
		log.info("getPriorIndex: start!");
		Connection conn = null;
		try {
			conn = getConnection();
			ResultSet rs = executeQuery(SqlStatement.GET_INDEX_REFS, conn, indexName);
			BigDecimal calculatedIndex = null;
			while (rs != null && rs.next()) {
				calculatedIndex = rs.getBigDecimal(1);
				log.info("getPriorIndex: indexName {} RefValue {}!", indexName, calculatedIndex);
			}
			if (callback != null && calculatedIndex != null) {
				callback.accept(calculatedIndex);
			}
		} catch (SQLException e) {
			log.error("indexName: {}", indexName);
			log.error("", e);
		} catch (Exception e) {
			log.error("indexName: {}", indexName);
			log.error("", e);
		} finally {
			if (conn != null)
				closeConnection(conn);
		}
	}
	
	public void calculatedIndex(String indexName, Consumer<BigDecimal> callback) {
		if (!isUse) {
			log.info("calculatedIndex: isUse: {}", isUse);
			return;
		}
		log.info("calculatedIndex: start!");
		Connection conn = null;
		try {
			int maxRetry = 3;
			int retryCount = 0;
			conn = getConnection();
			ResultSet rs = executeQuery(SqlStatement.CALCULATE_INDEX, conn, indexName);
			BigDecimal calculatedIndex = BigDecimal.ZERO;
			while (calculatedIndex.compareTo(BigDecimal.ZERO) == 0 && retryCount < maxRetry) {
				while (rs != null && rs.next()) {
					calculatedIndex = rs.getBigDecimal(1);
					log.info("calculatedIndex: indexName {} calculatedIndex {} retryCount {}!", indexName, calculatedIndex, retryCount);
				}
				retryCount++;
			}
			if (callback != null && calculatedIndex != null) {
				callback.accept(calculatedIndex);
			}
		} catch (SQLException e) {
			log.error("indexName: {}", indexName);
			log.error("", e);
		} catch (Exception e) {
			log.error("indexName: {}", indexName);
			log.error("", e);
		} finally {
			if (conn != null)
				closeConnection(conn);
		}
	}

	public void refreshStaticInfor() {
		if (!isUse) {
			log.info("refreshStaticInfor: isUse: {}", isUse);
			return;
		}
		log.info("refreshStaticInfor: start!");
		Connection conn = null;
		try {
			conn = getConnection();
			ResultSet rs = executeQuery(SqlStatement.GET_REFS, conn);
			while (rs != null && rs.next()) {
				String symbol = rs.getString("CompanyID");
				BigDecimal ceilingPriceFormarted = rs.getBigDecimal("CeilingPrice");
				BigDecimal floorPriceFormarted = rs.getBigDecimal("FloorPrice");
				BigDecimal refPriceFormarted = rs.getBigDecimal("RefPrice");
				String fullName = rs.getString("Name");
				String benefit = rs.getString("Benefit");
				String meeting = rs.getString("Meeting");
				String split = rs.getString("Split");
				String isMargin = rs.getString("IsMargin");
				
				StaticInfor staticInfor = StaticInfor.of();
				staticInfor.setBasicPrice(refPriceFormarted.multiply(BigDecimalConstant.THOUSAND).intValue());
				staticInfor.setCeilingPrice(ceilingPriceFormarted.multiply(BigDecimalConstant.THOUSAND).intValue());
				staticInfor.setFloorPrice(floorPriceFormarted.multiply(BigDecimalConstant.THOUSAND).intValue());
				staticInfor.setFullName(fullName);
				staticInfor.setBenefit(benefit);
				staticInfor.setMeeting(meeting);
				staticInfor.setSplit(split);
				staticInfor.setIsMargin(isMargin);
				
				CACHED_STATIC_INFOR.put(symbol, staticInfor);
				log.info(
						"getAllStaticInfo: put new infor: symbol {} fl {} ce {} rf {} name {} benefit {} meeting {} split {} isMargin {}!",
						symbol, floorPriceFormarted, ceilingPriceFormarted, refPriceFormarted, fullName, benefit,
						meeting, split, isMargin);
			}
		} catch (SQLException e) {
			log.error("", e);
		} finally {
			
			if (conn != null) {
				closeConnection(conn);

			}
		}
	}

	public StaticInfor getStaticInfor(String symbol) {
		if (!isUse)
			return null;
		return CACHED_STATIC_INFOR.get(symbol);
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		initDataSource();
		test();
		refreshStaticInfor();
	}
	
	public void test() {
		if (!isUse) {
			log.info("test: isUse: {}", isUse);
			return;
		}
		try {
			Connection conn = getConnection();
			String sql = "dbo.getrefs";
			ResultSet rs = conn.prepareCall(sql).executeQuery();
			String json = resultSetToJson(rs);
			log.info("resultSetToJson: {}", json);
			closeConnection(conn);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private String resultSetToJson(ResultSet rs) throws SQLException, JsonProcessingException {
		List<Map> list = new ArrayList<>();
		while (rs.next()) {
			Map<String, Object> objMap = new LinkedHashMap<>();
			getAllFields(rs).forEach((f) -> {
				try {
					objMap.put(f, rs.getString(f));
				} catch (SQLException e) {
					e.printStackTrace();
				}
			});
			list.add(objMap);
		}
		return mapper.writeValueAsString(list);
	}
	
	ObjectMapper mapper = new ObjectMapper();

	private Stream<String> getAllFields(ResultSet rs) throws SQLException {
		Stream.Builder<String> b = Stream.builder();
		int colCount = rs.getMetaData().getColumnCount();
		for (int i = 1; i <= colCount; i++)
			b.add(rs.getMetaData().getColumnName(i));
		return b.build();
		
	}
	
	private ResultSet executeQuery(String sql, Connection conn, String...params) {
		try {
			PreparedStatement ps = conn.prepareStatement(sql);
			if (params != null)
				for (int i = 1; i <= params.length; i++) {
					ps.setString(i, params[i-1]);
				}
			return ps.executeQuery();
		} catch (SQLException e) {
			log.error("Error! Sql: {}", sql);
			log.error("", e);
			return null;
		}
	}
	
	private Connection getConnection() throws SQLException {
		return dataSource.getConnection();
	}
	
	private void closeConnection(Connection conn) {
		try {
			conn.commit();
			conn.close();
			log.warn("closeConnection!conn is closed: {}", conn.isClosed());
		} catch (SQLException e) {
			log.error("", e);
		}
	}

	private void initDataSource() {
		if (!isUse) {
			log.info("initDataSource: isUse: {}", isUse);
			return;
		}
		log.info("initDataSource: url {} user {} pwd {}", url, user, "***");
		HikariConfig config = new HikariConfig();
		config.setJdbcUrl(url);
		config.setUsername(user);
		config.setPassword(pwd);
		config.setMaximumPoolSize(poolSize);
		config.setLeakDetectionThreshold(TimeConstant.ONE_SECOND * 30);
		config.setIdleTimeout(TimeConstant.ONE_MINUTE * 5);
		dataSource =  new HikariDataSource(config); 
		log.info("initDataSource: Successed");
	}
	
	private static final class SqlStatement {
		static final String GET_REFS = "dbo.getrefs";
		static final String CALCULATE_INDEX = "dbo.CalculateIndex ?";
		static final String GET_INDEX_REFS = "SELECT RefValue FROM dbo.sbIndexRef WHERE IndexName = ?";
	}
}
