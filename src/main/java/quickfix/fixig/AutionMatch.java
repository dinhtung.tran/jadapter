
package quickfix.fixig;
import quickfix.FieldNotFound;


public class AutionMatch extends Message
{

  static final long serialVersionUID = 20050617;
  public static final String MSGTYPE = "EP";
  

  public AutionMatch()
  {
    super();
    getHeader().setField(new quickfix.field.MsgType(MSGTYPE));
  }
  
  public void set(quickfix.field.ActionType value)
  {
    setField(value);
  }

  public quickfix.field.ActionType get(quickfix.field.ActionType  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.ActionType getActionType() throws FieldNotFound
  {
    quickfix.field.ActionType value = new quickfix.field.ActionType();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.ActionType field)
  {
    return isSetField(field);
  }

  public boolean isSetActionType()
  {
    return isSetField(33);
  }
  
  public void set(quickfix.field.Symbol value)
  {
    setField(value);
  }

  public quickfix.field.Symbol get(quickfix.field.Symbol  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.Symbol getSymbol() throws FieldNotFound
  {
    quickfix.field.Symbol value = new quickfix.field.Symbol();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.Symbol field)
  {
    return isSetField(field);
  }

  public boolean isSetSymbol()
  {
    return isSetField(55);
  }
  
  public void set(quickfix.field.MatchPrice value)
  {
    setField(value);
  }

  public quickfix.field.MatchPrice get(quickfix.field.MatchPrice  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.MatchPrice getMatchPrice() throws FieldNotFound
  {
    quickfix.field.MatchPrice value = new quickfix.field.MatchPrice();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.MatchPrice field)
  {
    return isSetField(field);
  }

  public boolean isSetMatchPrice()
  {
    return isSetField(31);
  }
  
  public void set(quickfix.field.MatchQtty value)
  {
    setField(value);
  }

  public quickfix.field.MatchQtty get(quickfix.field.MatchQtty  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.MatchQtty getMatchQtty() throws FieldNotFound
  {
    quickfix.field.MatchQtty value = new quickfix.field.MatchQtty();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.MatchQtty field)
  {
    return isSetField(field);
  }

  public boolean isSetMatchQtty()
  {
    return isSetField(32);
  }
  
}
  