
package quickfix.fixig;
import quickfix.FieldNotFound;


public class ETFTrackingError extends Message
{

  static final long serialVersionUID = 20050617;
  public static final String MSGTYPE = "TE";
  

  public ETFTrackingError()
  {
    super();
    getHeader().setField(new quickfix.field.MsgType(MSGTYPE));
  }
  
  public void set(quickfix.field.ActionType value)
  {
    setField(value);
  }

  public quickfix.field.ActionType get(quickfix.field.ActionType  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.ActionType getActionType() throws FieldNotFound
  {
    quickfix.field.ActionType value = new quickfix.field.ActionType();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.ActionType field)
  {
    return isSetField(field);
  }

  public boolean isSetActionType()
  {
    return isSetField(33);
  }
  
  public void set(quickfix.field.Symbol value)
  {
    setField(value);
  }

  public quickfix.field.Symbol get(quickfix.field.Symbol  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.Symbol getSymbol() throws FieldNotFound
  {
    quickfix.field.Symbol value = new quickfix.field.Symbol();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.Symbol field)
  {
    return isSetField(field);
  }

  public boolean isSetSymbol()
  {
    return isSetField(55);
  }
  
  public void set(quickfix.field.Price value)
  {
    setField(value);
  }

  public quickfix.field.Price get(quickfix.field.Price  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.Price getPrice() throws FieldNotFound
  {
    quickfix.field.Price value = new quickfix.field.Price();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.Price field)
  {
    return isSetField(field);
  }

  public boolean isSetPrice()
  {
    return isSetField(44);
  }
  
}
  