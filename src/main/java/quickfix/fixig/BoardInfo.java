
package quickfix.fixig;
import quickfix.FieldNotFound;


public class BoardInfo extends Message
{

  static final long serialVersionUID = 20050617;
  public static final String MSGTYPE = "BI";
  

  public BoardInfo()
  {
    super();
    getHeader().setField(new quickfix.field.MsgType(MSGTYPE));
  }
  
  public void set(quickfix.field.BoardCode value)
  {
    setField(value);
  }

  public quickfix.field.BoardCode get(quickfix.field.BoardCode  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.BoardCode getBoardCode() throws FieldNotFound
  {
    quickfix.field.BoardCode value = new quickfix.field.BoardCode();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.BoardCode field)
  {
    return isSetField(field);
  }

  public boolean isSetBoardCode()
  {
    return isSetField(425);
  }
  
  public void set(quickfix.field.BoardStatus value)
  {
    setField(value);
  }

  public quickfix.field.BoardStatus get(quickfix.field.BoardStatus  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.BoardStatus getBoardStatus() throws FieldNotFound
  {
    quickfix.field.BoardStatus value = new quickfix.field.BoardStatus();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.BoardStatus field)
  {
    return isSetField(field);
  }

  public boolean isSetBoardStatus()
  {
    return isSetField(426);
  }
  
  public void set(quickfix.field.TradingSessionID value)
  {
    setField(value);
  }

  public quickfix.field.TradingSessionID get(quickfix.field.TradingSessionID  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.TradingSessionID getTradingSessionID() throws FieldNotFound
  {
    quickfix.field.TradingSessionID value = new quickfix.field.TradingSessionID();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.TradingSessionID field)
  {
    return isSetField(field);
  }

  public boolean isSetTradingSessionID()
  {
    return isSetField(336);
  }
  
  public void set(quickfix.field.TradSesStatus value)
  {
    setField(value);
  }

  public quickfix.field.TradSesStatus get(quickfix.field.TradSesStatus  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.TradSesStatus getTradSesStatus() throws FieldNotFound
  {
    quickfix.field.TradSesStatus value = new quickfix.field.TradSesStatus();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.TradSesStatus field)
  {
    return isSetField(field);
  }

  public boolean isSetTradSesStatus()
  {
    return isSetField(340);
  }
  
  public void set(quickfix.field.Name value)
  {
    setField(value);
  }

  public quickfix.field.Name get(quickfix.field.Name  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.Name getName() throws FieldNotFound
  {
    quickfix.field.Name value = new quickfix.field.Name();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.Name field)
  {
    return isSetField(field);
  }

  public boolean isSetName()
  {
    return isSetField(421);
  }
  
  public void set(quickfix.field.Shortname value)
  {
    setField(value);
  }

  public quickfix.field.Shortname get(quickfix.field.Shortname  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.Shortname getShortname() throws FieldNotFound
  {
    quickfix.field.Shortname value = new quickfix.field.Shortname();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.Shortname field)
  {
    return isSetField(field);
  }

  public boolean isSetShortname()
  {
    return isSetField(422);
  }
  
  public void set(quickfix.field.TradingDate value)
  {
    setField(value);
  }

  public quickfix.field.TradingDate get(quickfix.field.TradingDate  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.TradingDate getTradingDate() throws FieldNotFound
  {
    quickfix.field.TradingDate value = new quickfix.field.TradingDate();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.TradingDate field)
  {
    return isSetField(field);
  }

  public boolean isSetTradingDate()
  {
    return isSetField(388);
  }
  
  public void set(quickfix.field.Time value)
  {
    setField(value);
  }

  public quickfix.field.Time get(quickfix.field.Time  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.Time getTime() throws FieldNotFound
  {
    quickfix.field.Time value = new quickfix.field.Time();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.Time field)
  {
    return isSetField(field);
  }

  public boolean isSetTime()
  {
    return isSetField(399);
  }
  
  public void set(quickfix.field.TotalTrade value)
  {
    setField(value);
  }

  public quickfix.field.TotalTrade get(quickfix.field.TotalTrade  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.TotalTrade getTotalTrade() throws FieldNotFound
  {
    quickfix.field.TotalTrade value = new quickfix.field.TotalTrade();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.TotalTrade field)
  {
    return isSetField(field);
  }

  public boolean isSetTotalTrade()
  {
    return isSetField(270);
  }
  
  public void set(quickfix.field.TotalStock2 value)
  {
    setField(value);
  }

  public quickfix.field.TotalStock2 get(quickfix.field.TotalStock2  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.TotalStock2 getTotalStock2() throws FieldNotFound
  {
    quickfix.field.TotalStock2 value = new quickfix.field.TotalStock2();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.TotalStock2 field)
  {
    return isSetField(field);
  }

  public boolean isSetTotalStock2()
  {
    return isSetField(250);
  }
  
  public void set(quickfix.field.NumSymbolAdvances value)
  {
    setField(value);
  }

  public quickfix.field.NumSymbolAdvances get(quickfix.field.NumSymbolAdvances  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.NumSymbolAdvances getnumSymbolAdvances() throws FieldNotFound
  {
    quickfix.field.NumSymbolAdvances value = new quickfix.field.NumSymbolAdvances();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.NumSymbolAdvances field)
  {
    return isSetField(field);
  }

  public boolean isSetnumSymbolAdvances()
  {
    return isSetField(251);
  }
  
  public void set(quickfix.field.NumSymbolNochange value)
  {
    setField(value);
  }

  public quickfix.field.NumSymbolNochange get(quickfix.field.NumSymbolNochange  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.NumSymbolNochange getnumSymbolNochange() throws FieldNotFound
  {
    quickfix.field.NumSymbolNochange value = new quickfix.field.NumSymbolNochange();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.NumSymbolNochange field)
  {
    return isSetField(field);
  }

  public boolean isSetnumSymbolNochange()
  {
    return isSetField(252);
  }
  
  public void set(quickfix.field.NumSymbolDeclines value)
  {
    setField(value);
  }

  public quickfix.field.NumSymbolDeclines get(quickfix.field.NumSymbolDeclines  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.NumSymbolDeclines getnumSymbolDeclines() throws FieldNotFound
  {
    quickfix.field.NumSymbolDeclines value = new quickfix.field.NumSymbolDeclines();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.NumSymbolDeclines field)
  {
    return isSetField(field);
  }

  public boolean isSetnumSymbolDeclines()
  {
    return isSetField(253);
  }
  
  public void set(quickfix.field.DateNo value)
  {
    setField(value);
  }

  public quickfix.field.DateNo get(quickfix.field.DateNo  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.DateNo getDateNo() throws FieldNotFound
  {
    quickfix.field.DateNo value = new quickfix.field.DateNo();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.DateNo field)
  {
    return isSetField(field);
  }

  public boolean isSetDateNo()
  {
    return isSetField(17);
  }
  
  public void set(quickfix.field.TotalNormalTradedQttyRd value)
  {
    setField(value);
  }

  public quickfix.field.TotalNormalTradedQttyRd get(quickfix.field.TotalNormalTradedQttyRd  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.TotalNormalTradedQttyRd gettotalNormalTradedQttyRd() throws FieldNotFound
  {
    quickfix.field.TotalNormalTradedQttyRd value = new quickfix.field.TotalNormalTradedQttyRd();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.TotalNormalTradedQttyRd field)
  {
    return isSetField(field);
  }

  public boolean isSettotalNormalTradedQttyRd()
  {
    return isSetField(220);
  }
  
  public void set(quickfix.field.TotalNormalTradedValueRd value)
  {
    setField(value);
  }

  public quickfix.field.TotalNormalTradedValueRd get(quickfix.field.TotalNormalTradedValueRd  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.TotalNormalTradedValueRd gettotalNormalTradedValueRd() throws FieldNotFound
  {
    quickfix.field.TotalNormalTradedValueRd value = new quickfix.field.TotalNormalTradedValueRd();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.TotalNormalTradedValueRd field)
  {
    return isSetField(field);
  }

  public boolean isSettotalNormalTradedValueRd()
  {
    return isSetField(221);
  }
  
  public void set(quickfix.field.TotalNormalTradedQttyOd value)
  {
    setField(value);
  }

  public quickfix.field.TotalNormalTradedQttyOd get(quickfix.field.TotalNormalTradedQttyOd  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.TotalNormalTradedQttyOd gettotalNormalTradedQttyOd() throws FieldNotFound
  {
    quickfix.field.TotalNormalTradedQttyOd value = new quickfix.field.TotalNormalTradedQttyOd();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.TotalNormalTradedQttyOd field)
  {
    return isSetField(field);
  }

  public boolean isSettotalNormalTradedQttyOd()
  {
    return isSetField(210);
  }
  
  public void set(quickfix.field.TotalNormalTradedValueOd value)
  {
    setField(value);
  }

  public quickfix.field.TotalNormalTradedValueOd get(quickfix.field.TotalNormalTradedValueOd  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.TotalNormalTradedValueOd gettotalNormalTradedValueOd() throws FieldNotFound
  {
    quickfix.field.TotalNormalTradedValueOd value = new quickfix.field.TotalNormalTradedValueOd();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.TotalNormalTradedValueOd field)
  {
    return isSetField(field);
  }

  public boolean isSettotalNormalTradedValueOd()
  {
    return isSetField(211);
  }
  
  public void set(quickfix.field.TotalPTTradedQtty value)
  {
    setField(value);
  }

  public quickfix.field.TotalPTTradedQtty get(quickfix.field.TotalPTTradedQtty  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.TotalPTTradedQtty gettotalPTTradedQtty() throws FieldNotFound
  {
    quickfix.field.TotalPTTradedQtty value = new quickfix.field.TotalPTTradedQtty();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.TotalPTTradedQtty field)
  {
    return isSetField(field);
  }

  public boolean isSettotalPTTradedQtty()
  {
    return isSetField(240);
  }
  
  public void set(quickfix.field.TotalPTTradedValue value)
  {
    setField(value);
  }

  public quickfix.field.TotalPTTradedValue get(quickfix.field.TotalPTTradedValue  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.TotalPTTradedValue gettotalPTTradedValue() throws FieldNotFound
  {
    quickfix.field.TotalPTTradedValue value = new quickfix.field.TotalPTTradedValue();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.TotalPTTradedValue field)
  {
    return isSetField(field);
  }

  public boolean isSettotalPTTradedValue()
  {
    return isSetField(241);
  }
  
  public void set(quickfix.field.MarketCode value)
  {
    setField(value);
  }

  public quickfix.field.MarketCode get(quickfix.field.MarketCode  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.MarketCode getMarketCode() throws FieldNotFound
  {
    quickfix.field.MarketCode value = new quickfix.field.MarketCode();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.MarketCode field)
  {
    return isSetField(field);
  }

  public boolean isSetMarketCode()
  {
    return isSetField(341);
  }
  
}
  