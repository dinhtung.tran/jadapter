
package quickfix.fixig;
import quickfix.FieldNotFound;


public class ETFNetValue extends Message
{

  static final long serialVersionUID = 20050617;
  public static final String MSGTYPE = "IV";
  

  public ETFNetValue()
  {
    super();
    getHeader().setField(new quickfix.field.MsgType(MSGTYPE));
  }
  
  public void set(quickfix.field.Time value)
  {
    setField(value);
  }

  public quickfix.field.Time get(quickfix.field.Time  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.Time getTime() throws FieldNotFound
  {
    quickfix.field.Time value = new quickfix.field.Time();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.Time field)
  {
    return isSetField(field);
  }

  public boolean isSetTime()
  {
    return isSetField(399);
  }
  
  public void set(quickfix.field.Text value)
  {
    setField(value);
  }

  public quickfix.field.Text get(quickfix.field.Text  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.Text getText() throws FieldNotFound
  {
    quickfix.field.Text value = new quickfix.field.Text();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.Text field)
  {
    return isSetField(field);
  }

  public boolean isSetText()
  {
    return isSetField(58);
  }
  
}
  