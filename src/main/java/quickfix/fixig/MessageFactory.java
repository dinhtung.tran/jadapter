/* -*- Generated Java -*- */
 /*******************************************************************************
 * Copyright (c) quickfixengine.org  All rights reserved. 
 * 
 * This file is part of the QuickFIX FIX Engine 
 * 
 * This file may be distributed under the terms of the quickfixengine.org 
 * license as defined by quickfixengine.org and appearing in the file 
 * LICENSE included in the packaging of this file. 
 * 
 * This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
 * THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE. 
 * 
 * See http://www.quickfixengine.org/LICENSE for licensing information. 
 * 
 * Contact ask@quickfixengine.org if any conditions of this licensing 
 * are not clear to you.
 ******************************************************************************/

package quickfix.fixig;

import quickfix.Message;
import quickfix.Group;

public class MessageFactory implements quickfix.MessageFactory
{
  public Message create( String beginString, String msgType ) {
  
       if(quickfix.fixig.DerivativesInfo.MSGTYPE.equals(msgType)) {
         return new quickfix.fixig.DerivativesInfo();
       }
     
       if(quickfix.fixig.ETFTrackingError.MSGTYPE.equals(msgType)) {
         return new quickfix.fixig.ETFTrackingError();
       }
     
       if(quickfix.fixig.ETFNetValue.MSGTYPE.equals(msgType)) {
         return new quickfix.fixig.ETFNetValue();
       }
     
       if(quickfix.fixig.AutionMatch.MSGTYPE.equals(msgType)) {
         return new quickfix.fixig.AutionMatch();
       }
     
       if(quickfix.fixig.BoardInfo.MSGTYPE.equals(msgType)) {
         return new quickfix.fixig.BoardInfo();
       }
     
       if(quickfix.fixig.StockInfo.MSGTYPE.equals(msgType)) {
         return new quickfix.fixig.StockInfo();
       }
     
       if(quickfix.fixig.BasketInfo.MSGTYPE.equals(msgType)) {
         return new quickfix.fixig.BasketInfo();
       }
     
       if(quickfix.fixig.IndexInfo.MSGTYPE.equals(msgType)) {
         return new quickfix.fixig.IndexInfo();
       }
     
       if(quickfix.fixig.TopNPrice.MSGTYPE.equals(msgType)) {
         return new quickfix.fixig.TopNPrice();
       }
     
  return new quickfix.fixig.Message();
  }

     public Group create(String beginString, String msgType, int correspondingFieldID) {
         
       if(quickfix.fixig.TopNPrice.MSGTYPE.equals(msgType)) {
         switch(correspondingFieldID) {
         
           case quickfix.field.NoTopPrice.FIELD:
                return new quickfix.fixig.TopNPrice.NoTopPrice();
		 
         }
       }
     
        return null;
     }
}

  