/*******************************************************************************
 * Copyright (c) quickfixengine.org  All rights reserved. 
 * 
 * This file is part of the QuickFIX FIX Engine 
 * 
 * This file may be distributed under the terms of the quickfixengine.org 
 * license as defined by quickfixengine.org and appearing in the file 
 * LICENSE included in the packaging of this file. 
 * 
 * This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING 
 * THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A 
 * PARTICULAR PURPOSE. 
 * 
 * See http://www.quickfixengine.org/LICENSE for licensing information. 
 * 
 * Contact ask@quickfixengine.org if any conditions of this licensing 
 * are not clear to you.
 ******************************************************************************/

package quickfix.fixig;

import quickfix.*;
import quickfix.field.*;

public class MessageCracker 
{
public void onMessage( quickfix.Message message, SessionID sessionID ) throws FieldNotFound, UnsupportedMessageType, IncorrectTagValue
  { throw new UnsupportedMessageType(); }
 public void onMessage( DerivativesInfo message, SessionID sessionID ) throws FieldNotFound, UnsupportedMessageType, IncorrectTagValue
    { throw new UnsupportedMessageType(); }
  public void onMessage( ETFTrackingError message, SessionID sessionID ) throws FieldNotFound, UnsupportedMessageType, IncorrectTagValue
    { throw new UnsupportedMessageType(); }
  public void onMessage( ETFNetValue message, SessionID sessionID ) throws FieldNotFound, UnsupportedMessageType, IncorrectTagValue
    { throw new UnsupportedMessageType(); }
  public void onMessage( AutionMatch message, SessionID sessionID ) throws FieldNotFound, UnsupportedMessageType, IncorrectTagValue
    { throw new UnsupportedMessageType(); }
  public void onMessage( BoardInfo message, SessionID sessionID ) throws FieldNotFound, UnsupportedMessageType, IncorrectTagValue
    { throw new UnsupportedMessageType(); }
  public void onMessage( StockInfo message, SessionID sessionID ) throws FieldNotFound, UnsupportedMessageType, IncorrectTagValue
    { throw new UnsupportedMessageType(); }
  public void onMessage( BasketInfo message, SessionID sessionID ) throws FieldNotFound, UnsupportedMessageType, IncorrectTagValue
    { throw new UnsupportedMessageType(); }
  public void onMessage( IndexInfo message, SessionID sessionID ) throws FieldNotFound, UnsupportedMessageType, IncorrectTagValue
    { throw new UnsupportedMessageType(); }
  public void onMessage( TopNPrice message, SessionID sessionID ) throws FieldNotFound, UnsupportedMessageType, IncorrectTagValue
    { throw new UnsupportedMessageType(); }
 
  public void crack( quickfix.Message message, SessionID sessionID )
    throws UnsupportedMessageType, FieldNotFound, IncorrectTagValue
  { crackInfogate((Message)message, sessionID); }

  public void crackInfogate( Message message, SessionID sessionID )
    throws UnsupportedMessageType, FieldNotFound, IncorrectTagValue
  {
    MsgType msgType = new MsgType();
    message.getHeader().getField(msgType);
    String msgTypeValue = msgType.getValue();

    if( msgTypeValue.equals(DerivativesInfo.MSGTYPE) )
      onMessage( (DerivativesInfo)message, sessionID );
    else
    if( msgTypeValue.equals(ETFTrackingError.MSGTYPE) )
      onMessage( (ETFTrackingError)message, sessionID );
    else
    if( msgTypeValue.equals(ETFNetValue.MSGTYPE) )
      onMessage( (ETFNetValue)message, sessionID );
    else
    if( msgTypeValue.equals(AutionMatch.MSGTYPE) )
      onMessage( (AutionMatch)message, sessionID );
    else
    if( msgTypeValue.equals(BoardInfo.MSGTYPE) )
      onMessage( (BoardInfo)message, sessionID );
    else
    if( msgTypeValue.equals(StockInfo.MSGTYPE) )
      onMessage( (StockInfo)message, sessionID );
    else
    if( msgTypeValue.equals(BasketInfo.MSGTYPE) )
      onMessage( (BasketInfo)message, sessionID );
    else
    if( msgTypeValue.equals(IndexInfo.MSGTYPE) )
      onMessage( (IndexInfo)message, sessionID );
    else
    if( msgTypeValue.equals(TopNPrice.MSGTYPE) )
      onMessage( (TopNPrice)message, sessionID );
    else onMessage( message, sessionID );
  }

  };


