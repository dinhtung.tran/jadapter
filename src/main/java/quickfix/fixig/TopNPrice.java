
package quickfix.fixig;
import quickfix.FieldNotFound;

import quickfix.Group;

public class TopNPrice extends Message
{

  static final long serialVersionUID = 20050617;
  public static final String MSGTYPE = "TP";
  

  public TopNPrice()
  {
    super();
    getHeader().setField(new quickfix.field.MsgType(MSGTYPE));
  }
  
  public TopNPrice(quickfix.field.Symbol symbol, quickfix.field.BoardCode boardCode) {
    this();
    setField(symbol);
    setField(boardCode);
  }
    
  public void set(quickfix.field.Symbol value)
  {
    setField(value);
  }

  public quickfix.field.Symbol get(quickfix.field.Symbol  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.Symbol getSymbol() throws FieldNotFound
  {
    quickfix.field.Symbol value = new quickfix.field.Symbol();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.Symbol field)
  {
    return isSetField(field);
  }

  public boolean isSetSymbol()
  {
    return isSetField(55);
  }
  
  public void set(quickfix.field.BoardCode value)
  {
    setField(value);
  }

  public quickfix.field.BoardCode get(quickfix.field.BoardCode  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.BoardCode getBoardCode() throws FieldNotFound
  {
    quickfix.field.BoardCode value = new quickfix.field.BoardCode();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.BoardCode field)
  {
    return isSetField(field);
  }

  public boolean isSetBoardCode()
  {
    return isSetField(425);
  }
  
  public void set(quickfix.field.NoTopPrice value)
  {
    setField(value);
  }

  public quickfix.field.NoTopPrice get(quickfix.field.NoTopPrice  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.NoTopPrice getNoTopPrice() throws FieldNotFound
  {
    quickfix.field.NoTopPrice value = new quickfix.field.NoTopPrice();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.NoTopPrice field)
  {
    return isSetField(field);
  }

  public boolean isSetNoTopPrice()
  {
    return isSetField(555);
  }
  
  public static class NoTopPrice extends Group {
    static final long serialVersionUID = 20050617;
    public NoTopPrice() {
        super(555, 556,
            new int[] {556, 132, 1321, 133, 1331,  0 } );
    }
    
  public void set(quickfix.field.NumTopPrice value)
  {
    setField(value);
  }

  public quickfix.field.NumTopPrice get(quickfix.field.NumTopPrice  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.NumTopPrice getNumTopPrice() throws FieldNotFound
  {
    quickfix.field.NumTopPrice value = new quickfix.field.NumTopPrice();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.NumTopPrice field)
  {
    return isSetField(field);
  }

  public boolean isSetNumTopPrice()
  {
    return isSetField(556);
  }
  
  public void set(quickfix.field.BestBidPrice value)
  {
    setField(value);
  }

  public quickfix.field.BestBidPrice get(quickfix.field.BestBidPrice  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.BestBidPrice getBestBidPrice() throws FieldNotFound
  {
    quickfix.field.BestBidPrice value = new quickfix.field.BestBidPrice();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.BestBidPrice field)
  {
    return isSetField(field);
  }

  public boolean isSetBestBidPrice()
  {
    return isSetField(132);
  }
  
  public void set(quickfix.field.BestBidQtty value)
  {
    setField(value);
  }

  public quickfix.field.BestBidQtty get(quickfix.field.BestBidQtty  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.BestBidQtty getBestBidQtty() throws FieldNotFound
  {
    quickfix.field.BestBidQtty value = new quickfix.field.BestBidQtty();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.BestBidQtty field)
  {
    return isSetField(field);
  }

  public boolean isSetBestBidQtty()
  {
    return isSetField(1321);
  }
  
  public void set(quickfix.field.BestOfferPrice value)
  {
    setField(value);
  }

  public quickfix.field.BestOfferPrice get(quickfix.field.BestOfferPrice  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.BestOfferPrice getBestOfferPrice() throws FieldNotFound
  {
    quickfix.field.BestOfferPrice value = new quickfix.field.BestOfferPrice();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.BestOfferPrice field)
  {
    return isSetField(field);
  }

  public boolean isSetBestOfferPrice()
  {
    return isSetField(133);
  }
  
  public void set(quickfix.field.BestOfferQtty value)
  {
    setField(value);
  }

  public quickfix.field.BestOfferQtty get(quickfix.field.BestOfferQtty  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.BestOfferQtty getBestOfferQtty() throws FieldNotFound
  {
    quickfix.field.BestOfferQtty value = new quickfix.field.BestOfferQtty();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.BestOfferQtty field)
  {
    return isSetField(field);
  }

  public boolean isSetBestOfferQtty()
  {
    return isSetField(1331);
  }
  
  }
  
}
  