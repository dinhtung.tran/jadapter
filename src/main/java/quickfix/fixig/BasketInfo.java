
package quickfix.fixig;
import quickfix.FieldNotFound;


public class BasketInfo extends Message
{

  static final long serialVersionUID = 20050617;
  public static final String MSGTYPE = "S";
  

  public BasketInfo()
  {
    super();
    getHeader().setField(new quickfix.field.MsgType(MSGTYPE));
  }
  
  public void set(quickfix.field.IDIndex value)
  {
    setField(value);
  }

  public quickfix.field.IDIndex get(quickfix.field.IDIndex  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.IDIndex getIDIndex() throws FieldNotFound
  {
    quickfix.field.IDIndex value = new quickfix.field.IDIndex();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.IDIndex field)
  {
    return isSetField(field);
  }

  public boolean isSetIDIndex()
  {
    return isSetField(1);
  }
  
  public void set(quickfix.field.IndexCode value)
  {
    setField(value);
  }

  public quickfix.field.IndexCode get(quickfix.field.IndexCode  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.IndexCode getIndexCode() throws FieldNotFound
  {
    quickfix.field.IndexCode value = new quickfix.field.IndexCode();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.IndexCode field)
  {
    return isSetField(field);
  }

  public boolean isSetIndexCode()
  {
    return isSetField(2);
  }
  
  public void set(quickfix.field.IDSymbol value)
  {
    setField(value);
  }

  public quickfix.field.IDSymbol get(quickfix.field.IDSymbol  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.IDSymbol getIDSymbol() throws FieldNotFound
  {
    quickfix.field.IDSymbol value = new quickfix.field.IDSymbol();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.IDSymbol field)
  {
    return isSetField(field);
  }

  public boolean isSetIDSymbol()
  {
    return isSetField(15);
  }
  
  public void set(quickfix.field.Symbol value)
  {
    setField(value);
  }

  public quickfix.field.Symbol get(quickfix.field.Symbol  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.Symbol getSymbol() throws FieldNotFound
  {
    quickfix.field.Symbol value = new quickfix.field.Symbol();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.Symbol field)
  {
    return isSetField(field);
  }

  public boolean isSetSymbol()
  {
    return isSetField(55);
  }
  
  public void set(quickfix.field.TotalQtty2 value)
  {
    setField(value);
  }

  public quickfix.field.TotalQtty2 get(quickfix.field.TotalQtty2  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.TotalQtty2 getTotalQtty2() throws FieldNotFound
  {
    quickfix.field.TotalQtty2 value = new quickfix.field.TotalQtty2();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.TotalQtty2 field)
  {
    return isSetField(field);
  }

  public boolean isSetTotalQtty2()
  {
    return isSetField(11);
  }
  
  public void set(quickfix.field.Weighted value)
  {
    setField(value);
  }

  public quickfix.field.Weighted get(quickfix.field.Weighted  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.Weighted getWeighted() throws FieldNotFound
  {
    quickfix.field.Weighted value = new quickfix.field.Weighted();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.Weighted field)
  {
    return isSetField(field);
  }

  public boolean isSetWeighted()
  {
    return isSetField(12);
  }
  
  public void set(quickfix.field.AddDate value)
  {
    setField(value);
  }

  public quickfix.field.AddDate get(quickfix.field.AddDate  value) throws FieldNotFound
  {
    getField(value);
    return value;
  }

  public quickfix.field.AddDate getAddDate() throws FieldNotFound
  {
    quickfix.field.AddDate value = new quickfix.field.AddDate();
    getField(value);
    return value;
  }

  public boolean isSet(quickfix.field.AddDate field)
  {
    return isSetField(field);
  }

  public boolean isSetAddDate()
  {
    return isSetField(28);
  }
  
}
  